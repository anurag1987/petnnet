<?php
class usermodel extends CI_Model
{
	public function servender()
	{
		$qry = $this->db->select('*')
						->get('petnnet_services');
		return $qry->result();
	}

	public function addmem($data)
	{
		$qry = $this->db->insert('petnnet_member',$data);
		return $qry;
	}

	public function memlogin($user,$pass)
	{
		$qry = $this->db->select('*')
						->where(['email'=>$user,'password'=>$pass])
						->get('petnnet_member');
		return $qry->row();
	}

	public function checkmail($email)
	{
		$qry = $this->db->where('email',$email)
						->get('petnnet_member');
		return $qry->num_rows();
	}

	public function get_member($id)
	{
		$qry = $this->db->where('memid',$id)
						->get('petnnet_member');
		return $qry->result();
	}

	public function upmemprofile($id,$data)
	{
		$qry = $this->db->where('memid',$id)
						->update('petnnet_member',$data);
		return $qry;
	}

	public function getmempass($id)
	{
		$qry = $this->db->select('*')
						->where('memid',$id)
						->get('petnnet_member');
		return $qry->row();
	}

	public function updatemem($newpass,$id)
	{
		$qry = $this->db->where('memid',$id)
						->update('petnnet_member',['password'=>$newpass]);
		return $qry;
	}

	public function business_mem($data)
	{
		$qry = $this->db->insert('petnnet_business',$data);
		return $qry;
	}

	public function checbusinesskemail($email)
	{
		$qry = $this->db->where('email',$email)
						->get('petnnet_business');
		return $qry->num_rows();
	}

	public function buslogin($user,$pass)
	{
		$qry = $this->db->select('*')
						->where(['email'=>$user,'password'=>$pass])
						->get('petnnet_business');
		return $qry->row();
	}

	public function getbusinesspass($id)
	{
		$qry = $this->db->select('*')
						->where('memid',$id)
						->get('petnnet_business');
		return $qry->row();
	}

	public function updatebusinesspass($newpass,$id)
	{
		$qry = $this->db->where('memid',$id)
						->update('petnnet_business',['password'=>$newpass]);
		return $qry;
	}

	public function get_business($id)
	{
		$qry = $this->db
						->where('memid',$id)
						->get('petnnet_business');
		return $qry->result();
	}

	public function updateprofile($id,$data)
	{
		$qry = $this->db->where('memid',$id)
						->update('petnnet_business',$data);
		return $qry;
	}

	public function sub($data)
	{
		$qry = $this->db->insert('petnnet_subscribe',$data);
		return $qry;
	}

	public function getpassword($email,$mobile)
	{
		$qry = $this->db->select("*")
						->where(['email'=>$email,'mobile'=>$mobile])
						->get('petnnet_member');
		return $qry->row();
	}

	public function businesspassword($email,$mobile)
	{
		$qry = $this->db->select("*")
						->where(['email'=>$email,'mobile'=>$mobile])
						->get('petnnet_business');
		return $qry->row();
	}

	public function servicelist($id)
	{
		$qry = $this->db->select('serviceid,pernnet_buss_services.memid as ser_memid,serv_title,serv_desc,serv_img,created_at,pernnet_buss_services.status as ser_st')
						->from('pernnet_buss_services','petnnet_business')
						->where('pernnet_buss_services.memid',$id)
						->join('petnnet_business','pernnet_buss_services.memid = petnnet_business.memid','left')
						->order_by('pernnet_buss_services.status','asc')
						->limit ('5')
						->get('');
		return $qry->result();
	}

	public function allservice($id)
	{
		$qry = $this->db->select('serviceid,pernnet_buss_services.memid as ser_memid,serv_title,serv_desc,serv_img,created_at,pernnet_buss_services.status as ser_st')
						->from('pernnet_buss_services','petnnet_business')
						->where('pernnet_buss_services.memid',$id)
						->join('petnnet_business','pernnet_buss_services.memid = petnnet_business.memid','left')
						->order_by('pernnet_buss_services.status','asc')
						->get('');
		return $qry->result();

	}

	public function delete_srv($id)

	{

		$qry = $this->db->delete('pernnet_buss_services',['serviceid'=>$id]);

		return $qry;

	}

	public function productlist($id)

	{

		$qry = $this->db->select('product_id,category_id,petnnet_product.memid as proid,title,proimg,description,sale_price,actual_price,created_at,petnnet_product.status as pro_status')

				 ->from('petnnet_product','petnnet_business','petnnet_services')

				 ->where('petnnet_product.memid',$id)

				 ->join('petnnet_business','petnnet_product.memid = petnnet_business.memid','left')

				 ->order_by('petnnet_product.status','asc')

				 ->limit(5)

				 ->get();

		return $qry->result();

	}

	public function allproducts($id)
	{
		$qry = $this->db->select('product_id,category_id,petnnet_product.memid as proid,title,proimg,petnnet_product.description as prodesc,sale_price,actual_price,created_at,petnnet_product.status as pro_status,petnnet_services.serviceid as servid,petnnet_services.name as name')
				 ->from('petnnet_product','petnnet_business','petnnet_services')
				 ->where('petnnet_product.memid',$id)
				 ->join('petnnet_business','petnnet_product.memid = petnnet_business.memid','left')
				 ->join('petnnet_services','petnnet_product.category_id = petnnet_services.serviceid','left')
				 ->order_by('petnnet_product.status','asc')
				 ->get();
		return $qry->result();
	}

	public function delete_pro($id)
	{
		$qry = $this->db->delete('petnnet_product',['product_id'=>$id]);
		return $qry;
	}
	
	public function procategory()
	{
		$qry = $this->db->select("*")
					   ->get("petnnet_product_category");
		return $qry->result();
	}

	public function add_service($data)

	{

		$qry = $this->db->insert('pernnet_buss_services',$data);

		return $qry;

	}

	public function addproduct($data)

	{

		$qry = $this->db->insert('petnnet_product',$data);

		return $qry;

	}

	public function product_list()

	{

		$qry = $this->db->select("*")

						->where('status',1)
						->limit(12)

						->get('petnnet_product');

		return $qry->result();

	}

}

?>





























