<?php 

 if (!defined('BASEPATH')) exit('No direct script access allowed');

 ob_start();

class admin_model extends CI_Model

{

	public function servender()

	{

		$qry = $this->db->select('*')

						->get('petnnet_services');

		return $qry->result();

	}

	public function memlistmenu($id)

	{

		$qry = $this->db->select("*")

						->where('memid',$id)

						->get('petnnet_business');

		return $qry;

			

	}

	public function memlist($id)

	{

		$qry = $this->db->select('memid,names,address,email,password,mobile,status,petnnet_business.mem_type as bustype,name,ser_code,mem_image,otppass,member_from')

						->from('petnnet_business','petnnet_services')

						->where('mem_type',$id)

						->join('petnnet_services','petnnet_business.mem_type = petnnet_services.serviceid','left')

						->get();

		return $qry->result();

	}

	public function editmem($id)

	{

		$qry = $this->db->select('memid,names,address,email,password,mobile,status,serviceid,name,ser_code,mem_image,otppass,member_from')

						->from('petnnet_business','petnnet_services')

						->where('memid',$id)

						->join('petnnet_services','petnnet_business.mem_type = petnnet_services.ser_code','left')

						->get();

		return $qry->result();

	}

	public function updatemem($data,$id)

	{

		$qry = $this->db->where('memid',$id)

						->update('petnnet_business',$data);

		return $qry;

							

	}

	public function delmem($id,$type)

	{

		$qry = $this->db->delete('petnnet_business',['memid'=>$id,'mem_type'=>$type]);

		return $qry;

	}

	public function productlist($id)

	{

		$qry = $this->db->select('product_id,category_id,petnnet_product.memid as proid,title,proimg,description,sale_price,actual_price,created_at,petnnet_product.status as pro_status')

				 ->from('petnnet_product','petnnet_business','petnnet_services')

				 ->where('petnnet_product.memid',$id)

				 ->join('petnnet_business','petnnet_product.memid = petnnet_business.memid','left')

				 ->order_by('petnnet_product.status','asc')

				 ->limit(5)

				 ->get();

		return $qry->result();

	}
	public function service_list()
	{
		$qry = $this->db->select("*")
				 ->get('petnnet_services');
		return $qry->result();
	}
	public function editservice($id)
	{
		$updateqry = $this->db->select("*")
							  ->where('serviceid',$id)
							  ->get('petnnet_services');
		return $updateqry->result();
	}
	public function update_service($data,$id)
	{
		$updateqry = $this->db->where('serviceid',$id)
							  ->update('petnnet_services',$data);
		return $qry;
	}
	public function del_service($id)
	{
		$delqry = $this->db->delete('petnnet_services',['serviceid'=>$id]);
		return $delqry;
	}
	public function get_product($id)

	{

		$qry = $this->db->select('*')

						->where('product_id',$id)

						->get('petnnet_product');

			return $qry->result();

	}

	public function pendingproduct()

	{

				$qry = $this->db->select('product_id,category_id,petnnet_product.memid as proid,petnnet_business.names as busname,title,proimg,description,sale_price,actual_price,created_at,petnnet_product.status as pro_status')

				 ->from('petnnet_product','petnnet_business','petnnet_services')

				 ->where('petnnet_product.status',0)

				 ->join('petnnet_business','petnnet_product.memid = petnnet_business.memid','left')

				 ->order_by('petnnet_product.status','asc')

				 ->limit(5)

				 ->get();

		return $qry->result();

	}

	public function deldaspro($id)

	{

		$qry = $this->db->delete('petnnet_product',['product_id'=>$id]);

		return $qry;

	}

	public function actdashpro($id)

	{

		$qry = $this->db->where('product_id',$id)

						->update('petnnet_product',['status'=>1]);

		return $qry;

	}

	public function activepro()

	{

				$qry = $this->db->select('product_id,category_id,petnnet_product.memid as proid,petnnet_business.names as busname,title,proimg,description,sale_price,actual_price,created_at,petnnet_product.status as pro_status')
				 ->from('petnnet_product','petnnet_business','petnnet_services')
				 ->where('petnnet_product.status',1)
				 ->join('petnnet_business','petnnet_product.memid = petnnet_business.memid','left')
				 ->order_by('petnnet_product.status','asc')
				 ->get();

		return $qry->result();

	}

	public function pro_del($id)

	{

		$qry = $this->db->delete('petnnet_product',['product_id',$id]);

			return $qry;

	}

	public function update_pro($data,$id)

	{

		$qry = $this->db->where('product_id',$id)

						->update('petnnet_product',$data);

		return $qry;

	}

	public function del_pro($id)

	{

		$qry = $this->db->delete('petnnet_product',['product_id'=>$id]);

		return $qry;

	}

	public function insert_cat($data)

	{

		$qry = $this->db->insert('petnnet_product_category',$data);

		return $qry;

	}

	public function catlist()

	{

		$qry = $this->db->select("*")

						->get('petnnet_product_category');

		return $qry->result();

	}

	public function editcat($id)

	{

		$qry = $this->db->select("*")

						->where('prod_catid',$id)

						->get("petnnet_product_category");

		return $qry->result();

	}

	public function update_cat($data,$id)

	{

		$qry = $this->db->where('prod_catid',$id)

						->update('petnnet_product_category',$data);

		return $qry;

	}

	public function del_Cat($id)

	{

		$qry = $this->db->delete('petnnet_product_category',['prod_catid'=>$id]);

		return $qry;

	}
	public function shoppro_list($id)
	{
		$qry = $this->db->select("*")
						->where('product_id',$id)
						->get('petnnet_product');
		return $qry->result();
	}
	public function editshoppro($id,$data)
	{
		$updateqry = $this->db->select("*")
						->where('product_id',$id)
						->update('petnnet_product',$data);
		return $updateqry;	
	}
	public function pro_status($id)
	{
		$qry = $this->db->select("*")
						->where('product_id',$id)
						->get('petnnet_product');
		return $qry->row();
							
	}
	public function upprostatus0($id)
	{
		$qry = $this->db->where('product_id',$id)
						->update('petnnet_product',['status'=>0]);
		return $qry;
	}
	public function upprostatus1($id)
	{
		$qry = $this->db->where('product_id',$id)
						->update('petnnet_product',['status'=>1]);
		return $qry;
	}

	public function customerlist()

	{

		$qry = $this->db->select("*")

						->get('petnnet_member');

		return $qry->result();

	}
	public function delete_Customer($id)
	{
		$qry = $this->db->delete('petnnet_member',['memid'=>$id]);
		return $qry;
	}

	public function sublist()

	{

		$qry = $this->db->select("*")

						->get('petnnet_subscribe');

		return $qry->result();

	}

	public function delsub($id)

	{

		$qry = $this->db->delete('petnnet_subscribe',['subid'=>$id]);

		return $qry;

	}

	

}

?>

