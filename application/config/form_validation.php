<?php 
$config = array(
				'member' => array(
									array(
											'field' => 'fname',
											'label' => 'Frist Name',
											'rules' => 'required'
										 ),
									array(
											'field' => '',
											'label' => 'Frist Name',
											'rules' => 'required'
										 ),
	
									array(
											'field' => 'lname',
											'label' => 'Last Name',
											'rules' => 'required'
										 ),
	
									array(
											'field' => 'email',
											'label' => 'Email-ID',
											'rules' => 'valid_email|required'
										 ),
	
									array(
											'field' => 'password',
											'label' => 'Password',
											'rules' => 'required|min_length[5]'
										 ),
	
									array(
											'field' => 'mobile',
											'label' => 'Mobile',
											'rules' => 'required'
										 ),
	
									array(
											'field' => 'memtype',
											'label' => 'Member Type',
											'rules' => 'required'
										 ),
	
									array(
											'field' => 'memform',
											'label' => 'Member Form',
											'rules' => 'required'
										 )
							),
				'category' =>array(
								array(
											'field' => 'title',
											'label' => 'Category Title',
											'rules' => 'required'
									),
								array(
											'field' => '',
											'label' => '',
											'rules' => ''
									)
								  ),
				'usermem' =>array(
								array(
											'field' => 'name',
											'label' => 'Member Name',
											'rules' => 'required'
										 ),
								array(
											'field' => 'email',
											'label' => 'Member Email',
											'rules' => 'required|valid_email'
										 ),
								array(
											'field' => 'mobile',
											'label' => 'Mobile',
											'rules' => 'required'
										 ),
								array(
											'field' => 'category',
											'label' => 'Category',
											'rules' => 'required'
										 ),
								array(
											'field' => 'address',
											'label' => 'Address',
											'rules' => 'required'
										 )
								  ),
				'business' =>array(
								array(
											'field' => 'name',
											'label' => 'Member Name',
											'rules' => 'required'
										 ),
								array(
											'field' => 'email',
											'label' => 'Member Email',
											'rules' => 'required|valid_email'
										 ),
								array(
											'field' => 'mobile',
											'label' => 'Mobile',
											'rules' => 'required'
										 ),
								array(
											'field' => 'category',
											'label' => 'Category',
											'rules' => 'required'
										 ),
								array(
											'field' => 'address',
											'label' => 'Address',
											'rules' => 'required'
										 )
								  ),
				'sub' =>	array(
								array(
											'field' => 'email',
											'label' => 'Subscribe',
											'rules' => 'required'
										 )
								  ),
				'test' =>	array(
								array(
											'field' => 'test',
											'label' => 'Member Form',
											'rules' => 'required'
										 )
								  )
				);
?>
