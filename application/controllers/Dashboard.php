<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
  ob_start();
class dashboard extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		ob_start();
		if(!$id = $this->session->userdata('id')){
		return redirect('admin_login');
		}
		$this->load->model("Admin_model");
		$results = $this->Admin_model->servender();
		$this->load->view("admin/menu",['results'=>$results]);
	}
	public function index()
	{
		$productlist = $this->Admin_model->pendingproduct();
		$this->load->view("admin/dashboard",['productlist'=>$productlist]);
	}
	public function memlist($id)
	{
		$memlist = $this->Admin_model->memlist($id);
		$id = $this->session->userdata('busid');
		//$servlist = $this->Usermodel->servicelist($id);
		$products = $this->Admin_model->productlist($id);
		//print_r($products); die;
		//$this->load->view('user/business/businesspro',['result'=>$result,'servlist'=>$servlist,'products'=>$products]);
		$this->load->view('admin/memlist',['memlist'=>$memlist,'products'=>$products]);
	}
	public function products($id)
	{
		$memlist = $this->Admin_model->memlist($id);
		$products = $this->Admin_model->productlist($id);
		$this->load->view('admin/products',['memlist'=>$memlist,'products'=>$products]);
	}
	public function getproduct($id)
	{
		$data = $this->Admin_model->get_product($id);
		$results = $this->Admin_model->servender();
		$this->load->view('admin/updateproduct',['data'=>$data,'results'=>$results]);
	}
	public function updateproduct($id)
	{
				$data['category_id'] = $this->input->post('service');
				$data['title'] = $this->input->post('title');
				$data['description'] = $this->input->post('description');
				$data['sale_price'] = $this->input->post('sale_price');
				$data['actual_price'] = $this->input->post('actual_price');
				$data['status'] = $this->input->post('status');
				$result = $this->Admin_model->update_pro($data,$id);
		if($result){
			$this->session->set_flashdata('success','Product Update Successfully');
				$this->getproduct($id);
		}else{
			$this->session->set_flashdata('success','Product not Update Successfully');
				$this->getproduct($id);
		}
	}
	public function delproduct($id){
		$result = $this->Admin_model->del_pro($id);
		if($result){
				$this->session->set_flashdata('success','Product Update Successfully');
				$this->products($id);
		}
	}
	public function del_pro_services()
	{
		 $result = $this->Admin_model->del_Cat($id);
			if($result){
				$this->session->set_flashdata('success','Product Successfully Deleted');
				return redirect('dashboard/productlist');
		}
	}
	public function deldashproduct($id)
	{
		 $result = $this->Admin_model->deldaspro($id);
			if($result){
				$this->session->set_flashdata('success','Product Successfully Deleted');
				return redirect('dashboard');
		}
	}
	public function productlist()
	{
		$result = $this->Admin_model->activepro();
		$this->load->view('admin/productlist',['result'=>$result]);
	}
	public function editshopproduct($id)
	{
		$procatlist = $this->Admin_model->catlist();
		$prolist = $this->Admin_model->shoppro_list($id);
		$this->load->view('admin/editshopproduct',['procatlist'=>$procatlist,'prolist'=>$prolist]);
	}
	public function updateshoppro($id)
	{
		if($_FILES['proimg']['name']){
			 	$config['upload_path']          = './assets/image/product/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 10000;
				$this->load->library('upload', $config);
				$imgpath = $this->upload->do_upload('proimg');
			 if ( ! $this->upload->do_upload('proimg'))
                {
                        $error = array('error' => $this->upload->display_errors());
                       print_r($error); die;
                }else{
					$image_name = $this->upload->data(); 
					//print_r($image_name['file_name']); die;
					$data['proimg'] = $image_name['file_name'];
				}
		}
		$data['category_id'] =$this->input->post('category_id');
		$data['title'] =$this->input->post('title');
		$data['sale_price'] =$this->input->post('sale_price');
		$data['actual_price'] =$this->input->post('actual_price');
		$data['description'] =$this->input->post('description');
		$data['status'] =$this->input->post('status');
		$result = $this->Admin_model->editshoppro($id,$data);
		if($result){
		$this->session->set_flashdata('success','Product Successfully Deleted');
		return redirect('dashboard/productlist');
		}
	}
	public function getstatus($id)
	{
		$result = $this->Admin_model->pro_status($id);
		if($result->status == 1){
		$status = $this->Admin_model->upprostatus0($id);
		$this->productlist();
		}elseif($result->status == 0){
		$status = $this->Admin_model->upprostatus1($id);
		$this->productlist();
		}
	}

	public function productdel($id)

	{

		$result = $this->Admin_model->pro_del($id);

			if($result){

				$this->session->set_flashdata('success','Product Successfully Deleted');

				return redirect('productlist');

	}

	

	}

	public function activepro($id)

	{

		$result = $this->Admin_model->actdashpro($id);

		if($result){

				$this->session->set_flashdata('success','Product Activeed Successfully');

				return redirect('dashboard');

		}

	}

	public function edit_mem($id)

	{

		$data = $this->Admin_model->editmem($id);

		$results = $this->Admin_model->servender();

		$this->load->view('admin/editmem',['data'=>$data,'results'=>$results]);

	}

	public function update_mem($id)

	{

		if($this->form_validation->run('membber')){

			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');

			$this->edit_mem($id);

		}else{

				$data['names'] = $this->input->post('fname');

				$data['address'] = $this->input->post('address');

				$data['email'] = $this->input->post('email');

				$data['password'] = $this->input->post('password');

				$data['mobile'] = $this->input->post('mobile');

				$data['status'] = $this->input->post('status');

				$data['mem_type'] = $this->input->post('memtype');

				$data['member_from'] = $this->input->post('memform');

			$result = $this->Admin_model->updatemem($data,$id);

			if($result){

				$this->session->set_flashdata('success','Member Profile Update Successfully');

				$this->edit_mem($id);

			}else{

				$this->session->set_flashdata('success','Member Profile Not Update Yet');

				$this->edit_mem($id);

			}

		}

	}

	public function del_mem($id)

	{

		$result = $this->Admin_model->delmem($id);

		if($result){

				$this->session->set_flashdata('success','One Record Deleted Successfully');

				$this->memlist($id);

			//return redirect('dashboard/memlist($id)');

			}else{

				$this->session->set_flashdata('success','Member Profile Not Deleted Yet');

			}

		$this->load->view('admin/mem');

	}

	public function category()

	{

		$this->load->view('admin/add_procat');

	}

	public function insert_category()

	{

		if(empty($_FILES['catimg']['name'])){

			$this->form_validation->set_rules('catimg','Category Image','required');

		}

		if($this->form_validation->run('category')==FALSE){

				$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');

				$this->category();

			}else{

                $config['upload_path']          = './assets/image/category';

                $config['allowed_types']        = 'gif|jpg|png|jpeg';

                $config['max_size']             = 10000;

             

				$this->load->library('upload', $config);

				//print_r($config); die;



                if ( ! $this->upload->do_upload('catimg'))

                {

                        $error = array('error' => $this->upload->display_errors());



                       print_r($error); die;

                }else{

					$image_name = $this->upload->data(); 

					//print_r($image_name['file_name']); die;

					$data['prod_cat_image'] = $image_name['file_name'];

					$data['prod_cat_title'] = $this->input->post('title');

					$data['prod_cat_description'] = $this->input->post('description');

					$data['prod_cat_status'] = 0;

					$data['prod_cat_add_date'] = time();

					

					$result = $this->Admin_model->insert_cat($data);

					if($result){

						$this->session->set_flashdata('success','New Category Added Successfully');

						return redirect('Dashboard/category');

					}else{

						$this->session->set_flashdata('success','Not Added Yet Try again latter');

						return redirect('Dashboard/category');

					}

				}

		}

	}

	public function category_list()

	{

		$result = $this->Admin_model->catlist();

		$this->load->view('admin/catlist',['result'=>$result]);

	}

	public function edit_cat($id)

	{

		$result = $this->Admin_model->editcat($id);

		$this->load->view('admin/edit_cat',['result'=>$result]);

	}

	public function updateCat($id)

	{

		if($_FILES['catimg']['name']){

			 	$config['upload_path']          = './assets/image/category/';

                $config['allowed_types']        = 'gif|jpg|png|jpeg';

                $config['max_size']             = 10000;

             

				$this->load->library('upload', $config);

			 if ( ! $this->upload->do_upload('catimg'))

                {

                        $error = array('error' => $this->upload->display_errors());



                       print_r($error); die;

                }else{

					$image_name = $this->upload->data(); 

					//print_r($image_name['file_name']); die;

					$data['prod_cat_image'] = $image_name['file_name'];

				}

		}

		if($this->form_validation->run('category')==FALSE){

				$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');

				return $this->edit_cat($id);

			}else{


					$data['prod_cat_title'] = $this->input->post('title');

					$data['prod_cat_description'] = $this->input->post('description');

					$data['prod_cat_status'] = $this->input->post('status');

			//print_r($data); die;

			$result = $this->Admin_model->update_cat($data,$id);

					if($result){

						$this->session->set_flashdata('success','New Category updated Successfully');

						return redirect('dashboard/category_list');

					}else{

						$this->session->set_flashdata('success','Not Added Yet Try again latter');

						return $this->category_list($id);

					}

		}

	}

	public function deleteCat($id)

	{

		$result = $this->Admin_model->del_cat($id);

		if($result){

				$this->session->set_flashdata('success','One Record Deleted Successfully');

				$this->category_list($id);

			}else{

				$this->session->set_flashdata('success','Record Not Deleted Yet');

				$this->category_list($id);

			}

	}
	public function manageservices()
	{
		
		$servicelist = $this->Admin_model->service_list();
		$this->load->view('admin/servicelist',['servicelist'=>$servicelist]);
	}
	public function editservices($id)
	{
		$getdata = $this->Admin_model->editservice($id);
		$this->load->view('admin/updateservice',['getdata'=>$getdata]);
		
	}
	public function updateservice($id)
	{
		if($_FILES['serviceimg']['name']){
				$config['upload_path']          = './assets/image/adminservice/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 10000;
			$this->load->library('upload',$config);
			if ( ! $this->upload->do_upload('serviceimg'))

                {

                        $error = array('error' => $this->upload->display_errors());



                       print_r($error); die;

                }else{

					$image_name = $this->upload->data(); 

					//print_r($image_name['file_name']); die;

					$data['serviceimg'] = $image_name['file_name'];
					//$data['prod_cat_image'] = $image_name['serviceimg'];

				}
		}
			
		
			//$image_name = $this->upload->data();
			$data['name'] = $this->input->post('name'); 
			$data['ser_code'] = $this->input->post('ser_code');
			$data['description'] = $this->input->post('description');
			$result = $this->Admin_model->update_service($data,$id);
			if($result){
				$this->session->set_flashdata('success','Service update Successfully');
						return redirect('dashboard/manageservices');
					}else{
						$this->session->set_flashdata('success','Not Added Yet Try again latter');
						return redirect('dashboard/manageservices');
			}
		}
	
	public function delservices($id)
	{
		$delresult = $this->Admin_model->del_service($id);
		if($delresult){
			$this->session->set_flashdata('success','One Record Deleted Successfully');
			$this->manageServices();
		}
	}

	public function customer()

	{

		$result = $this->Admin_model->customerlist();

		$this->load->view('admin/customerlist',['result'=>$result]);

	}
	public function deleteCustomer($id)
	{
		$result = $this->Admin_model->delete_Customer($id);
		if($result){
					$this->session->set_flashdata('success','One Customer Deleted Successfully');

						return redirect('dashboard/customer');

					}
	}

	public function subscribe()

	{

		$result = $this->Admin_model->sublist();

		$this->load->view('admin/subscribelist',['result'=>$result]);

	}

	public function del_sub($id)

	{

		$result = $this->Admin_model->delsub($id);

		if($result){

				$this->session->set_flashdata('success','One Record Deleted Successfully');

				return redirect('Dashboard/subscribe');

			}else{

				$this->session->set_flashdata('success','Record Not Deleted Yet');

				return redirect('Dashboard/subscribe');

			}

	}

	public function logout()

	{

		$this->session->unset_userdata('id');

		return redirect('admin_login');

	}

}

?>









































