<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Business_Member extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		ob_start();
		if(!$id = $this->session->userdata('id')){
		return redirect('Business');
		}
		$this->load->model("Usermodel");
	}
	public function index()
	{
		$id = $this->session->userdata('id');
		$result = $this->Usermodel->get_member($id);
		$this->load->view('user/business/businesspro',['result'=>$result]);
	}
	public function logout()
	{
		$this->session->unset_userdata('id');
		return redirect('Business');
	}
	
	
}
	
?>