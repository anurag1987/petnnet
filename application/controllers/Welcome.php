<?php defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->model("Usermodel");
	}

	public function index()
	{
		$this->load->model('Admin_model');
		$services = $this->Admin_model->service_list();
		$this->load->view('user/index',['services'=>$services]);
	}
	public function dailytips()
	{
		$this->load->view("user/daily_tips");
	}

	public function petcharity()
	{
		$this->load->view("user/pet_charity");
	}

	public function lostfound()
	{
		$this->load->view("user/Lost_found");
	}

	public function contact()
	{
		$this->load->view("user/contact");
	}

	public function about()
	{
		$this->load->view("user/about");
	}

	public function petshop()
	{
		$this->load->view("user/shop");
	}

	public function petdoc()
	{
		$this->load->view("user/doctors");
	}

	public function pettrainers()
	{
		$this->load->view("user/trainers");
	}

	public function pethostel()
	{
		$this->load->view("user/hostel");
	}

	public function petsitters()
	{
		$this->load->view("user/sitters");
	}

	public function pettravel()
	{
		$this->load->view("user/travel");
	}

	public function petdiary()
	{
		$this->load->view("user/diary");
	}

	public function petbreed()
	{
		$this->load->view("user/breed");
	}

	public function petgroom()
	{
		$this->load->view("user/groom");
	}

	public function petevent()
	{
		$this->load->view("user/event");
	}

	public function peteducation()
	{
		$this->load->view("user/education");
	}

	public function petfood()
	{
		$this->load->view("user/food");
	}

	public function businessus()
	{
		$this->load->view("user/business-with-us");
	}

	public function payment()
	{
		$this->load->view("user/payment-options");
	}

	public function privacy()
	{
		$this->load->view("user/privacy-policy");
	}

	public function terms()
	{
		$this->load->view("user/terms-conditions");
	}

	public function forgotpassword()
	{
		$this->load->view("user/resetpassword");
	}

	public function products()
	{
		$result = $this->Usermodel->product_list();
		$this->load->view("user/products",['result'=>$result]);
	}
	public function productDetalis($id)
	{
		//echo $id; die;
		$this->load->view('user/productdetails');
	}
	public function getpassword()
	{
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('mobile','Mobile','required');
		if($this->form_validation->run()==FALSE){
			$this->form_validation->set_error_delimiters('<div style="color:red">','</div>');
			$this->forgotpassword();//return redirect('Welcome/forgotpassword');
		}else{
		$email = $this->input->post('email');
		$mobile = $this->input->post('mobile');
		$result = $this->Usermodel->getpassword($email,$mobile);
		if($result){
			$email =  $result->email;
			$password = $result->password;
			$name = $result->names;
			//echo $email.$password;
			$massage = '
			<div style="height:auto; width:450px; margin-left:auto; margin-right:auto; overflow:hidden; border:1px solid #d4d3d3;">

			  <div style="height:auto; width:430px; padding:10px;">

				<div style="height:80px; width:430px; margin-bottom:5px; background-color:#FBD431;"><a href="http://192.163.247.171/~zopguru/"><img src="http://petnnet.com/images/logo.png" border="0" height="80" /></a></div>

				<div style="height:auto; width:430px; line-height:14px; font-size:10px; color:#747474; margin-bottom:10px;"></div>

				<div style="height:auto; width:408px; padding:10px; border-left:1px solid #d4d2d2; border-right:1px solid #d4d2d2;">

				  <table width="408" border="0" cellspacing="0" cellpadding="0">

					<tr>

					  <td align="left" valign="top"><table width="100%" border="0" cellspacing="2" cellpadding="2">

						  <tr>

							<td width="33%"><strong>Message for PetNnet</strong></td>

						  </tr>

						  <tr>

							<td>&nbsp;</td>

						  </tr>

						  <tr>

							<td><strong>Login Details:</strong></td>

						  </tr>

						  <tr>

							<td>&nbsp;</td>

						  </tr>

						  <tr>

							<td>Subject: Get Email & Password </td>

						  </tr>

						  <tr>

							<td>&nbsp;</td>

						  </tr>

						  <tr>

							<td> Name: '.$name.'</td>

						  </tr>

						  <tr>

							<td>Email-Id: '.$email.'</td>

						  </tr>

						  <tr>

							<td>Password: '.$password.'</td>

						  </tr>

						  <tr>

							<td>&nbsp;</td>

						  </tr>

						  <tr>

							<td>&nbsp;</td>

						  </tr>

						</table></td>

					</tr>

				  </table>

				</div>

				<div style="height:auto; width:428px; border:1px solid #bbbbbb; overflow:hidden;"></div>

			  </div>

			</div>';

					$petnntmail = 'donotreply@petNnet.com';

					$petnnetname = 'petNnet.com';

					$this->load->library('email');

					

					$this->email->from($petnntmail);

					$this->email->to($email);

					$this->email->subject('Registraion Greeting');

					

					$this->email->message($massage);

					$this->email->set_newline("\r\n");

			$this->session->set_flashdata('error','Your emailid & Password send on register email id. Login Now');

			return redirect('Member_login');

					if(!$this->email->send()){

						show_error($this->email->print_debugger());

					}

		}else{

			$this->session->set_flashdata('error','User email & Password are not valid, please try again');

			return redirect('Welcome/forgotpassword');

		}

		}

	}

	public function subscribe()

	{

		 $email = $this->input->post('email');

		 $data['emailid'] = $email;

		 $data['created_at'] = time();

		 $data['status'] = 0;

		$result = $this->Usermodel->sub($data);

		echo "<h4 style='color:white;'>Thank you for Subscribing</h4>";

		}

}

	





























