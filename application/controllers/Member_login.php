<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Member_login extends MY_Controller{
	public function __construct()
	{parent::__construct();
		ob_start();
		if($this->session->userdata('memid')){
			return redirect('Member');
		}
	 	$this->load->model("Usermodel");
	 	$this->load->helper('email');
	 
	}
	public function index()
	{
		$this->load->view('user/member_reg');
	}
	public function add_member()
	{
		$email = $this->input->post('email');
		if($checkemail = $this->Usermodel->checkmail($email)){
		$this->session->set_flashdata('error','This email id Already Registered!');
		$this->index();
		}else
		
		if($this->form_validation->run('usermem')==FALSE)
		{
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');
			$this->index();
		}else{
				$config['upload_path']          = './assets/image/memimg/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10000;
              
                $this->load->library('upload', $config);
				$this->upload->do_upload('memimg');

					$file_name = $this->upload->data();
					$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    				$pass = array(); //remember to declare $pass as an array
					$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
					for ($i = 0; $i < 8; $i++) {
					$n = rand(0, $alphaLength);
					$pass[] = $alphabet[$n];}
					$password = implode($pass);
					$email = $this->input->post('email');
						
					$data['mem_image'] = $file_name['file_name'];
					$data['names'] = $this->input->post('name');
					$data['email'] = $email;
					$data['password'] = $password;
					$data['mobile'] = $this->input->post('mobile');
					$data['status'] = 0;
					$data['add_date'] = time();
					$data['mem_type'] = $this->input->post('category');
					$data['address'] = $this->input->post('address');
					$data['otppass'] = '';
					$data['member_from'] = '';
					$massage = 'THANK YOU FOR REGISTERTION, YOU ARE RECEIVING THIS EMAIL BECAUSE YOU ARE SUCCESSFULLY REGISTERED IN PETNNET.COM. PLEASE DO NOT REPLY TO THIS MAIL. THIS IS AN AUTO GENERATED MAIL AND REPLIES TO THIS EMAIL ID ARE NOT ATTENDED TO. ADD donotreply@petNnet.com TO YOUR REGISTER EMAIL ID: '.$email.' AND PASSWORD: '.$password.' GET THIS LOGIN DETAILS AN LOGIN NOW.';
					$petnntmail = 'donotreply@petNnet.com';
					$petnnetname = 'petNnet.com';
					$this->load->library('email');
					
					$this->email->from($petnntmail);
					$this->email->to($email);
					$this->email->subject('Registraion Greeting');
					
					$this->email->message($massage);
					$this->email->set_newline("\r\n");
					if(!$this->email->send()){
						show_error($this->email->print_debugger());
					}
					$result = $this->Usermodel->addmem($data);
					if($result){
						$this->session->set_flashdata('success','New Member Registered Successfully, Your Email & Password send On your Registered Email ID');
						return redirect('Member_login');
					}else{
						$this->session->set_flashdata('error','Member not Registered Yet');
						$this->index();
					}
					
				}
		}
	public function member_log()
	{
		$this->form_validation->set_rules('username',' User Name or Email-id','required|valid_email');
		$this->form_validation->set_rules('password','Password','required|min_length[6]');
		if($this->form_validation->run()==FALSE){
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			return $this->index();
		}else{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			$result = $this->Usermodel->memlogin($user,$pass);
			if($result){
				$id = $result->memid;
				$this->session->set_userdata('memid',$id);
				return redirect('Member');
			}else{
				$this->session->set_flashdata('error','Invalid Username Password, Please Try again');
				return redirect('Member_login');
			}
		}
	}
}
?>