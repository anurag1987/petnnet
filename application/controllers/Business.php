<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ob_start();

class Business extends MY_Controller {

	public function __construct()

	{

		parent::__construct();

		ob_start();

		if(!$id = $this->session->userdata('busid')){

		return redirect('Business_Logn');

		}

		$this->load->model("Usermodel");

		

	}

	public function index()

	{

		$id = $this->session->userdata('busid');

		$result = $this->Usermodel->get_business($id);

		$servlist = $this->Usermodel->servicelist($id);

		$products = $this->Usermodel->productlist($id);

		$this->load->view('user/business/businesspro',['result'=>$result,'servlist'=>$servlist,'products'=>$products]);

	}

	public function profile()

	{

		$id = $this->session->userdata('busid');

		$result = $this->Usermodel->get_business($id);

		$this->load->view('user/business/updateprofile',['result'=>$result]);

	}

	public function update($id)

	{

		

		$data['names'] = $this->input->post('names');

		$data['email'] = $this->input->post('email');

		$data['mobile'] = $this->input->post('mobile');

		$data['country'] = $this->input->post('country');

		$data['state'] = $this->input->post('state');

		$data['city'] = $this->input->post('city');

		$data['address'] = $this->input->post('address');

		$data['mem_type'] = $this->input->post('mem_type');

		$result = $this->Usermodel->updateprofile($id,$data);

		if($result){

			$this->session->set_flashdata('success','Profile Successfully Updated.');

			return redirect('business/profile');

			}else{

			$this->session->set_flashdata('error','Profile not Updated, try again latter!');

			return redirect('business/profile');

		}

	}

	public function updatepassword()

	{

		$id = $this->session->userdata('busid');

		$result = $this->Usermodel->get_business($id);

		$this->load->view('user/business/change-password',['result'=>$result]);

	}

	public function changerpassword()

	{

		$this->form_validation->set_rules('oldpass','Old Password','required|min_length[5]');

		$this->form_validation->set_rules('newpass','New Password','required|min_length[5]');

		$this->form_validation->set_rules('confirmpass','Confirm Password','required|min_length[5]');

		if ($this->form_validation->run() == FALSE){

			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');

			$this->updatepassword();

		}else{

			$currentpass = $this->input->post('oldpass');

			$newpass = $this->input->post('newpass');

			$confirmpass = $this->input->post('confirmpass');

			$id = $this->session->userdata('busid');

			$getpassword = $this->Usermodel->getbusinesspass($id);

			if($getpassword->password == $currentpass){

				if($newpass == $confirmpass){

					$result = $this->Usermodel->updatebusinesspass($newpass,$id);

					if($result){

						$this->session->set_flashdata('success','Password Updated Successfully.');

				return redirect('business/updatepassword');

					}else{

						$this->session->set_flashdata('error','Failed update password.');

				return redirect('business/updatepassword');	

					}

				}else{

				$this->session->set_flashdata('error','Sorry! Your New Password & Confirm Password is not Matched.');

				return redirect('business/updatepassword');	

				}

			}else{

				$this->session->set_flashdata('error','Sorry! Your Current Password is not Valid.');

				return redirect('business/updatepassword');

			}

		}

	}

	public function mngservice()

	{

		$id = $this->session->userdata('busid');

		$result = $this->Usermodel->get_business($id);

		$servlist = $this->Usermodel->allservice($id);

		$this->load->view('user/business/allservices',['result'=>$result,'servlist'=>$servlist]);

	}

	public function services()

	{

		$id = $this->session->userdata('busid');

		$result = $this->Usermodel->get_business($id);

		$this->load->view('user/business/mngservice',['result'=>$result]);

	}

	public function addservice()

	{

		$this->form_validation->set_rules('title','Title','required');

		$this->form_validation->set_rules('description','Description','required');

		if ($this->form_validation->run() == FALSE){

			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');

			$this->mngservice();

		}else{

			

			$config['upload_path'] = './assets/image/services/';

			$config['allowed_types'] = 'gif|jpg|png|jpeg';

			$config['max_size'] = 3000;

			$this->load->library('upload', $config);

			$this->upload->do_upload('serimg');



            		$filename = $this->upload->data();

			

			$data['serv_img'] = $filename['file_name'];

			$id = $this->session->userdata('busid');

			$data['memid'] = $id;

			$data['serv_title'] = $this->input->post('title');

			$data['serv_desc'] = $this->input->post('description');

			$data['created_at'] = time();

			$data['status'] = 0;

			$result = $this->Usermodel->add_service($data);

			if($result){

				$this->session->set_flashdata('success','New Service Added Successfully');

			return redirect('business/mngservice');

			}else{

				$this->session->set_flashdata('success','New Service Not Added yet try again!');

			return redirect('business/mngservice');

			}

		}

	}

	public function delservice($id)

	{

		$result = $this->Usermodel->delete_srv($id);

		if($result){

			$this->session->set_flashdata('success','Record Successfully Deleted');

			return redirect('business/mngservice');

			}else{

			$this->session->set_flashdata('error','Record not Deleted try again latter!');

			return redirect('business/mngservice');

		}

	}

	public function manageproduct()

	{

		$id = $this->session->userdata('busid');

		$result = $this->Usermodel->get_business($id);

		$products = $this->Usermodel->allproducts($id);

		$this->load->view('user/business/productlist',['result'=>$result,'products'=>$products]);

		

	}

	public function addproduct()

	{

		$id = $this->session->userdata('busid');

		$result = $this->Usermodel->get_business($id);

		$procategory = $this->Usermodel->procategory();

		$this->load->view('user/business/addproduct',['result'=>$result,'procategory'=>$procategory]);

	}

	public function insertproduct()

	{

		

		$this->form_validation->set_rules('category','Category','required');

		$this->form_validation->set_rules('title','Title','required');

		$this->form_validation->set_rules('description','Description','required');

		$this->form_validation->set_rules('saleprice','Sale Price','required');

		$this->form_validation->set_rules('actualprice','Actual Price','required');

		if($this->form_validation->run()==FALSE){

			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');

			$this->addproduct();

		}else{

			

			$config['upload_path'] = './assets/image/product/';

			$config['allowed_types'] = 'gif|jpg|png|jpeg';

			$config['max_size'] = 3000;

			$this->load->library('upload', $config);

			$this->upload->do_upload('proimg');



            		$filename = $this->upload->data();

			

			$data['proimg'] = $filename['file_name'];

			$data['category_id'] = $this->input->post('category');

			$data['memid'] = $this->session->userdata('busid');

			$data['title'] = $this->input->post('title');

			$data['description'] = $this->input->post('description');

			$data['sale_price'] = $this->input->post('saleprice');

			$data['actual_price'] = $this->input->post('actualprice');

			$data['created_at'] = time();

			$data['status'] = 0;

			$result = $this->Usermodel->addproduct($data);

			if($result){

			$this->session->set_flashdata('success','New Product Added Successfully');

			return redirect('business/manageproduct');

			}else{

			$this->session->set_flashdata('error','Product Not Added yet try again!');

			return redirect('business/addproduct');

			}

		}

	}

	public function delproduct($id)

	{

		$result = $this->Usermodel->delete_pro($id);

		if($result){

			$this->session->set_flashdata('success','Record Successfully Deleted');

			return redirect('business/manageproduct');

			}else{

			$this->session->set_flashdata('error','Record not Deleted try again latter!');

			return redirect('business/manageproduct');

		}

	}

	public function logout()

	{

		$this->session->unset_userdata('busid');

		return redirect('businesslogin');

	}

	

	

}

	

?>