<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Member extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		ob_start();
		if(!$id = $this->session->userdata('memid')){
		return redirect('Member_login');
		}
		$this->load->model("Usermodel");
		$results = $this->Usermodel->servender();
		$this->load->view('user/header',['results'=>$results]);
	}
	public function index()
	{
		$id = $this->session->userdata('memid');
		$result = $this->Usermodel->get_member($id);
		$this->load->view('user/member/mem_profile',['result'=>$result]);
	}
	public function profile()
	{
		$id = $this->session->userdata('memid');
		$result = $this->Usermodel->get_member($id);
		$this->load->view('user/member/updatemem',['result'=>$result]);
	}
	public function updateprofile($id)
	{
		$data['names'] = $this->input->post('names');
		$data['email'] = $this->input->post('email');
		$data['mobile'] = $this->input->post('mobile');
		$data['country'] = $this->input->post('country');
		$data['state'] = $this->input->post('state');
		$data['city'] = $this->input->post('city');
		$data['address'] = $this->input->post('address');
		$data['mem_type'] = $this->input->post('mem_type');
		$result = $this->Usermodel->upmemprofile($id,$data);
		if($result){
			$this->session->set_flashdata('success','Profile Successfully Updated.');
			return redirect('member/profile');
			}else{
			$this->session->set_flashdata('error','Profile not Updated, try again latter!');
			return redirect('member/profile');
		}
	}
	public function updatepassword()
	{
		$id = $this->session->userdata('memid');
		$result = $this->Usermodel->get_member($id);
		$this->load->view('user/member/change-password',['result'=>$result]);
	}
	public function changerpassword()
	{
		$this->form_validation->set_rules('oldpass','Old Password','required|min_length[5]');
		$this->form_validation->set_rules('newpass','New Password','required|min_length[5]');
		$this->form_validation->set_rules('confirmpass','Confirm Password','required|min_length[5]');
		if ($this->form_validation->run() == FALSE){
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');
			$this->updatepassword();
		}else{
			$currentpass = $this->input->post('oldpass');
			$newpass = $this->input->post('newpass');
			$confirmpass = $this->input->post('confirmpass');
			$id = $this->session->userdata('memid');
			$getpassword = $this->Usermodel->getmempass($id);
			if($getpassword->password == $currentpass){
				if($newpass == $confirmpass){
					$result = $this->Usermodel->updatemem($newpass,$id);
					if($result){
						$this->session->set_flashdata('success','Password Updated Successfully.');
				return redirect('member/updatepassword');
					}else{
						$this->session->set_flashdata('error','Failed update password.');
				return redirect('member/updatepassword');	
					}
				}else{
				$this->session->set_flashdata('error','Sorry! Your New Password & Confirm Password is not Matched.');
				return redirect('member/updatepassword');	
				}
			}else{
				$this->session->set_flashdata('error','Sorry! Your Current Password is not Valid.');
				return redirect('member/updatepassword');
			}
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('memid');
		return redirect('member_login');
	}
	
	
}
	
?>