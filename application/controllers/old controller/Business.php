<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Business extends MY_Controller{
	public function __construct()
	{parent::__construct();
		ob_start();
		if($this->session->userdata('id')){
			return redirect('Business_Member');
		}
		$this->load->model("Usermodel");
	}
	public function index()
	{
		$this->load->model('Admin_model');
		$results = $this->Admin_model->servender();
		$this->load->view('user/businessreg',['results'=>$results]);
	}
	public function business_mem()
	{
		if(empty($_FILES['memimg']['name'])){
			$this->form_validation->set_rules('memimg','Image','required');
		}
		if($this->form_validation->run('business')==FALSE)
		{
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');
			$this->index();
		}else{
				//echo "validation error"; die;
			$config['upload_path']          = './assets/image/business/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10000;
              
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('memimg'))
                {
                        $error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload_form', $error);
                }else{
					$file_name = $this->upload->data();
					$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    				$pass = array(); //remember to declare $pass as an array
					$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
					for ($i = 0; $i < 8; $i++) {
					$n = rand(0, $alphaLength);
					$pass[] = $alphabet[$n];}
						
					$data['mem_image'] = $file_name['file_name'];
					$data['name'] = $this->input->post('name');
					$data['email'] = $this->input->post('email');;
					$data['password'] = implode($pass);
					$data['mobile'] = $this->input->post('mobile');
					$data['status'] = 0;
					$data['add_date'] = time();
					$data['mem_type'] = $this->input->post('category');
					$data['address'] = $this->input->post('address');
					$data['otppass'] = '';
					$data['member_from'] = '';
					//print_r($data); die;
					
					$result = $this->Usermodel->business_mem($data);
					if($result){
						$this->session->set_flashdata('success','New Business Member Registered Successfully, Your Email & Password send On your Registered Email ID');
						return redirect('Business');
					}else{
						$this->session->set_flashdata('success','Member not Registered Yet');
						$this->index();
					}
					
				}
		}
	}
	public function business_log()
	{
		$this->form_validation->set_rules('username',' User Name or Email-id','required|valid_email');
		$this->form_validation->set_rules('password','Password','required|min_length[5]');
		if($this->form_validation->run()==FALSE){
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->index();//return redirect('admin_login');
		}else{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			$result = $this->Usermodel->buslogin($user,$pass);
			if($result){
				$id = $result->memid;
				$this->session->set_userdata('id',$id);
				return redirect('Business_Member');
			}else{
				$this->session->set_flashdata('error','Invalid Username Password, Please Try again');
				return redirect('Business');
			}
		}
		//$this->load->view('user/mem_profile.php');
	}
}
?>