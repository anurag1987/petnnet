<?php
  if (!defined('BASEPATH')) exit('No direct script access allowed');
  ob_start();
class dashboard extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		ob_start();
		if(!$id = $this->session->userdata('id')){
		return redirect('admin_login');
		}
		
		$this->load->model("Admin_model");
		$results = $this->Admin_model->servender();
		$this->load->view("admin/menu",['results'=>$results]);
	}
	public function index()
	{
		$this->load->view("admin/dashboard");
	}
	public function memlist($id)
	{
		$memlist = $this->Admin_model->memlist($id);
		$this->load->view('admin/memlist',['memlist'=>$memlist]);
	}
	public function edit_mem($id)
	{
		$data = $this->Admin_model->editmem($id);
		$results = $this->Admin_model->servender();
		$this->load->view('admin/editmem',['data'=>$data,'results'=>$results]);
	}
	public function update_mem($id)
	{
		if($this->form_validation->run('membber')){
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');
			$this->edit_mem($id);
		}else{
				$data['names'] = $this->input->post('fname');
				$data['address'] = $this->input->post('address');
				$data['email'] = $this->input->post('email');
				$data['password'] = $this->input->post('password');
				$data['mobile'] = $this->input->post('mobile');
				$data['status'] = $this->input->post('status');
				$data['mem_type'] = $this->input->post('memtype');
				$data['member_from'] = $this->input->post('memform');
			$result = $this->Admin_model->updatemem($data,$id);
			if($result){
				$this->session->set_flashdata('success','Member Profile Update Successfully');
				$this->edit_mem($id);
			}else{
				$this->session->set_flashdata('success','Member Profile Not Update Yet');
				$this->edit_mem($id);
			}
		}
	}
	public function del_mem($id)
	{
		$result = $this->Admin_model->delmem($id);
		if($result){
				$this->session->set_flashdata('success','One Record Deleted Successfully');
				$this->memlist($id);
			}else{
				$this->session->set_flashdata('success','Member Profile Not Deleted Yet');
				$this->memlist($id);
			}
	}
	public function category()
	{
		$this->load->view('admin/add_procat');
	}
	public function insert_category()
	{
		if(empty($_FILES['catimg']['name'])){
			$this->form_validation->set_rules('catimg','Category Image','required');
			
		}
		if($this->form_validation->run('category')==FALSE){
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');
				$this->category();
			}else{
                $config['upload_path']          = './assets/image/category';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 10000;
             
				$this->load->library('upload', $config);
				//print_r($config); die;

                if ( ! $this->upload->do_upload('catimg'))
                {
                        $error = array('error' => $this->upload->display_errors());

                       print_r($error); die;
                }else{
					$image_name = $this->upload->data(); 
					//print_r($image_name['file_name']); die;
					$data['prod_cat_image'] = $image_name['file_name'];
					$data['prod_cat_title'] = $this->input->post('title');
					$data['prod_cat_description'] = $this->input->post('description');
					$data['prod_cat_status'] = 0;
					$data['prod_cat_add_date'] = time();
					
					$result = $this->Admin_model->insert_cat($data);
					if($result){
						$this->session->set_flashdata('success','New Category Added Successfully');
						$this->category();
					}else{
						$this->session->set_flashdata('success','Not Added Yet Try again latter');
						$this->category();
					}
				}
		}
	}
	public function category_list()
	{
		$result = $this->Admin_model->catlist();
		$this->load->view('admin/catlist',['result'=>$result]);
	}
	public function business()
	{
		$result = $this->Admin_model->businesslist();
		$this->load->view('admin/businesslist',['result'=>$result]);
	}
	public function logout()
	{
		$this->session->unset_userdata('id');
		return redirect('admin_login');
	}
}
?>




















