<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Business_Logn extends MY_Controller{
	public function __construct()
	{parent::__construct();
		ob_start();
		if($this->session->userdata('busid')){
			return redirect('Business');
		}
		$this->load->model("Usermodel");
	}
	public function index()
	{
		$this->load->model('Admin_model');
		$results = $this->Admin_model->servender();
		$this->load->view('user/businessreg',['results'=>$results]);
	}
	public function business_mem()
	{
		$email = $this->input->post('email');
		if($checkemail = $this->Usermodel->checbusinesskemail($email)){
		$this->session->set_flashdata('error','This email id Already Registered!');
		$this->index();
			}else
		
		if($this->form_validation->run('business')==FALSE)
		{
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">','</div>');
			$this->index();
		}else{
				//echo "validation error"; die;
				$config['upload_path']          = './assets/image/business/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10000;
              
                	$this->load->library('upload', $config);

            		$file_name = $this->upload->data();
					$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    				$pass = array(); //remember to declare $pass as an array
					$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
					for ($i = 0; $i < 8; $i++) {
					$n = rand(0, $alphaLength);
					$pass[] = $alphabet[$n];}
					$password = implode($pass);
					$email = $this->input->post('email');
					
					$data['mem_image'] = $file_name['file_name'];
					$data['names'] = $this->input->post('name');
					$data['email'] = $email;
					$data['password'] = $password;
					$data['mobile'] = $this->input->post('mobile');
					$data['status'] = 0;
					$data['add_date'] = time();
					$data['mem_type'] = $this->input->post('category');
					$data['address'] = $this->input->post('address');
					$data['otppass'] = '';
					$data['member_from'] = '';
					$massage = 'THANK YOU FOR REGISTERTION, YOU ARE RECEIVING THIS EMAIL BECAUSE YOU ARE SUCCESSFULLY REGISTERED FOR BUSINESS IN PETNNET.COM. PLEASE DO NOT REPLY TO THIS MAIL. THIS IS AN AUTO GENERATED MAIL AND REPLIES TO THIS EMAIL ID ARE NOT ATTENDED TO. ADD donotreply@petNnet.com TO YOUR REGISTER EMAIL ID: '.$email.' AND PASSWORD: '.$password.' GET THIS LOGIN DETAILS AN LOGIN NOW.';
					$petnntmail = 'donotreply@petNnet.com';
					$petnnetname = 'petNnet.com';
					$this->load->library('email');
					
					$this->email->from($petnntmail);
					$this->email->to($email);
					$this->email->subject('Registraion Greeting');
					
					$this->email->message($massage);
					$this->email->set_newline("\r\n");
					if(!$this->email->send()){
						show_error($this->email->print_debugger());
					}
					
					$result = $this->Usermodel->business_mem($data);
					print_r($result);
					if($result){
						$this->session->set_flashdata('success','New Business Member Registered Successfully, Your Email & Password send On your Registered Email ID');
						return redirect('Business_Logn');
					}else{
						$this->session->set_flashdata('error','Member not Registered Yet');
						$this->index();
					}
					
				}
		}
	public function business_log()
	{
		$this->form_validation->set_rules('username',' User Name or Email-id','required|valid_email');
		$this->form_validation->set_rules('password','Password','required|min_length[5]');
		if($this->form_validation->run()==FALSE){
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->index();//return redirect('admin_login');
		}else{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			$result = $this->Usermodel->buslogin($user,$pass);
			//print_r($result); die;
			if($result){
				$id = $result->memid;
				$this->session->set_userdata('busid',$id);
				return redirect('Business');
			}else{
				$this->session->set_flashdata('error','Invalid Username Password, Please Try again');
				return redirect('Business_Logn');
			}
		}
		//$this->load->view('user/mem_profile.php');
	}
	public function password()
	{
		$this->load->view('user/businesspassword');
	}
	public function businesspass()
	{
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('mobile','Mobile','required');
		if($this->form_validation->run()==FALSE){
			$this->form_validation->set_error_delimiters('<div style="color:red">','</div>');
			$this->password();//return redirect('Welcome/forgotpassword');
		}else{
		$email = $this->input->post('email');
		$mobile = $this->input->post('mobile');
		$result = $this->Usermodel->businesspassword($email,$mobile);
		if($result){
			$email =  $result->email;
			$password = $result->password;
			$name = $result->names;
			//echo $email.$password;
			$massage = '
			<div style="height:auto; width:450px; margin-left:auto; margin-right:auto; overflow:hidden; border:1px solid #d4d3d3;">
			  <div style="height:auto; width:430px; padding:10px;">
				<div style="height:80px; width:430px; margin-bottom:5px; background-color:#FBD431;"><a href="http://192.163.247.171/~zopguru/"><img src="http://petnnet.com/images/logo.png" border="0" height="80" /></a></div>
				<div style="height:auto; width:430px; line-height:14px; font-size:10px; color:#747474; margin-bottom:10px;"></div>
				<div style="height:auto; width:408px; padding:10px; border-left:1px solid #d4d2d2; border-right:1px solid #d4d2d2;">
				  <table width="408" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td align="left" valign="top"><table width="100%" border="0" cellspacing="2" cellpadding="2">
						  <tr>
							<td width="33%"><strong>Message for PetNnet</strong></td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td><strong>Login Details:</strong></td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>Subject: Get Email & Password </td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td> Name: '.$name.'</td>
						  </tr>
						  <tr>
							<td>Email-Id: '.$email.'</td>
						  </tr>
						  <tr>
							<td>Password: '.$password.'</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
				  </table>
				</div>
				<div style="height:auto; width:428px; border:1px solid #bbbbbb; overflow:hidden;"></div>
			  </div>
			</div>';
					$petnntmail = 'donotreply@petNnet.com';
					$petnnetname = 'petNnet.com';
					$this->load->library('email');
					
					$this->email->from($petnntmail);
					$this->email->to($email);
					$this->email->subject('Registraion Greeting');
					
					$this->email->message($massage);
					$this->email->set_newline("\r\n");
			
					if(!$this->email->send()){
						show_error($this->email->print_debugger());
					}
			$this->session->set_flashdata('success','Your emailid & Password send on register email id. Login Now');
			return redirect('business_Logn');
		}else{
			$this->session->set_flashdata('success','User email & Password are not valid, please try again');
			return redirect('business_Logn/password');
		}
		}
	}
}
?>