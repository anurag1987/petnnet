  <?php include("header.php"); ?>

  <?php include("menu.php"); ?>

  <main class="main--container">

    <section class="page--header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-6">

            <h2 class="page--title h5">Form </h2>

            <ul class="breadcrumb">

              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard');?>">Dashboard</a></li>

              <li class="breadcrumb-item"><span>Category</span></li>

            </ul>

          </div>

          

        </div>

      </div>

    </section>

    <section class="main--content">

      <div class="row gutter-20">

        <div class="col-md-12">

          <div class="panel">

            <div class="panel-heading">

              <h3 class="panel-title">Update Category for Product</h3>

            </div>

            <?php $arr = []; foreach($result as $value): $arr = $value; endforeach;?>

            <?php echo form_open("Dashboard/updateCat/".$arr->prod_catid,['enctype'=>'multipart/form-data']);?>

             <div class="panel-content">

            <?php if($success = $this->session->flashdata('success')){?>

             <div class="alert-success"><?php echo $success;?></div>

             <?php }?>

                 <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Category Title</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'title','class'=>'form-control','value'=>$arr->prod_cat_title]);?>

				 <?php echo form_error("title");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Description</span>

                <div class="col-md-10">

                  <?php echo form_textarea(['name'=>'description','class'=>'form-control','value'=>$arr->prod_cat_description]);?>

                  <?php echo form_error("description");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Status</span>

                <div class="col-md-10">

                 <select name="status" class="form-control">

                 	<option>Select Status</option>

             		 <option value="0" <?php if($arr->prod_cat_status==0){?>selected <?php }?> > Pending</option>

                 	<option value="1" <?php if($arr->prod_cat_status==1){?>selected <?php }?> >Active</option>

                 </select>

                  </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Category Image</span>

                <div class="col-md-10">

                  <?php echo form_input(['type'=>'file','name'=>'catimg','value'=>$arr->prod_cat_image]);?>

                  <?php echo form_error("catimg");?>

                  <img src="<?php echo base_url('assets/image/category/'.$arr->prod_cat_image);?>" width="150px" height="200px;">

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right"></span>

                <div class="col-md-6">

                  <?php echo form_submit(['name'=>'submit','value'=>'Submit','class'=>'btn btn-success']);?>

                </div>

              </div>

			  </div>

         <?php echo form_close();?>

          </div>

        </div>

      </div>

    </section>

        <?php include("footer.php"); ?>

  </main>

</div>

<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>

<script src="assets/js/perfect-scrollbar.min.js"></script>

<script src="assets/js/jquery.sparkline.min.js"></script>

<script src="assets/js/raphael.min.js"></script>

<script src="assets/js/morris.min.js"></script>

<script src="assets/js/select2.min.js"></script>

<script src="assets/js/jquery-jvectormap.min.js"></script>

<script src="assets/js/jquery-jvectormap-world-mill.min.js"></script>

<script src="assets/js/horizontal-timeline.min.js"></script>

<script src="assets/js/jquery.validate.min.js"></script>

<script src="assets/js/jquery.steps.min.js"></script>

<script src="assets/js/dropzone.min.js"></script>

<script src="assets/js/ion.rangeSlider.min.js"></script>

<script src="assets/js/datatables.min.js"></script>

<script src="assets/js/main.js"></script>

</body>

</html>