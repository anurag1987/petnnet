

  <?php include("header.php"); ?>

  <?php include("menu.php"); ?>

  <main class="main--container">

    <section class="page--header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-6">

            <h2 class="page--title h5">Subscribe List </h2>

            <ul class="breadcrumb">

              <li class="breadcrumb-item"><a href="<?php echo base_url("dashboard");?>">Dashboard</a></li>

              <li class="breadcrumb-item"><span>Member List</span></li>

            </ul>

          </div>

          

        </div>

      </div>

    </section>

    <section class="main--content">

      <div class="row gutter-20">

        <div class="col-xl-12">

          <div class="panel">

            

             <?php if($success = $this->session->flashdata('success')){?>

             <div class="alert-success"><?php echo $success;?></div>

             <?php }?>

            <div class="panel-body">

              <div class="table-responsive">

                <table class="table table-bordered">

                  <thead>

                    <tr>

                      <th>#</th>

                      <th>E-mail</th>


                      <th>Delete</th>

                    </tr>

                  </thead>

                  <tbody>

                   <?php $i = 1; if($result){ foreach($result as $value){ $i++;?>

                     <tr>

                      <td><?php echo $i;?></td>

                      <td><?php echo $value->emailid;?></td>


                      <td><span class="label label-warning"><a href="<?php echo base_url("Dashboard/del_sub/".$value->subid);?>" class="btn-link"> <i class="fa fa-trash"></i></a></span></td>

                    </tr>

                    <?php } 

								}else{ ?>

					<tr>

						<td colspan="8" align="center" class="btn-danger">No Data Available Yet</td>

					</tr>						 

										  

						<?php }?>

                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>

        

        

        

        

        

        

        

      </div>

    </section>

    <?php include("footer.php"); ?>

  </main>

</div>



</body>

</html>