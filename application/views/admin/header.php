<?php ob_start();?>
<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>PetNnet</title>
<meta name="author" content="">
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="icon" href="<?php echo base_url('assets/login_assets/img/logo.png');?>" type="image/x-icon">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7CMontserrat:400,500">
<?php echo link_tag('assets/login_assets/css/bootstrap.min.css');?>
<?php echo link_tag('assets/login_assets/css/fontawesome-all.min.css');?>
<?php echo link_tag('assets/login_assets/css/jquery-ui.min.css');?>
<?php echo link_tag('assets/login_assets/css/perfect-scrollbar.min.css');?>
<?php echo link_tag('assets/login_assets/css/morris.min.css');?>
<?php echo link_tag('assets/login_assets/css/select2.min.css');?>
<?php echo link_tag('assets/login_assets/css/jquery-jvectormap.min.css');?>
<?php echo link_tag('assets/login_assets/css/horizontal-timeline.min.css');?>
<?php echo link_tag('assets/login_assets/css/weather-icons.min.css');?>
<?php echo link_tag('assets/login_assets/css/dropzone.min.css');?>
<?php echo link_tag('assets/login_assets/css/ion.rangeSlider.min.css');?>
<?php echo link_tag('assets/login_assets/css/ion.rangeSlider.skinFlat.min.css');?>
<?php echo link_tag('assets/login_assets/css/datatables.min.css');?>
<?php echo link_tag('assets/login_assets/css/fullcalendar.min.css');?>
<?php echo link_tag('assets/login_assets/css/style.css');?>
</head>
<body>
<div class="wrapper">
   
   <header class="navbar navbar-fixed">
    <div class="navbar--header"> <a href="<?php echo base_url('dashboard');?>" class="logo"> <img src="<?php echo base_url('assets/login_assets/img/logo.png');?>" alt=""> </a> <a href="#" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar"> <i class="fa fa-bars"></i> </a> </div>
    <a href="#" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar"> <i class="fa fa-bars"></i> </a>
    <div class="navbar--search">
      <form action="search-results.php">
        <input type="search" name="search" class="form-control" placeholder="Search Something..." required>
        <button class="btn-link"><i class="fa fa-search"></i></button>
      </form>
    </div>
    <div class="navbar--nav ml-auto">
      <ul class="nav">
        <li class="nav-item dropdown nav--user online"> <a href="#" class="nav-link" data-toggle="dropdown"> <img src="<?php echo base_url('assets/login_assets/img/avatars/01_80x80.png');?>" alt="" class="rounded-circle"> <span>Admin </span> <i class="fa fa-angle-down"></i> </a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-cog"></i>Settings</a></li>
            <li class="dropdown-divider"></li>
            <li><a href="#"><i class="fa fa-lock"></i>Lock Screen</a></li>
            <li><!--<i class="fa fa-power-off"></i><?php echo anchor('dashboard/logout','Logout');?>--><a href="<?php echo base_url('dashboard/logout');?>"><i class="fa fa-power-off"></i>Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </header>
  