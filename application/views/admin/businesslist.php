
  <?php include("header.php"); ?>
  <?php include("menu.php"); ?>
  <main class="main--container">
    <section class="page--header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <h2 class="page--title h5">Member List </h2>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
              <li class="breadcrumb-item"><span>Business Member </span></li>
            </ul>
          </div>
          
        </div>
      </div>
    </section>
    <section class="main--content">
      <div class="row gutter-20">
        <div class="col-xl-12">
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">
                <select name="filter" data-trigger="selectmenu" data-minimum-results-for-search="-1">
                  <option value="top-search">Short By</option>
                  <option value="average-search">Member Name</option>
                  <option value="average-search">Mobile</option>
                  <option value="average-search">Status</option>
                </select>
              </h3>
            </div>
             <?php if($success = $this->session->flashdata('success')){?>
             <div class="alert-success"><?php echo $success;?></div>
             <?php }?>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th> Image</th>
                      <th>Member Name</th>
                      <th>Phone No.</th>
                      <th>E-mail</th>
                      <th>Status</th>
                      <!--<th>Edit</th>
                      <th>Delete</th>-->
                    </tr>
                  </thead>
                  <tbody>
                   <?php $i = 0; if($result){ foreach($result as $value){ $i++;?>
                     <tr>
                      <td><?php echo $i;?></td>
                      <td><img src="<?php echo base_url('assets/image/memimg/'.$value->mem_image);?>" height="100px" width="100px"></td>
                      <td><a href="javascript:void(0)" class="btn-link"><?php echo $value->name;?></a></td>
                      <td><?php echo $value->mobile;?></td>
                      <td><?php echo $value->email;?></td>
                      <td><?php echo $value->status;?></td>
                     <!-- <td><span class="label label-success"> <i class="fa fa-edit"></i> <?php echo anchor('Dashboard/edit_mem/'.$value->memid,'Edit',['class'=>'btn-link']);?></span></td>
                      <td><span class="label label-warning"> <i class="fa fa-trash"></i> <?php echo anchor('Dashboard/del_mem/'.$value->memid,'Delete',['class'=>'btn-link']);?></span></td>-->
                    </tr>
                    <?php } 
								}else{ ?>
					<tr>
						<td colspan="8" align="center" class="btn-danger">No Data Available Yet</td>
					</tr>						 
										  
						<?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        
        
        
        
        
        
        
      </div>
    </section>
    <?php include("footer.php"); ?>
  </main>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/perfect-scrollbar.min.js"></script>
<script src="assets/js/jquery.sparkline.min.js"></script>
<script src="assets/js/raphael.min.js"></script>
<script src="assets/js/morris.min.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/jquery-jvectormap.min.js"></script>
<script src="assets/js/jquery-jvectormap-world-mill.min.js"></script>
<script src="assets/js/horizontal-timeline.min.js"></script>
<script src="assets/js/jquery.validate.min.js"></script>
<script src="assets/js/jquery.steps.min.js"></script>
<script src="assets/js/dropzone.min.js"></script>
<script src="assets/js/ion.rangeSlider.min.js"></script>
<script src="assets/js/datatables.min.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
