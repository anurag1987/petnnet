<? @session_start();?>
    <?php include("header.php"); ?>
  <?php include("menu.php"); ?>
  <main class="main--container">
    <section class="page--header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <h2 class="page--title h5">Dashboard</h2>
            <ul class="breadcrumb">
              <li class="breadcrumb-item active"><span>Dashboard</span></li>
            </ul>
          </div>
          <div class="col-lg-6">
            <div class="summary--widget">
              <div class="summary--item">
                <p class="summary--chart" data-trigger="sparkline" data-type="bar" data-width="5" data-height="38" data-color="#009378">2,9,7,9,11,9,7,5,7,7,9,11</p>
                <p class="summary--title">Total Vendor</p>
                <p class="summary--stats text-green"><?php echo $this->db->count_all_results('petnnet_business')?></p>
              </div>
              <div class="summary--item">
                <p class="summary--chart" data-trigger="sparkline" data-type="bar" data-width="5" data-height="38" data-color="#e16123">2,3,7,7,9,11,9,7,9,11,9,7</p>
                <p class="summary--title">Products</p>
                <p class="summary--stats text-orange"><?php echo $this->db->count_all_results('petnnet_product')?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="main--content">
      <div class="row gutter-20">
        <div class="col-md-4">
          <div class="panel">
            <div class="miniStats--panel">
              <div class="miniStats--header bg-darker">
              <p class="miniStats--chart" data-trigger="sparkline" data-type="bar" data-width="4" data-height="30" data-color="#e16123">2,2,3,9,11,9,7,20,9,7,6,5,6,9,4,9,5,3,5,9,15,3</p>
                <!--<p class="miniStats--label text-white bg-blue"> <i class="fa fa-level-up-alt"></i> <span>10%</span> </p>-->
              </div>
              <div class="miniStats--body"> <i class="miniStats--icon fa fa-user text-blue"></i>
                <p class="miniStats--caption text-blue">Monthly</p>
                <h3 class="miniStats--title h4">New Buyer</h3>
                <p class="miniStats--num text-blue"><?php echo $this->db->count_all_results('petnnet_member')?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel">
            <div class="miniStats--panel">
              <div class="miniStats--header bg-darker">
                <p class="miniStats--chart" data-trigger="sparkline" data-type="bar" data-width="4" data-height="30" data-color="#e16123">2,2,3,9,11,9,7,20,9,7,6,5,6,9,4,9,5,3,5,9,15,3</p>
                <!--<p class="miniStats--label text-white bg-orange"> <i class="fa fa-level-down-alt"></i> <span>10%</span> </p>-->
              </div>
              <div class="miniStats--body"> <i class="miniStats--icon fa fa-ticket-alt text-orange"></i>
                <p class="miniStats--caption text-orange">Monthly</p>
                <h3 class="miniStats--title h4">Products Seller</h3>
                <p class="miniStats--num text-orange"><?php $this->db->like('mem_type', 'Shop');
$this->db->from('petnnet_business');
echo $this->db->count_all_results(); ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel">
            <div class="miniStats--panel">
              <div class="miniStats--header bg-darker">
                <p class="miniStats--chart" data-trigger="sparkline" data-type="bar" data-width="4" data-height="30" data-color="#009378">2,2,3,9,11,9,7,20,9,7,6,5,6,9,4,9,5,3,5,9,15,3</p>
                <!--<p class="miniStats--label text-white bg-green"> <i class="fa fa-level-up-alt"></i> <span>10%</span> </p>-->
              </div>
              <div class="miniStats--body"> <i class="miniStats--icon fa fa-rocket text-green"></i>
                <p class="miniStats--caption text-green">Monthly</p>
                <h3 class="miniStats--title h4">Products Launched</h3>
                <p class="miniStats--num text-green"><?php echo $this->db->count_all_results('petnnet_product')?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-12">
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Product for Approval</h3><br>
                <?php if($success = $this->session->flashdata('success')){?>
             <div class=" alert alert-success"><?php echo $success;?></div>
             <?php }?>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-border">
                  <thead>
                  	<tr>
                  		<th> Business Member</th>
                  		<th>Product Title </th>
                  		<th>Sale Price </th>
                  		<th>Actual Price </th>
                  		<th> Manage</th>
                  	</tr>
                   
                  </thead>
                   <tbody>
                    <?php foreach($productlist as $value):?>
                    <tr>
                      <!--<td><div class="media">
                          <div class="media--img"> <img src="<?php echo base_url('login_assets/img/product/'.$value->proimg);?>" alt=""> </div>
                        </div></td>-->
                      <td><div class="media">
                          <div class="media--info">
                            <h3 class="media--name h5"><?php echo $value->busname;?></h3>
                            <!--<p class="media--desc">Menz Products</p>-->
                          </div>
                        </div></td>
                      <td><?php echo $value->title;?></td>
                      <td><?php echo $value->sale_price;?></td>
                      <td><?php echo $value->actual_price;?></td>
                      <td><a href="<?php echo base_url('dashboard/activepro/'.$value->product_id);?>"> <span class="label label-success">Ok</span></a>
                      <a href="<?php echo base_url('dashboard/deldashproduct/'.$value->product_id);?>"> <span class="label label-warning">Del</span></a></td>
                    </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        
        <!--<div class="col-xl-12">
          <div class="panel">
             
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table style--2">
                  <thead>
                    <tr>
                      <th>Product Image</th>
                      <th>Product ID</th>
                      <th>Customer Name</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>Tracking No.</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><img src="<?php echo base_url('login_assets/img/products/thumb-01.jpg');?>" alt=""></td>
                      <td>3BSD59</td>
                      <td><a href="#" class="btn-link">Customer Name</a></td>
                      <td>$99</td>
                      <td>2</td>
                      <td><span class="text-muted">#BG6R9853lP</span></td>
                      <td><span class="label label-success">Paid</span></td>
                    </tr>
                    <tr>
                      <td><img src="<?php echo base_url('login_assets/img/products/thumb-01.jpg');?>" alt=""></td>
                      <td>3BSD59</td>
                      <td><a href="#" class="btn-link">Customer Name</a></td>
                      <td>$99</td>
                      <td>2</td>
                      <td><span class="text-muted">#BG6R9853lP</span></td>
                      <td><span class="label label-warning">Due</span></td>
                    </tr>
                    <tr>
                      <td><img src="<?php echo base_url('login_assets/img/products/thumb-01.jpg');?>" alt=""></td>
                      <td>3BSD59</td>
                      <td><a href="#" class="btn-link">Customer Name</a></td>
                      <td>$99</td>
                      <td>2</td>
                      <td><span class="text-muted">#BG6R9853lP</span></td>
                      <td><span class="label label-info">Rejected</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>-->
        
        <!--<div class="col-xl-5 col-md-5">
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Overall Rating</h3>
              <div class="dropdown">
                <button type="button" class="btn-link dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-v"></i> </button>
                <ul class="dropdown-menu">
                  <li><a href="#"><i class="fa fa-sync"></i>Update Data</a></li>
                  <li><a href="#"><i class="fa fa-cogs"></i>Settings</a></li>
                  <li><a href="#"><i class="fa fa-times"></i>Remove Panel</a></li>
                </ul>
              </div>
            </div>
            <div class="panel-chart">
              <div class="chart--body line--chart style--4" data-trigger="sparklineChart01">10,15,15,20,18,20,18,22</div>
              <div class="chart--stats style--4">
                <ul class="nav">
                  <li> <span class="text">The product is awesome</span> <span class="stat">20%</span> </li>
                  <li> <span class="text">I am so pleased</span> <span class="stat">40%</span> </li>
                  <li> <span class="text">The product is really good</span> <span class="stat">20%</span> </li>
                  <li> <span class="text">The product is awesome</span> <span class="stat">60%</span> </li>
                  <li> <span class="text">I am so surprised</span> <span class="stat">20%</span> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>-->
        <!--<div class="col-xl-7 col-md-7">
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Events Timeline</h3>
              <div class="dropdown">
                <button type="button" class="btn-link dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-v"></i> </button>
                <ul class="dropdown-menu">
                  <li><a href="#"><i class="fa fa-sync"></i>Update Data</a></li>
                  <li><a href="#"><i class="fa fa-cogs"></i>Settings</a></li>
                  <li><a href="#"><i class="fa fa-times"></i>Remove Panel</a></li>
                </ul>
              </div>
            </div>
            <div class="panel-body">
              <div class="cd-horizontal-timeline">
                <div class="timeline">
                  <div class="events-wrapper">
                    <div class="events">
                      <ol>
                        <li><a href="#" data-date="10/11/2014" class="older-event">Meeting</a></li>
                        <li><a href="#" data-date="16/11/2014" class="selected">New Project</a></li>
                        <li><a href="#" data-date="06/12/2014">Party</a></li>
                        <li><a href="#" data-date="06/01/2015">Dinner</a></li>
                      </ol>
                      <span class="filling-line"></span> </div>
                  </div>
                  <ul class="cd-timeline-navigation">
                    <li><a href="#" class="prev inactive"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#" class="next"><i class="fa fa-angle-right"></i></a></li>
                  </ul>
                </div>
                <div class="events-content">
                  <ol>
                    <li data-date="10/11/2014">
                      <div class="title">
                        <h2 class="h4">Meeting</h2>
                      </div>
                      <div class="subtitle">
                        <p>10 November 2014, 7:45 PM</p>
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure maiores nulla aspernatur. Nemo doloremque a deserunt quas, sunt, voluptate inventore iure? Deserunt sit omnis quas eligendi, nulla architecto alias officia.</p>
                      </div>
                    </li>
                    <li data-date="16/11/2014" class="selected">
                      <div class="title">
                        <h2 class="h4">New Project Lauched</h2>
                      </div>
                      <div class="subtitle">
                        <p>16 November 2014, 7:45 PM</p>
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure maiores nulla aspernatur. Nemo doloremque a deserunt quas, sunt, voluptate inventore iure? Deserunt sit omnis quas eligendi, nulla architecto alias officia.</p>
                      </div>
                    </li>
                    <li data-date="06/12/2014">
                      <div class="title">
                        <h2 class="h4">Party</h2>
                      </div>
                      <div class="subtitle">
                        <p>06 December 2014, 7:45 PM</p>
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure maiores nulla aspernatur. Nemo doloremque a deserunt quas, sunt, voluptate inventore iure? Deserunt sit omnis quas eligendi, nulla architecto alias officia.</p>
                      </div>
                    </li>
                    <li data-date="06/01/2015">
                      <div class="title">
                        <h2 class="h4">Dinner</h2>
                      </div>
                      <div class="subtitle">
                        <p>06 January 2015, 7:45 PM</p>
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure maiores nulla aspernatur. Nemo doloremque a deserunt quas, sunt, voluptate inventore iure? Deserunt sit omnis quas eligendi, nulla architecto alias officia.</p>
                      </div>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>-->
        
        
        
        
      </div>
    </section>
    <?php include("footer.php"); ?>
  
</body>
</html>
