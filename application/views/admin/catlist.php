  <?php include("header.php"); ?>

  <?php include("menu.php"); ?>

  <main class="main--container">

    <section class="page--header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-6">

            <h2 class="page--title h5">Member List </h2>

            <ul class="breadcrumb">

              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard');?>">Dashboard</a></li>

              <li class="breadcrumb-item"><span>Member List</span></li>

            </ul>

          </div>

          <div class="col-md-6" style="padding-top:10px;" ><a href="<?php echo base_url('dashboard/category');?>" class="btn btn-success btn-xs pull-right" style="float:right;">Add New Category</a></div>

        </div>

      </div>

    </section>

    <section class="main--content">

      <div class="row gutter-20">

        <div class="col-xl-12">

          <div class="panel">

            <div class="panel-heading">

              <h3 class="panel-title">

                

              </h3>

            </div>

             <?php if($success = $this->session->flashdata('success')){?>

             <div class="alert-success"><?php echo $success;?></div>

             <?php }?>

            <div class="panel-body">

              <div class="table-responsive">

                <table class="table table-bordered">

                  <thead>

                    <tr>

                      <th>#</th>

                      <th> Image</th>

                      <th>Title</th>

                      <th>Status</th>

                     <th>Manage</th>

                    </tr>

                  </thead>

                  <tbody>

                   <?php $i = 0; if($result){ foreach($result as $value){ $i++;?>

                     <tr>

                      <td><?php echo $i;?></td>

                      <td><img src="<?php echo base_url('assets/image/category/'.$value->prod_cat_image);?>" height="100px" width="100px"></td>

                      <td><a href="javascript:void(0)" class="btn-link"><?php echo $value->prod_cat_title;?></a></td>

                      <td><?php if($value->prod_cat_status==0){ echo 'Pending';}else{echo 'Active';}?></td>

                      <td><span class="label label-success"> <a href="<?php echo base_url('dashboard/edit_cat/'.$value->prod_catid);?>" class="btn-link"><i class="fa fa-edit"></i></a></span>

                      <span class="label label-warning"> <a href="<?php echo base_url('dashboard/deleteCat/'.$value->prod_catid);?>" class="btn-link"><i class="fa fa-trash"></i></a></span></td>

                    </tr>

                    <?php } 

								}else{ ?>

					<tr>

						<td colspan="8" align="center" class="btn-danger">No Data Available Yet</td>

					</tr>						 

										  

						<?php }?>

                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>

        

        

        

        

        

        

        

      </div>

    </section>

    <?php include("footer.php"); ?>

  </main>

</div>

<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>

<script src="assets/js/perfect-scrollbar.min.js"></script>

<script src="assets/js/jquery.sparkline.min.js"></script>

<script src="assets/js/raphael.min.js"></script>

<script src="assets/js/morris.min.js"></script>

<script src="assets/js/select2.min.js"></script>

<script src="assets/js/jquery-jvectormap.min.js"></script>

<script src="assets/js/jquery-jvectormap-world-mill.min.js"></script>

<script src="assets/js/horizontal-timeline.min.js"></script>

<script src="assets/js/jquery.validate.min.js"></script>

<script src="assets/js/jquery.steps.min.js"></script>

<script src="assets/js/dropzone.min.js"></script>

<script src="assets/js/ion.rangeSlider.min.js"></script>

<script src="assets/js/datatables.min.js"></script>

<script src="assets/js/main.js"></script>

</body>

</html>

