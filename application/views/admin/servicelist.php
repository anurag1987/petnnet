

  <?php include("header.php"); ?>

  <?php include("menu.php"); ?>

  <main class="main--container">

    <section class="page--header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-6">

            <h2 class="page--title h5">Service List </h2>

            <ul class="breadcrumb">

              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard');?>">Dashboard</a></li>

              <li class="breadcrumb-item"><span>Service List</span></li>

            </ul>

          </div>

          

        </div>

      </div>

    </section>

    <section class="main--content">

      <div class="row gutter-20">

        <div class="col-xl-12">

          <div class="panel">

            <div class="panel-heading">
            </div>

             <?php if($success = $this->session->flashdata('success')){?>

             <div class="alert-success"><?php echo $success;?></div>

             <?php }?>

            <div class="panel-body">

              <div class="table-responsive">

                <table class="table table-bordered">

                  <thead>

                    <tr>

                      <th>#</th>

                      <th> Image</th>

                      <th>Name</th>
                      <th>Description</th>

                      <th>Manage</th>

                    </tr>

                  </thead>

                  <tbody>

                   <?php $i = 0; foreach($servicelist as $value){ $i++;?>

                     <tr>

                      <td><?php echo $i;?></td>

                      <td><img src="<?php echo base_url('assets/image/adminservice/'.$value->serviceimg);?>" style="height: 50px;"></td>

                      <td><?php echo $value->name;?></a></td>

                      <td><?php echo $value->description;?></td>
                      <td><span class="label label-success"> 

                      <a href="<?php echo base_url('dashboard/editservices/'.$value->serviceid);?>" class="btn-link"><i class="fa fa-edit"></i> </a>

                      </span>

                      <span class="label label-warning"> 

                      <a href="<?php echo base_url('dashboard/delservices/'.$value->serviceid);?>" class="btn-link"><i class="fa fa-trash"></i> </a>

                      </span>
					</td>

                    </tr>

                    <?php }  ?>

		

                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>

        

        

        

        

        

        

        

      </div>

    </section>

    <?php include("footer.php"); ?>

  </main>

</div>

<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>

<script src="assets/js/perfect-scrollbar.min.js"></script>

<script src="assets/js/jquery.sparkline.min.js"></script>

<script src="assets/js/raphael.min.js"></script>

<script src="assets/js/morris.min.js"></script>

<script src="assets/js/select2.min.js"></script>

<script src="assets/js/jquery-jvectormap.min.js"></script>

<script src="assets/js/jquery-jvectormap-world-mill.min.js"></script>

<script src="assets/js/horizontal-timeline.min.js"></script>

<script src="assets/js/jquery.validate.min.js"></script>

<script src="assets/js/jquery.steps.min.js"></script>

<script src="assets/js/dropzone.min.js"></script>

<script src="assets/js/ion.rangeSlider.min.js"></script>

<script src="assets/js/datatables.min.js"></script>

<script src="assets/js/main.js"></script>

</body>

</html>



