

<aside class="sidebar" data-trigger="scrollbar">

    <div class="sidebar--nav">

      <ul>

        <li>

          <ul>

            <li class="active"> <a href="<?php echo base_url('dashboard');?>"> <i class="fa fa-home"></i> <span>Dashboard</span> </a> </li>

            <li> <a href="#"> <i class="fa fa-shopping-cart"></i> <span>Services Vendors</span> </a>

              <ul>

              <?php foreach($results as $serresult):?>

               <li><a href="<?php echo base_url('Dashboard/memlist/'.$serresult->ser_code);?>"><?php echo $serresult->name;?></a></li>

                <?php endforeach;?>
              </ul>
            </li>
			<li> <a href="<?php echo base_url('dashboard/manageservices');?>"> <i class="fa fa-cog"></i> <span>Manage Services</span> </a></li>
            
		<li> <a href="<?php echo base_url('Dashboard/category_list');?>"> <i class="fa fa-th-list"></i> <span>Manage Product Category</span> </a></li>
            
             <!--<li> <a href="javascript:void(0)"> <i class="fa fa-th"></i> <span>Manage Category </span> </a>

              <ul>

                <li><?php echo anchor('Dashboard/category','Add Category');?></li>

                <li><?php echo anchor('Dashboard/category_list','Category List');?></li>

               </ul>

            </li>-->
			 <li> <a href="<?php echo base_url('Dashboard/productlist');?>"> <i class="fa fa-cog"></i> <span>Product List</span> </a></li>
            <li> <a href="<?php echo base_url('Dashboard/subscribe');?>"> <i class="fa fa-th-list"></i> <span>Manage Subscribe</span> </a></li>

			<li> <a href="#"> <i class="fa fa-th"></i> <span>Manage Consumer</span> </a>

              <ul>

                <li><a href="<?php echo base_url('Dashboard/customer');?>">Manage Members</a></li>

    			<!--<li><a href="javascript:void(0)">Paid Members</a></li>-->

               </ul>

            </li>

            <li> <a href="javascript:void(0)"> <i class="fa fa-cog"></i> <span>Lost/Found</span> </a></li>

            <li> <a href="javascript:void(0)"> <i class="fa fa-cog"></i> <span>Admin Setting</span> </a></li>

            <li> <a href="<?php echo base_url('Dashboard/logout');?>"> <i class="fa fa-power-off"></i> <span>Logout</span> </a></li>

          </ul>

        </li>

      </ul>

    </div>

    <div class="sidebar--widgets">

      <div class="widget">

        <h3 class="h6 widget--title">Information Summary</h3>

        <div class="summary--widget">

          <div class="summary--widget">

          <div class="summary--item">

            <p class="summary--title">Total Vendor</p>

            <p class="summary--stats"><?php echo $this->db->count_all_results('petnnet_business')?></p>

          </div>

          <div class="summary--item">

            <p class="summary--title">Total Shop</p>

            <p class="summary--stats"><?php $this->db->like('mem_type', 'Shop');

$this->db->from('petnnet_business');

echo $this->db->count_all_results(); ?></p>

          </div>

          <div class="summary--item">

            <p class="summary--title">Total Products</p>

            <p class="summary--stats"><?php echo $this->db->count_all_results('petnnet_product')?></p>

          </div>



          <!--<div class="summary--item">

            <p class="summary--title">Daily Traffic</p>

            <p class="summary--stats">307.512</p>

          </div>-->

        </div>

      </div>

    </div>

  </aside>