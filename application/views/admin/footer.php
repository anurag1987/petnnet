<footer class="main--footer main--footer-light">

      <p>Copyright &copy; <a href="javascript:void(0)">PetNnet</a>. All Rights Reserved.</p>

    </footer>

    </main>

</div>

<script src="<?php echo base_url('assets/login_assets/js/jquery.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/jquery-ui.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/bootstrap.bundle.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/perfect-scrollbar.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/jquery.sparkline.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/raphael.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/morris.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/select2.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/jquery-jvectormap.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/jquery-jvectormap-world-mill.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/horizontal-timeline.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/jquery.validate.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/jquery.steps.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/dropzone.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/ion.rangeSlider.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/datatables.min.js');?>"></script>

<script src="<?php echo base_url('assets/login_assets/js/main.js');?>"></script>