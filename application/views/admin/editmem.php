  <?php include("header.php"); ?>

  <?php include("menu.php"); ?>

  <?php $arr = [];

	foreach($data as $value){

		$arr = $value;

	} ?>

  <main class="main--container">

    <section class="page--header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-6">

            <h2 class="page--title h5">Business Member </h2>

            <ul class="breadcrumb">

              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard');?>">Dashboard</a></li>

              <li class="breadcrumb-item"><span>Update Business Member</span></li>

            </ul>

          </div>

          

        </div>

      </div>

    </section>

    <section class="main--content">

      <div class="row gutter-20">

        <div class="col-md-12">

          <div class="panel">

            <div class="panel-heading">

              <h3 class="panel-title">Update Business Member</h3>

            </div>

            <?php echo form_open("Dashboard/update_mem/".$arr->memid,['enctype'=>'multipart/form-data']);?>

            <input type="hidden" name="id" value="<?php echo $arr->memid;?>">

             <div class="panel-content">

            <?php if($success = $this->session->flashdata('success')){?>

             <div class="alert-success"><?php echo $success;?></div>

             <?php }?>

                 <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Frist Name</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'fname','class'=>'form-control','value'=>$arr->names]);?>

				 <?php echo form_error("fname");?>

                </div>

              </div>

              

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Email-ID</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'email','class'=>'form-control','value'=>$arr->email]);?>

                  <?php echo form_error("email");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Password</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'password','class'=>'form-control','value'=>$arr->password]);?>

                  <?php echo form_error("password");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Mobile</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'mobile','class'=>'form-control','value'=>$arr->mobile]);?>

                  <?php echo form_error("mobile");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Member Type</span>

                

                 <div class="col-md-10">

                <select name="memtype" class="form-control">

                	<option value="<?php echo $arr->ser_code;?>"><?php echo $arr->name;?></option>

                	<?php foreach($results as $services):?>

                	<option value="<?php echo $services->ser_code;?>"><?php echo $services->name;?></option>

                	<?php endforeach;?>

                </select>  

                <?php echo form_error("memtype");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Address</span>

                <div class="col-md-10">

                  <?php echo form_textarea(['name'=>'address','class'=>'form-control','value'=>$arr->address]);?>

                  <?php echo form_error("address");?>

                </div>

              </div>

               <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Member Form</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'memform','class'=>'form-control','value'=>$arr->member_from]);?>

                  <?php echo form_error("memform");?>

                  

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Status</span>

                <div class="col-md-10">

                 <select name="status" class="form-control">

                 	<option value="0" <?php if($arr->status==0){?> selected<?php }?> >Dactive</option>

                 	<option value="1" <?php if($arr->status==1){?> selected<?php }?>>Active</option>

                 </select>

                  <?php echo form_error("status");?>

                  

                </div>

              </div>

              <!--<div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Member Image</span>

                <div class="col-md-10">

                  <?php echo form_input(['type'=>'file','name'=>'memimg']);?>

                  <?php echo form_error("file");?>

                  <img src="<?php echo base_url('assets/image/memimg/'.$arr->mem_image);?>" width="150px" height="150px">

                </div>

              </div>-->

                <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right"></span>

                <div class="col-md-6">

                  <?php echo form_submit(['name'=>'submit','value'=>'Submit','class'=>'btn btn-success']);?>

                </div>

              </div>

              

			  </div>

         <?php echo form_close();?>

          </div>

        </div>

      </div>

    </section>

        <?php include("footer.php"); ?>

  </main>

</div>

<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>

<script src="assets/js/perfect-scrollbar.min.js"></script>

<script src="assets/js/jquery.sparkline.min.js"></script>

<script src="assets/js/raphael.min.js"></script>

<script src="assets/js/morris.min.js"></script>

<script src="assets/js/select2.min.js"></script>

<script src="assets/js/jquery-jvectormap.min.js"></script>

<script src="assets/js/jquery-jvectormap-world-mill.min.js"></script>

<script src="assets/js/horizontal-timeline.min.js"></script>

<script src="assets/js/jquery.validate.min.js"></script>

<script src="assets/js/jquery.steps.min.js"></script>

<script src="assets/js/dropzone.min.js"></script>

<script src="assets/js/ion.rangeSlider.min.js"></script>

<script src="assets/js/datatables.min.js"></script>

<script src="assets/js/main.js"></script>

</body>

</html>