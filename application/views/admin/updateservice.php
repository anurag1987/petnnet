  <?php include("header.php"); ?>

  <?php include("menu.php");  ?>


  <main class="main--container">

    <section class="page--header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-6">

            <h2 class="page--title h5">Form </h2>

            <ul class="breadcrumb">

              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard');?>">Dashboard</a></li>

              <li class="breadcrumb-item"><span>Update Service</span></li>

            </ul>

          </div>

          

        </div>

      </div>

    </section>

    <section class="main--content">

      <div class="row gutter-20">

        <div class="col-md-12">

          <div class="panel">

            <div class="panel-heading">

              <h3 class="panel-title">Update Service</h3>

            </div>

            <?php $arr = []; foreach($getdata as $value): $arr = $value; endforeach;?>

            <?php echo form_open("Dashboard/updateservice/".$arr->serviceid,['enctype'=>'multipart/form-data']);?>

             <div class="panel-content">

            <?php if($success = $this->session->flashdata('success')){?>

             <div class="alert-success"><?php echo $success;?></div>

             <?php }?>

                 <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Service name</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'name','class'=>'form-control','value'=>$arr->name]);?>

				 <?php echo form_error("name");?>

                </div>

              </div>
              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Service Code</span>

                <div class="col-md-10">

                  <?php echo form_input(['name'=>'ser_code','class'=>'form-control','value'=>$arr->ser_code]);?>

				 <?php echo form_error("ser_code");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Description</span>

                <div class="col-md-10">

                  <?php echo form_textarea(['name'=>'description','class'=>'form-control','value'=>$arr->description]);?>

                  <?php echo form_error("description");?>

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right">Category Image</span>

                <div class="col-md-10">

                  <?php echo form_input(['type'=>'file','name'=>'serviceimg','value'=>$arr->serviceimg]);?>

                  <?php echo form_error("catimg");?>

                  <img src="<?php echo base_url('assets/image/adminservice/'.$arr->serviceimg);?>" width="150px" height="200px;">

                </div>

              </div>

              <div class="form-group row"> <span class="label-text col-md-2 col-form-label text-md-right"></span>

                <div class="col-md-6">

                  <?php echo form_submit(['name'=>'submit','value'=>'Submit','class'=>'btn btn-success']);?>

                </div>

              </div>

			  </div>

         <?php echo form_close();?>

          </div>

        </div>

      </div>

    </section>

        <?php include("footer.php"); ?>

  </main>

</div>

<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>

<script src="assets/js/perfect-scrollbar.min.js"></script>

<script src="assets/js/jquery.sparkline.min.js"></script>

<script src="assets/js/raphael.min.js"></script>

<script src="assets/js/morris.min.js"></script>

<script src="assets/js/select2.min.js"></script>

<script src="assets/js/jquery-jvectormap.min.js"></script>

<script src="assets/js/jquery-jvectormap-world-mill.min.js"></script>

<script src="assets/js/horizontal-timeline.min.js"></script>

<script src="assets/js/jquery.validate.min.js"></script>

<script src="assets/js/jquery.steps.min.js"></script>

<script src="assets/js/dropzone.min.js"></script>

<script src="assets/js/ion.rangeSlider.min.js"></script>

<script src="assets/js/datatables.min.js"></script>

<script src="assets/js/main.js"></script>

</body>

</html>