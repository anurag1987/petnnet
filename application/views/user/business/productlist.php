  <?php include_once dirname(__FILE__).('/../header.php');?>
  <?php $arr = [];	foreach($result as $value):?>
  <?php $arr = $value;?>
  <?php endforeach;?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
   <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
            <h2 class="title text-center">Manage Product</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="<?php echo base_url('business');?>">Home</a></li>
                <li class="active text-silver-gray">Product</li>
               
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <div align="center"><img src="<?php echo base_url('assets/image/business/'.$arr->mem_image);?> " alt=""></div>
              </div>
              <h3 class="name font-24 mt-20 mb-0 text-center"><?php echo $arr->names;?></h3>
              <h4 class="mt-5 text-theme-colored text-center">Member Id : #<?php echo $arr->memid;?></h4>
              <?php include('menu.php');?>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="line-bottom mt-0">Profile</h4>
                  <div class="volunteer-address">
                    <div class="row">
                      <ul>
                        <li class="pull-left col-md-6">
                          <div class="bg-light media border-bottom p-15">
                            <div class="media-left"> <i class="fa fa-phone text-theme-colored font-24 mt-5"></i> </div>
                            <div class="media-body">
                              <h5 class="mt-0 mb-0">Contact:</h5>
                             <p><span>Phone:</span> +91<?php echo $arr->mobile;?><br>
                                <span>Email:</span> <?php echo $arr->email;?></p>
                            </div>
                          </div>
                        </li>
                        <li class="pull-left col-md-6">
                          <div class="bg-light media border-bottom p-15 mb-20">
                            <div class="media-left"> <i class="fa fa-map-marker text-theme-colored2 font-24 mt-5"></i> </div>
                            <div class="media-body">
                             <h5 class="mt-0 mb-0">Address:</h5>
                              <p><?php echo $arr->address;?></p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 mt-0">
                  <div class="border-bottom mb-20">
                  <?php if($success = $this->session->flashdata('success')):?>
                    <div class="alert alert-success"><?php echo $success;?></div>
                    <?php endif;?>
                     <?php if($error = $this->session->flashdata('error')):?>
                    <div class="alert alert-success"><?php echo $error;?></div>
                    <?php endif;?>
                    <h4 class="theme-colored pull-left mt-0"> Manage Porduct</h4>
                     <a href="<?php echo base_url('business/addproduct');?>" class="btn-theme-colored2 btn-sm pull-right">Add New</a>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <table class="table table-bordered tbl-pad-2">
                        <tr>
                          <td><strong>#</strong></td>
                         <!-- <td><strong>Category Name</strong></td>-->
                          <td><strong>Product Name</strong></td>
                          <td><strong>Sale Price</strong></td>
                          <td><strong>Actual Price</strong></td>
                          <td><strong>Created At</strong></td>
                          <td><strong>Status</strong></td>
                          <td><strong>Manage</strong></td>
                        </tr>
                        <?php $i =0; foreach($products as $products){ $i++; ?>
                         <tr>
                          <td><?php echo $i;?></td>
                         <!-- <td><?php echo $products->name;?> </td>-->
                          <td><?php echo $products->title;?> </td>
                          <td><?php echo $products->sale_price;?> </td>
                          <td><?php echo $products->actual_price;?> </td>
                          <td><?php echo(date("d/m/Y",$products->created_at)); ?> </td>
                          <td><?php if($products->pro_status==0){echo "Pending";}else{echo "Active";}?> </td>
                          <td><a href="<?php echo base_url('business/delproduct/'.$products->product_id);?>">Delete</a></td>
                        </tr>
							  <?php }?>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <?php include_once dirname(__FILE__).('/../footer.php');?>
</div>
<!-- end wrapper -->
<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>
</body>
</html>
