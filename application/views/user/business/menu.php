               <div class="left-box mt-20">
                <ul class="plist">
                  <li><a href="<?php echo base_url('business');?>"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                  <li><a href="<?php echo base_url('business/profile');?>"><i class="fa fa-edit"></i> Update Profile</a> </li>
                  <li><a href="<?php echo base_url('business/mngservice');?>"><i class="fa fa-align-center"></i> Manage Service  </a> </li> 
                  <li><a href="javascript:void(0)"><i class="fa fa-clock-o"></i> Manage business Hour </a> </li>
                 <li><a href="<?php echo base_url('business/manageproduct');?>"><i class="fa fa-list"></i> Manage Products</a> </li> 
                 <?php if ($value->mem_type == 'Shop'){ }else{ ?>
                  <li><a href="javascript:void(0)"><i class="fa fa-list-alt"></i> Manage Appointment </a> </li>
					  <?php }?>
                  <li><a href="javascript:void(0)"><i class="fa fa-list-ol"></i> View Order </a> </li>
                  <li><a href="<?php echo base_url('business/updatepassword');?>"><i class="fa fa-key"></i> Change Password </a> </li>
                  <li><a href="<?php echo base_url('business/logout');?>"><i class="fa fa-power-off"></i> Logout </a> </li>
                </ul>
              </div>
             