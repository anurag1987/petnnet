  <?php include_once dirname(__FILE__).('/../header.php');?>
<?php $arr = [];	foreach($result as $value):?>
  <?php $arr = $value;?>
    <?php endforeach;?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
  <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Manage Product</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="<?php echo base_url('business');?>">Home</a></li>
                <li class="active text-silver-gray">Add Product</li>
               
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <div align="center"><img src="<?php echo base_url('assets/image/business/'.$arr->mem_image);?> " alt=""></div>
              </div>
              <h3 class="name font-24 mt-20 mb-0 text-center"><?php echo $arr->names;?></h3>
              <h4 class="mt-5 text-theme-colored text-center">Member Id : #<?php echo $arr->memid;?></h4>
              <?php include('menu.php');?>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="line-bottom mt-0">Add New Product</h4>
                  <form method="post" enctype="multipart/form-data" action="<?php echo base_url('business/insertproduct');?>">
                
                   <div class="form-group">
                    <label>Select Category <small style="color: red">*</small></label>
                 <select name="category" class="form-control">
                 	<option value=""> Select Category</option>
                 	<?php  foreach($procategory as $procategory):?>
                 	<option value="<?php echo $procategory->prod_catid;?>"><?php echo $procategory->prod_cat_title;?></option>
                 	<?php endforeach;?>
                 </select>
                 <?php echo form_error('category');?>
                </div>
                   <div class="form-group">
                    <label>Product Title <small style="color: red">*</small></label>
                 <?php echo form_input(['name'=>'title','class'=>'form-control','value'=>set_value('title')]);?>
                 <?php echo form_error('title');?>
                </div>
                <div class="form-group">
                  <label>Description <small style="color: red">*</small></label>
                   <?php echo form_textarea(['name'=>'description','class'=>'form-control','value'=>set_value('description'),'rows'=>5]);?>
                  <?php echo form_error('description');?>
                </div>
                <div class="form-group">
                    <label>Sale Price <small style="color: red">*</small></label>
                 <?php echo form_input(['name'=>'saleprice','class'=>'form-control','value'=>set_value('saleprice')]);?>
                 <?php echo form_error('saleprice');?>
                </div>
                <div class="form-group">
                    <label>Actual Price <small style="color: red">*</small></label>
                  <?php echo form_input(['name'=>'actualprice','class'=>'form-control','value'=>set_value('actualprice')]);?>
                 <?php echo form_error('actualprice');?>
                </div>
                <div class="form-group">
                  <label>Image </label>
                  <input type="file" name="proimg" class="form-control" value="<?php echo set_value('proimg');?>"> 
                     <?php echo form_error('proimg');?>
                </div>
                
                <div class="form-group">
                  <input name="form_botcheck" class="form-control" type="hidden" value="" />
                  <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Add Now</button>
                </div>
             </form>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <?php include_once dirname(__FILE__).('/../footer.php');?>

</body>
</html>
