  <?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">Pets Lost / Found</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <!--<li><a href="#">Pages</a></li>-->
                <li class="active text-theme-colored">Pets Lost / Found</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

<section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <div class="p-20 border-1px mb-30">
        <div class=" form-horizontal">
        <form id="searchform" method="get">
        <div class="row">
        <div class="col-md-4">
         <input name="pp" id="lst" type="radio" value="" class="radio-inline"> <label for="lst">Lost </label>&nbsp;&nbsp;&nbsp;<input name="pp" id="fd" type="radio" value="" class="radio-inline"> <label for="fd">Found </label>
                </div>
                <div class="col-md-8 input-group">
                  <input class="form-control" name="x" placeholder="Search term..." style="height:40px;" type="text">
                    <span class="input-group-btn">
                    <button class="btn btn-colored btn-theme-colored" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
                  </div>
                  </div>
                </form>
                </div>
        </div>
        </div>
        <div class="clearfix"></div>
          <div class="col-md-6">
          <h4>Losted Pets</h4>
            <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
          </div>
          <div class="col-md-6">
          <h4>Founded Pets</h4>
            <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
                <article class="post clearfix mb-30 pb-0 box-shadow">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="entry-header">
                        <div class="post-thumb"> <img class="img-responsive img-fullwidth" src="https://placehold.it/200x200" alt=""> </div>
                      </div>
                    </div>
                    <div class="col-sm-9 p-0">
                      <div class="entry-content mt-0">
                        <a href="#">
                        <h4 class="entry-title mt-0 mb-0 pt-0 pull-left">German Shephared Dog</h4>&nbsp;&nbsp;<span class="text-theme-colored"><i class="fa fa-calendar"></i> SEP 12,18</span>
                        </a>
                        <div class="clearfix"></div>
                        <ul class="list-inline font-12 mb-5 mt-0">
                          <li>Bride <a href="#" class="text-theme-colored">German Shephared Dog |</a></li>
                          <li>Color <a href="#" class="text-theme-colored">Black </a></li>
                        </ul>
                        <p class="mb-5 font-12"> I Lost my Pet, If anyone found ples contact  <a href="#">[...]</a></p>
						<ul class="list-inline font-12 mb-5 mt-0">
                          <li><i class="fa fa-phone-square"></i>  <a href="#" class="text-theme-colored">+91 0000000000, +91 9999999999 |</a></li>
                          <li><i class="fa fa-envelope"></i>  <a href="#" class="text-theme-colored">email@email.com </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </article>
          </div>
        </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->

  <!-- Footer -->
 <?php include("footer.php"); ?>
</body>
</html>