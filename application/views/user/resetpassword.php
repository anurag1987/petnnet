
  <?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Forgot Password</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Login</a></li>
                <li class="active text-silver-gray">Forgot Password</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-push-3">
            <div class="border-1px p-25">
           <?php if($error = $this->session->flashdata('error')):?>
     <div class="alert alert-danger"><?php echo $error;?> </div>
     <?php endif;?> 
               <h4 class="text-theme-colored text-uppercase m-0">Forgot Password</h4>
              <div class="line-bottom mb-10"></div>
              <p>Please Email and Mobile Number For Get Password</p>
              <?php echo form_open('Welcome/getpassword');?>
                <div class="row">
                  
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <?php echo form_input(['name'=>'email','class'=>'form-control required email','placeholder'=>'Enter Register Email ID']);?>
                    <?php echo form_error('email');?>
                     </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <?php echo form_input(['name'=>'mobile','class'=>'form-control required email','placeholder'=>'Enter Register Mobile Number']);?>
                    <?php echo form_error('mobile');?>
                     </div>
                  </div>
                  
                </div>
                
                <div class="form-group mb-0 mt-20">
                  <input name="form_botcheck" class="form-control" type="hidden" value="">
                  <button type="submit" class="btn btn-dark btn-theme-colored" data-loading-text="Please wait...">Get Password</button>
                </div>
              <?php echo form_close();?>
              <!-- Appointment Form Validation-->
              <script>
                $("#appointment_form").validate({
                  submitHandler: function(form) {
                    var form_btn = $(form).find('button[type="submit"]');
                    var form_result_div = '#form-result';
                    $(form_result_div).remove();
                    form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                    var form_btn_old_msg = form_btn.html();
                    form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                    $(form).ajaxSubmit({
                      dataType:  'json',
                      success: function(data) {
                        if( data.status === 'true' ) {
                          $(form).find('.form-control').val('');
                        }
                        form_btn.prop('disabled', false).html(form_btn_old_msg);
                        $(form_result_div).html(data.message).fadeIn('slow');
                        setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                      }
                    });
                  }
                });
              </script>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <?php include("footer.php"); ?>

</body>
</html>