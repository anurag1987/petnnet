<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>

	<!-- Stylesheet -->
<link rel="icon" href="<?php echo base_url('assets/user_assets/images/logo-wide.png');?>" type="image/x-icon">
<?php echo link_tag('assets/user_assets/css/bootstrap.min.css');?>
<?php echo link_tag('assets/user_assets/css/jquery-ui.min.css');?>
<?php echo link_tag('assets/user_assets/css/animate.css');?>
<?php echo link_tag('assets/user_assets/css/css-plugin-collections.css');?>


<!-- CSS | menuzord megamenu skins -->
<?php echo link_tag('assets/user_assets/css/menuzord-megamenu.css');?>
<link href="<?php echo base_url('assets/user_assets/css/menuzord-skins/menuzord-boxed.css');?>" id="menuzord-menu-skins" rel="stylesheet">
<!-- CSS | Main style file -->
<?php echo link_tag('assets/user_assets/css/style-main.css');?>

<!-- CSS | Custom Margin Padding Collection -->
<?php echo link_tag('assets/user_assets/css/custom-bootstrap-margin-padding.css');?>
<!-- CSS | Responsive media queries -->
<?php echo link_tag('assets/user_assets/css/responsive.css');?>
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<?php echo link_tag('assets/user_assets/js/revolution-slider/css/settings.css');?>
<?php echo link_tag('assets/user_assets/js/revolution-slider/css/layers.css');?>
<?php echo link_tag('assets/user_assets/js/revolution-slider/css/navigation.css');?>

<!-- CSS | Theme Color -->
<?php echo link_tag('assets/user_assets/css/colors/theme-skin-color-set1.css');?>
<?php echo link_tag('assets/user_assets/css/mystyle.css');?>
<?php echo link_tag('assets/user_assets/css/tab1.css');?>

<!-- external javascripts -->
	<script src="<?php echo base_url("assets/user_assets/js/jquery-2.2.4.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/jquery-ui.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/bootstrap.min.js");?>"></script>

<!-- JS | jquery plugin collection for this theme -->
	<script src="<?php echo base_url("assets/user_assets/js/jquery-plugin-collection.js");?>"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/jquery.themepunch.tools.min.js");?>"></script>
<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js");?>"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>