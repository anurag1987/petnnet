  <?php include("header.php"); ?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Pet Food / Accessories</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Pet Services</a></li>
                <li class="active text-silver-gray">Pet Food / Accessories</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-md-9 blog-pull-right">
          <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/food.jpg');?>" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppercase font-weight-500">Pet Food / Accessories Name</h4>
                    <div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante Nulla vel metus scelerisque ante.</p>
                    <a href="evfoodent-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <!--<a href="products.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Products <i class="fa fa-angle-double-right"></i></a>-->
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/food.jpg');?>" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppercase font-weight-500">Pet Food / Accessories Name</h4>
                    <div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante Nulla vel metus scelerisque ante.</p>
                    <a href="evfoodent-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <!--<a href="products.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Products <i class="fa fa-angle-double-right"></i></a>-->
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/food.jpg');?>" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppercase font-weight-500">Pet Food / Accessories Name</h4>
                    <div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante Nulla vel metus scelerisque ante.</p>
                    <a href="evfoodent-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <!--<a href="products.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Products <i class="fa fa-angle-double-right"></i></a>-->
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/food.jpg');?>" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppercase font-weight-500">Pet Food / Accessories Name</h4>
                    <div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante Nulla vel metus scelerisque ante.</p>
                    <a href="evfoodent-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <!--<a href="products.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Products <i class="fa fa-angle-double-right"></i></a>-->
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/food.jpg');?>" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppercase font-weight-500">Pet Food / Accessories Name</h4>
                    <div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante Nulla vel metus scelerisque ante.</p>
                    <a href="evfoodent-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <!--<a href="products.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Products <i class="fa fa-angle-double-right"></i></a>-->
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/food.jpg');?>" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppercase font-weight-500">Pet Food / Accessories Name</h4>
                    <div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante Nulla vel metus scelerisque ante.</p>
                    <a href="evfoodent-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <!--<a href="products.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Products <i class="fa fa-angle-double-right"></i></a>-->
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            
            
            <div class="row">
              <div class="col-sm-12">
                <nav>
                  <ul class="pagination theme-colored pull-right xs-pull-center mb-xs-40">
                    <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="sidebar sidebar-left mt-sm-30 box-shadow p-15">
              <div class="widget">
                <h5 class="widget-title">Search By Category</h5>
                <form method="post" action="#">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="input-group mb-10">
                        <input placeholder="Click to Search" class="form-control search-input" type="text">
                        <span class="input-group-btn">
                        <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                        </span>
                      </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
  <select class="form-control" id="sel1" placeholder="Select City">
    <option>Select City</option>
    <option>Delhi</option>
    <option>Mumgai</option>
    <option>Chenai</option>
    <option>Kolkata</option>
  </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                     <select class="form-control" id="sel1" placeholder="Select City">
    <option>Shop Type</option>
    <option>Hole Sale</option>
    <option>Retailer</option>
  </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                     <select class="form-control" id="sel1" placeholder="Select Time">
    <option>7:00 AM</option>
    <option>7:30 AM</option>
    <option>8:00 AM</option>
    <option>8:30 AM</option>
    <option>9:00 AM</option>
    <option>9:30 AM</option>
    <option>10:00 AM</option>
    <option>10:30 AM</option>
    <option>11:00 AM</option>
  </select>
                    </div>
                  </div>
                  
                </div>
                
                <div class="form-group mb-0 mt-20">
                  <button type="submit" class="btn btn-dark btn-theme-colored">Search</button>
                </div>
              </form>
              </div>
              <div class="widget">
                <h5 class="widget-title">Search By Products</h5>
                <ul class="list list-divider list-border">
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                </ul>
              </div>
              
              
              
            </div>
          </div> 
         </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <!-- Footer -->
  <?php include("footer.php"); ?>

</body>
</html>
