<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>
<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="css/menuzord-megamenu.css" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">
<link href="css/mystyle.css" rel="stylesheet" type="text/css">
<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- Header -->
  <?php include("header.php"); ?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Shop Detail</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Pet Services</a></li>
                <li class="active text-silver-gray">Shop Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 pull-left flip">
              <div class="row">
                <div class="col-md-12">
                  <img alt="" src="https://placehold.it/800x350" />
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h2>Shop Naqme</h2>
                  <p class="lead">Lorem ipsum dolor sit amet <span class="text-theme-colored2 font-weight-600">Orthopaedics</span> adipiscing elit. Etiam aliquet odio non porta laoreet. Vestibulum in dui euismod, molestie quam <span class="text-theme-colored2 font-weight-600">porta</span>, sagittis arcu. Pellentesque vitae pulvinar urna.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam aliquet odio non porta laoreet. Vestibulum in dui euismod, molestie quam porta, sagittis arcu. Pellentesque vitae pulvinar urna, in dignissim nulla. Mauris iaculis, tortor sed pharetra varius.</p>
                </div>
                <div class="col-md-12 mb-30">
                  <h4>Contact Info</h4>
                  <ul class="list-icon theme-colored">
                    <li><i class="fa fa-map-marker"></i> Ram Rati Chauraha. Seva Gali, Singapore</li>
                    <li><i class="fa fa-phone"></i> +91 00000 00000</li>
                    <li><i class="fa fa-envelope"></i> info@domain.com</li>
                  </ul>
                  <a class="btn btn-theme-colored2 mt-30" href="products.php">View Products</a>
                </div>
              </div>
            </div>
            <div class="col-sx-12 col-sm-4 col-md-4 sidebar pull-right flip">
              <div class="widget">
                <h3 class="line-bottom mt-0">Products Category</h3>
                <div class="categories">
                  <ul class="list angle-double-right list-border">
                    <li><a href="#">Pet Health</a></li>
                    <li><a href="#">Pet Accessories</a></li>
                    <li><a href="#">Pet Wellness</a></li>
                    <li><a href="#">Natural & Raw Food</a></li>
                    <li><a href="#">Pet Grooming</a></li>
                    <li><a href="#">Pet Adoption</a></li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <div class="bg-img-box maxwidth400 border-10px p-20">
                  <h5><i class="fa fa-clock-o text-theme-colored"></i> Opening Hours</h5>
                  <div class="opening-hours">
                    <ul class="list-unstyled">
                    <li class="clearfix"> <span> Sunday </span>
                        <div class="value"> 9:30AM - 18:00AM </div>
                      </li>
                      <li class="clearfix"> <span> Monday </span>
                        <div class="value"> 9:30AM - 18:00AM </div>
                      </li>
                      <li class="clearfix"> <span> Tuesday </span>
                        <div class="value"> 9:30AM - 18:00AM </div>
                      </li>
                      <li class="clearfix"> <span> Wednesday </span>
                        <div class="value"> 9:30AM - 18:00AM </div>
                      </li>
                      <li class="clearfix"> <span> Thursday </span>
                        <div class="value"> 9:30AM - 18:00AM </div>
                      </li>
                      <li class="clearfix"> <span> Friday </span>
                        <div class="value"> 9:30AM - 18:00AM </div>
                      </li>
                      <li class="clearfix"> <span> Saturday </span>
                        <div class="value"> 9:30AM - 18:00AM </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <!-- Footer -->
  <?php include("footer.php"); ?>
</div>
<!-- end wrapper -->
<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>
</body>
</html>
