<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>
<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="css/menuzord-megamenu.css" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">
<link href="css/mystyle.css" rel="stylesheet" type="text/css">
<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- Header -->
  <?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: home -->
    <section id="home" class="divider no-bg" data-bg-img="images/bg/bg16.html">
      <div class="bg-video">
        <div id="home-video" class="video">
          <div class="player video-container" data-property="{videoURL:'VNk_bckIP5Q',containment:'#home-video',autoPlay:true, showControls:false, mute:false, startAt:0, opacity:1}"></div>
        </div>
      </div>
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container pt-200 pb-200 pt-sm-40 pb-sm-40">
            <div class="row">
              <div class="col-md-12 text-center">
              <div class="col-md-6 col-md-offset-3 text-center">
              <img src="images/logo-wide.png">
              <br>
<br>

                 <a class="btn btn-colored btn-theme-colored" href="#"><i class="fa fa-sign-in"></i> Register for Business</a> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: home-box -->
    <!--<section class="">
      <div class="container pt-0 pb-0">
        <div class="section-content">
          <div class="row mt-sm-30" data-margin-top="-90px">
            <div class="col-sm-12 col-md-4">
              <div class="icon-box home-icon-box bg-theme-colored2 text-center p-40 transparent-7 box-tranform">
                <a class="icon icon-sm mb-20" href="#">
                  <i class="flaticon-pet-animals font-54 text-white"></i>
                </a>
                <h3 class="text-white mt-0">Daily Tips</h3>
                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat</p>
                <a href="#" class="btn btn-theme-colored mt-20">Contact With Us</a>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="icon-box home-icon-box bg-theme-colored text-center p-40 transparent-7 box-tranform">
                <a class="icon icon-sm mb-20" href="#">
                 <i class="flaticon-pet-person font-54 text-white"></i>
                </a>
                <h3 class="text-white mt-0">Daily Care</h3>
                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat</p>
                <a href="#" class="btn btn-theme-colored2 mt-20">Contact With Us</a>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="icon-box home-icon-box bg-theme-colored2 text-center p-40 transparent-7 box-tranform">
                <a class="icon icon-sm mb-20" href="#">
                  <i class="flaticon-pet-people font-54 text-white"></i>
                </a>
                <h3 class="text-white mt-0">Expert Advice</h3>
                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat</p>
                <a href="#" class="btn btn-theme-colored mt-20">Contact With Us</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>-->

    <!-- Section: About -->
    <!--<section class="">
      <div class="container pt-40">
        <div class="row">
          <div class="col-md-7">   
            <h2 class="mt-0">Find Your <span class="text-theme-colored2">Perfect Pet Sitter</span></h2>
            <p class="mb-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem volup tatem obcaecati. Atque commodi molestiae consectetur.</p>
            <div class="row mb-sm-30">
              <div class="col-sm-6 col-md-6">
                <div class="icon-box icon-left">
                  <a class="icon bg-theme-colored2 icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">
                    <i class="flaticon-pet-animals text-white"></i>
                  </a>
                  <h4 class="icon-box-title mt-10">Pet Care Services</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>
                </div>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="icon-box icon-left">
                  <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">
                    <i class="flaticon-pet-man text-white"></i>
                  </a>
                  <h4 class="icon-box-title mt-10">Pet Handler</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>
                </div>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="icon-box icon-left">
                  <a class="icon bg-theme-colored2 icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">
                    <i class="flaticon-pet-man text-white"></i>
                  </a>
                  <h4 class="icon-box-title mt-10">Pet Sitter</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>
                </div>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="icon-box icon-left">
                  <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">
                    <i class="flaticon-pet-scissors text-white"></i>
                  </a>
                  <h4 class="icon-box-title mt-10">Pet Groomer</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <div class="box-hover-effect play-button mt-10">
              <div class="effect-wrapper">
                <div class="thumb">
                  <img class="img-fullwidth" src="images/about/7.jpg" alt="project">
                </div>
                <div class="overlay-shade bg-theme-colored"></div>
                <div class="text-holder text-holder-middle">
                  <a href="https://youtu.be/JC7IKuQs4HE" data-lightbox-gallery="youtube-video" title="Youtube Video"><img alt="" src="images/play-button/s1.png"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>-->

    <!-- Section: Services --> 
    <section class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class=" mt-0 line-height-1">Available <span class="text-theme-colored2">Pet</span> Services</h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content text-center">
          <div class="row">
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq7.jpg" alt="">                </div>
                <div class="details">
                  <h5 class="mb-15">Pet shops</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq4.jpg" alt="">                </div>
                <div class="details">
                  <h5 class="mb-15">Pet doctors / veterinarians</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq5.jpg" alt="">                </div>
                <div class="details">
                  <h5 class="mb-15">Pet trainers</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.6s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq3.jpg" alt="">                </div>
                <div class="details">
                  <h5 class="mb-15">Pet hostels</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq2.jpg" alt="">                </div>
                <div class="details">
                  <h5 class="mb-15">Pet sitters</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq1.jpg" alt="">                </div>
                <div class="details">
                  <h5 class="mb-15">Pet travel companies / handlers</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq8.jpg" alt="">
                </div>
                <div class="details">
                  <h5 class="mb-15">Pet activity diary / schedule platform</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq9.jpg" alt="">
                </div>
                <div class="details">
                  <h5 class="mb-15">Pet breeding</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq6.jpg" alt="">
                </div>
                <div class="details">
                  <h5 class="mb-15">Pet grooming centers & spas</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq10.jpg" alt="">
                </div>
                <div class="details">
                  <h5 class="mb-15">Pet event organizers</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq11.jpg" alt="">
                </div>
                <div class="details">
                  <h5 class="mb-15">Pet education / courses / course providers</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="service-box">
                <div class="thumb">
                  <img class="" src="images/services/sq12.jpg" alt="">
                </div>
                <div class="details">
                  <h5 class="mb-15">Pet food / accessories</h5>
                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>
                  <a href="#" class="btn btn-theme-colored2">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Features 1 -->
    <section class="">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-30">
            <h2 class="mt-30">For Pet <span class="text-theme-colored2">Enthusiasts</span></h2>
            <p class="mb-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem volup tatem obcaecati. Atque commodi molestiae consectetur.</p>

            <div class="icon-box icon-left mb-50">
              <a class="icon bg-theme-colored2 icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">
                <i class="flaticon-pet-animals text-white"></i>
              </a>
              <h4 class="icon-box-title mt-10">Pet knowledge base / Wikipedia</h4>
              <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>
            </div>

            <div class="icon-box icon-left mb-50">
              <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">
                <i class="flaticon-pet-animals text-white"></i>
              </a>
              <h4 class="icon-box-title mt-10">Pet forum</h4>
              <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>
            </div>

            <div class="icon-box icon-left mb-50">
              <a class="icon bg-theme-colored2 icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">
                <i class="flaticon-pet-animals text-white"></i>
              </a>
              <h4 class="icon-box-title mt-10">Animal abuse / pet cruelty articles</h4>
              <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>
            </div>
            
            
            <a href="#" class="btn btn-dark btn-theme-colored btn-xl">View All Pet</a> <a href="#" class="btn btn-dark btn-theme-colored2 btn-xl">Post A Job</a>
          </div>
          <div class="col-md-6">
            <img class="img-fullwidth" src="images/about/p2.png" alt="project">
          </div>
        </div>
      </div>
    </section>

    <!-- Section Contact -->
    <section class="divider" data-bg-img="images/bg/p1.jpg">
      <div class="container">
        <div class="row">
            <div class="col-md-5">
             <div class="p-30 bg-theme-colored mt-10">
                <h2 class="text-white mt-0 mb-10">Find  Your Pet Sitter!</h2>
              <!-- Appilication Form Start-->
              <form id="reservation_form" name="reservation_form" class="reservation-form mt-20" method="post" action="">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group mb-20">
                      <input placeholder="Enter Name" type="text" id="reservation_name" name="reservation_name" required="" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-20">
                      <input placeholder="Email" type="text" id="reservation_email" name="reservation_email" class="form-control" required="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-20">
                      <input placeholder="Phone" type="text" id="reservation_phone" name="reservation_phone" class="form-control" required="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-20">
                      <div class="styled-select">
                        <select id="person_select" name="person_select" class="form-control" required>
                          <option value="">Services</option>
                          <option value="1 Person">Pet Grooming</option>
                          <option value="2 Person">Pet DayCare</option>
                          <option value="3 Person">Pet Sitting</option>
                          <option value="Family Pack">Veterinary Help</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-20">
                      <input name="Date" class="form-control required date-picker" type="text" placeholder="Date" aria-required="true">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <textarea placeholder="Enter Message" rows="2" class="form-control required" name="form_message" id="form_message" aria-required="true"></textarea>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-0 mt-10">
                      <input name="form_botcheck" class="form-control" type="hidden" value="">
                      <button type="submit" class="btn btn-theme-colored2 btn-lg btn-block" data-loading-text="Please wait...">Submit Request</button>
                    </div>
                  </div>
                </div>
              </form>
              <!-- Application Form End-->
              </div>
            </div>
        </div>
      </div>
    </section>

    <!-- Section: Choose Us -->
    <section>
      <div class="container pb-0">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase line-bottom-double-line-centered mt-0">Why <span class="text-theme-colored2">Choose </span> Us</h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-4 mt-20">
              <div class="icon-box icon-theme-colored benefit-icon tmedia text-right p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-right flip ml-30 pl-0">
                <i class="flaticon-pet-animals font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">Care Advice</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-right flip ml-30 pl-0">
                <i class="flaticon-pet-pets-hotel-house-sign-with-a-paw font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">pet washing</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-right flip ml-30 pl-0">
                <i class="flaticon-pet-people-2 font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">pet treatment</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <img src="images/about/p1.png" alt="">
            </div>
            <div class="col-md-4 mt-10">
              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-left flip">
                <i class="flaticon-pet-transport font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">Emergency Service</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-left flip">
                <i class="flaticon-pet-play font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">walking & training</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-left flip">
                <i class="flaticon-pet-feeding-the-dog font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">pet accessories</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!--start gallary Section-->
    <section class="">
      <div class="container">
        <div class="section-title text-center mt-0">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1">Our <span class="text-theme-colored2">Gallery</span></h2>
            <div class="title-icon">
              <img class="mb-10" src="images/title-icon.png" alt="">
            </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <!-- Portfolio Filter -->
              <div class="portfolio-filter text-center">
                <a href="#" class="active" data-filter="*">All</a>
                <a href="#branding" class="" data-filter=".branding">Catagory 1</a>
                <a href="#design" class="" data-filter=".design">Catagory 2</a>
                <a href="#photography" class="" data-filter=".photography">Catagory 3</a>
              </div>
              <!-- End Portfolio Filter -->
              
              <!-- Portfolio Gallery Grid -->
              <div class="gallery-isotope default-animation-effect grid-3 gutter-small clearfix" data-lightbox="gallery">
                <!-- Portfolio Item Start -->
                <div class="gallery-item design">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/1.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/1.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item branding photography">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/2.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/2.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item design">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/3.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/3.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item branding">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/4.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/4.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item design photography">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/5.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/5.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item photography">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/6.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/6.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item branding">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/7.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/7.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item photography">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/8.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/8.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                <!-- Portfolio Item Start -->
                <div class="gallery-item branding">
                  <div class="thumb">
                    <img class="img-fullwidth" src="images/gallery/9.jpg" alt="project">
                    <div class="overlay-shade bg-theme-colored2"></div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="images/gallery/full/9.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
              </div>
              <!-- End Portfolio Gallery Grid -->
            </div>
          </div>
        </div>
      </div>
    </section>

    <!--start testimonial Section-->
    <section class="divider" data-bg-img="images/bg/p2.jpg">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-white text-uppercase mt-0 line-height-1">Testimonial</h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <div class="owl-carousel-3col" data-dots="true">
                <div class="item">
                  <div class="testimonial testimonial-dog">
                    <div class="comment p-30">
                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>
                    </div>
                    <div class="content mt-20">
                      <div class="thumb pull-left flip mr-20">
                        <img class="img-circle img-thumbnail" alt="" src="images/testimonials/1.png">
                      </div>
                      <div class="pull-left flip mt-10">
                        <div class="author">Tegan Bolton</div>
                        <div class="author-title">Happy Client</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial testimonial-dog">
                    <div class="comment p-30">
                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>
                    </div>
                    <div class="content mt-20">
                      <div class="thumb pull-left flip mr-20">
                        <img class="img-circle img-thumbnail" alt="" src="images/testimonials/2.png">
                      </div>
                      <div class="pull-left flip mt-10">
                        <div class="author">Linda Hamilton</div>
                        <div class="author-title">Happy Client</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial testimonial-dog">
                    <div class="comment p-30">
                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>
                    </div>
                    <div class="content mt-20">
                      <div class="thumb pull-left flip mr-20">
                        <img class="img-circle img-thumbnail" alt="" src="images/testimonials/3.png">
                      </div>
                      <div class="pull-left flip mt-10">
                        <div class="author">Jennifer Smith</div>
                        <div class="author-title">Happy Client</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial testimonial-dog">
                    <div class="comment p-30">
                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>
                    </div>
                    <div class="content mt-20">
                      <div class="thumb pull-left flip mr-20">
                        <img class="img-circle img-thumbnail" alt="" src="images/testimonials/1.jpg">
                      </div>
                      <div class="pull-left flip mt-10">
                        <div class="author">Gracy Smith</div>
                        <div class="author-title">Happy Client</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> 
      </div>
    </section>

    <!-- Section: Blog -->
    <section id="blog" class="">
      <div class="container pb-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1">Recent  <span class="text-theme-colored2"> News</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="owl-carousel-3col owl-nav-top mb-sm-0" data-nav="true">
              <div class="item">
                <article class="post clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">
                  <div class="entry-header">
                    <div class="post-thumb thumb"> 
                      <img src="images/blog/3.jpg" alt="" class="img-responsive img-fullwidth"> 
                    </div>
                  </div>
                  <div class="entry-content">
                    <div class="entry-meta mt-20">
                      <ul class="list-inline">
                        <li>October 15, 2018</li>
                        <li>By Admin</li>
                      </ul>
                    </div>
                    <div class="event-content">
                      <h3 class="entry-title text-white text-capitalize mt-5"><a href="#">Things Need To Know Before You Move With Pets</a></h3>
                      <p class="entry-paragraph mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit eius illum libero dolor nobis deleniti, sint iste veritatis ipsa optio nobis</p>
                      <div class="mt-20"><a href="#" class="btn btn-theme-colored2">Read More</a> </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
              <div class="item">
                <article class="post clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">
                  <div class="entry-header">
                    <div class="post-thumb thumb"> 
                      <img src="images/blog/4.jpg" alt="" class="img-responsive img-fullwidth"> 
                    </div>
                  </div>
                  <div class="entry-content">
                    <div class="entry-meta mt-20">
                      <ul class="list-inline">
                        <li>October 15, 2018</li>
                        <li>By Admin</li>
                      </ul>
                    </div>
                    <div class="event-content">
                      <h3 class="entry-title text-white text-capitalize mt-5"><a href="#">Things Need To Know Before You Move With Pets</a></h3>
                      <p class="entry-paragraph mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit eius illum libero dolor nobis deleniti, sint iste veritatis ipsa optio nobis</p>
                      <div class="mt-20"><a href="#" class="btn btn-theme-colored2">Read More</a> </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
              <div class="item">
                <article class="post clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">
                  <div class="entry-header">
                    <div class="post-thumb thumb"> 
                      <img src="images/blog/5.jpg" alt="" class="img-responsive img-fullwidth"> 
                    </div>
                  </div>
                  <div class="entry-content">
                    <div class="entry-meta mt-20">
                      <ul class="list-inline">
                        <li>October 15, 2018</li>
                        <li>By Admin</li>
                      </ul>
                    </div>
                    <div class="event-content">
                      <h3 class="entry-title text-white text-capitalize mt-5"><a href="#">Things Need To Know Before You Move With Pets</a></h3>
                      <p class="entry-paragraph mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit eius illum libero dolor nobis deleniti, sint iste veritatis ipsa optio nobis</p>
                      <div class="mt-20"><a href="#" class="btn btn-theme-colored2">Read More</a> </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <?php include("footer.php"); ?>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
(Load Extensions only on Local File Systems ! 
 The following part can be removed on Server for On Demand Loading) -->
<script  src="js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script  src="js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>

</body>
</html>