  <?php include("header.php"); ?>

  

  <!-- Start main-content -->

  <div class="main-content">

    <!-- Section: home -->

   <section id="home" class="divider no-bg" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/bg16.html');?>">

      <div class="bg-video">

        <div id="home-video" class="video">

          <div class="player video-container" data-property="{videoURL:'VNk_bckIP5Q',containment:'#home-video',autoPlay:true, showControls:false, mute:false, startAt:0, opacity:1}"></div>

        </div>

      </div>

      <div class="display-table">

        <div class="display-table-cell">

          <div class="container pt-200 pb-200 pt-sm-40 pb-sm-40">

            <div class="row">

              <div class="col-md-12 text-center">

              <div class="col-md-6 col-md-offset-3 text-center">

              <img src="<?php echo base_url('assets/user_assets/images/logo-ban.png');?>">

              <br>

<br>



                  <a class="btn btn-colored btn-theme-colored" href="<?php echo base_url('businesslogin');?>"><i class="fa fa-sign-in"></i> Register for Business</a> 

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>

         <!-- Section: Services --> 

    <section class="bg-silver-light">

      <div class="container">

        <div class="section-title text-center">

          <div class="row">

            <div class="col-md-8 col-md-offset-2">

              <h2 class=" mt-0 line-height-1">Available <span class="text-theme-colored2">Pet</span> Services</h2>

              <div class="title-icon">

                <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">

              </div>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>

            </div>

          </div>

        </div>

        <div class="section-content text-center">

          <div class="row">
			<?php foreach ($services as $values):?>
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/image/adminservice/'.$values->serviceimg);?>" alt="">

                </div>

                <div class="details">

                  <h5 class="mb-15"><?php echo $values->name;?></h5>

                  <p><?php echo $values->description;?></p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Shops</a> 

                      </div>

                    </div>

              </div>

            </div>
            <?php endforeach;?>

            <!--<div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq2.jpg');?>" alt="">                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Sitters</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Sitters</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq6.jpg');?>" alt="">

                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Grooming Centers & Spas</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Grooming Centers & Spas</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq4.jpg');?>" alt="">                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Doctors / Veterinarians</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">et Doctors / Veterinarians</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq1.jpg');?>" alt="">                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Travel Companies / Handlers</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Travel Companies / Handlers</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq10.jpg');?>" alt="">

                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Event Organizers</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Event Organizers</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq5.jpg');?>" alt="">                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Trainers</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Trainers</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq8.jpg');?>" alt="">

                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Activity Diary / Schedule Platform</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Activity Diary / Schedule Platform</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq11.jpg');?>" alt="">

                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Education / Courses / Course providers</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Education / Courses / Course providers</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.6s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq3.jpg');?>" alt="">                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Hostels</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Hostels</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq9.jpg');?>" alt="">

                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Breeding</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Breeding</a> 

                      </div>

                    </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">

              <div class="service-box">

                <div class="thumb">

                  <img class="" src="<?php echo base_url('assets/user_assets/images/services/sq12.jpg');?>" alt="">

                </div>

                <div class="details">

                  <h5 class="mb-15">Pet Food / Accessories</h5>

                  <p>Lorem ipsum dolor sit amet consec tetur adipis icing elit vero omnis unde</p>

                  

                </div>

                <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Food / Accessories</a> 

                      </div>

                    </div>

              </div>

            </div>

          </div>-->

        </div>

      </div>

    </section>

    <!-- Section: Features 1 -->

    <section class="">

      <div class="container">

        <div class="row">

          <div class="col-md-6 mb-30">

            <h2 class="mt-30">For Pet <span class="text-theme-colored2">Enthusiasts</span></h2>

            <p class="mb-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem volup tatem obcaecati. Atque commodi molestiae consectetur.</p>



            <div class="icon-box icon-left mb-50">

              <a class="icon bg-theme-colored2 icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">

                <i class="flaticon-pet-animals text-white"></i>

              </a>

              <h4 class="icon-box-title mt-10"><a href="#">Pet Knowledge Base / Wikipedia</a></h4>

              <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>

              <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Knowledge Base / Wikipedia</a> 

                      </div>

                    </div>

            </div>



            <div class="icon-box icon-left mb-50">

              <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">

                <i class="flaticon-pet-animals text-white"></i>

              </a>

              <h4 class="icon-box-title mt-10"><a href="#">Pet Forum</a></h4>

              <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>

              <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Forum</a> 

                      </div>

                    </div>

            </div>



            <div class="icon-box icon-left mb-50">

              <a class="icon bg-theme-colored2 icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">

                <i class="flaticon-pet-animals text-white"></i>

              </a>

              <h4 class="icon-box-title mt-10"><a href="#">Animal Abuse / Pet Cruelty Articles</a></h4>

              <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>

              <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Animal Abuse / Pet Cruelty Articles</a> 

                      </div>

                    </div>

            </div>

            <div class="icon-box icon-left mb-50">

              <a class="icon bg-theme-colored2 icon-circled icon-border-effect effect-circled pull-left flip sm-pull-none" href="#">

                <i class="flaticon-pet-animals text-white"></i>

              </a>

              <h4 class="icon-box-title mt-10"><a href="#">Pet Related Articles / News</a></h4>

              <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor.</p>

              <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                          <a href="#">Pet Related Articles / News</a> 

                      </div>

                    </div>

            </div>

            

            <!--<a href="#" class="btn btn-dark btn-theme-colored btn-xl">View All Pet</a> <a href="#" class="btn btn-dark btn-theme-colored2 btn-xl">Post A Job</a>-->

          </div>

          <div class="col-md-6">

              <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/about/p2.png');?>" alt="project">

          </div>

        </div>

      </div>

    </section>

    

    



    <!-- Section Contact -->

    <section class="divider" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/p1.jpg');?>">

      <div class="container">

        <div class="row">

            <div class="col-md-5">

             <div class="p-30 bg-theme-colored mt-10">

               <!-- Appilication Form Start-->

               <form id="reservation_form" name="reservation_form" class="reservation-form mt-20" method="post" action="">

              <div class="row">

               <div class="col-sm-6"><h2 class="text-white mt-0 mb-10">Find Your</h2></div>

				<div class="col-sm-6">

                    <div class="form-group mb-20">

                      <div class="styled-select">

                        <select id="person_select" name="person_select" class="form-control" required>

                          <option value="">Services</option>

                          <option value="">Pet shops</option>

                        <option value="">Pet doctors / veterinarians</option>

                        <option value="">Pet trainers</option>

                        <option value="">Pet hostels</option>

                        <option value="">Pet sitters</option>

                        <option value="">Pet travel companies / handlers</option>

                        <option value="">Pet activity diary / schedule platform</option>

                        <option value="">Pet breeding</option>

                        <option value="">Pet grooming centers & spas</option>

                        <option value="">Pet event organizers</option>

                        <option value="">Pet education / courses / course providers</option>

                        <option value="">Pet food / accessories</option> 

                        <option value="">Pet charity / shelters / help</option>



                        </select>

                      </div>

                    </div>

                  </div>

                  </div>

                <div class="row">

                  <div class="col-sm-12">

                    <div class="form-group mb-20">

                      <input placeholder="Enter Name" type="text" id="reservation_name" name="reservation_name" required="" class="form-control">

                    </div>

                  </div>

                  <div class="col-sm-6">

                    <div class="form-group mb-20">

                      <input placeholder="Email" type="text" id="reservation_email" name="reservation_email" class="form-control" required="">

                    </div>

                  </div>

                  <div class="col-sm-6">

                    <div class="form-group mb-20">

                      <input placeholder="Phone" type="text" id="reservation_phone" name="reservation_phone" class="form-control" required="">

                    </div>

                  </div>

                  <div class="col-sm-6">

                    <div class="form-group mb-20">

                      <div class="styled-select">

                        <select id="person_select" name="person_select" class="form-control" required>

                          <option value="">Time</option>

                            <option>7:00 AM</option>

                            <option>7:30 AM</option>

                            <option>8:00 AM</option>

                            <option>8:30 AM</option>

                            <option>9:00 AM</option>

                            <option>9:30 AM</option>

                            <option>10:00 AM</option>

                            <option>10:30 AM</option>

                            <option>11:00 AM</option>

                          </select>

                      </div>

                    </div>

                  </div>

                  <div class="col-sm-6">

                    <div class="form-group mb-20">

                      <input name="Date" class="form-control required date-picker" type="text" placeholder="Date" aria-required="true">

                    </div>

                  </div>

                  <div class="col-sm-12">

                    <div class="form-group">

                      <textarea placeholder="Enter Message" rows="2" class="form-control required" name="form_message" id="form_message" aria-required="true"></textarea>

                    </div>

                  </div>

                  <div class="col-sm-12">

                    <div class="form-group mb-0 mt-10">

                      <input name="form_botcheck" class="form-control" type="hidden" value="">

                      <button type="submit" class="btn btn-theme-colored2 btn-lg btn-block" data-loading-text="Please wait...">Submit Request</button>

                    </div>

                  </div>

                </div>

              </form>

              <!-- Application Form End-->

              </div>

            </div>

        </div>

      </div>

    </section>



    <!-- Section: Choose Us -->

    <section>

      <div class="container pb-0">

        <div class="section-title text-center">

          <div class="row">

            <div class="col-md-8 col-md-offset-2">

              <h2 class="text-uppercase line-bottom-double-line-centered mt-0">Why <span class="text-theme-colored2">Choose </span> Us</h2>

              <div class="title-icon">

                <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">

              </div>

            </div>

          </div>

        </div>

        <div class="section-content">

          <div class="row">

            <div class="col-md-4 mt-20 row">

            <h4 class="text-right">Consumer Reasons</h4>

              <div class="icon-box icon-theme-colored benefit-icon tmedia text-right p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-right flip ml-5 pl-0">

                <i class="flaticon-pet-animals font-18"></i></a>

                <div class="media-body">

                  <!--<h4 class="media-heading heading">Care Advice</h4>-->

                  <p>All your pet needs in one site  - All Products, Accessories, Foods, Doctors etc.  </p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-right flip ml-5 pl-0">

                <i class="flaticon-pet-pets-hotel-house-sign-with-a-paw font-18"></i></a>

                <div class="media-body">

                  <p>Competitive price listing from all pet related businesses (best price guaranteed)</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-right flip ml-5 pl-0">

                <i class="flaticon-pet-people-2 font-18"></i></a>

                <div class="media-body">

                  <p>Review / rating system all users can vote on which pet service / business is the best </p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon tmedia text-right p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-right flip ml-5 pl-0">

                <i class="flaticon-pet-animals font-18"></i></a>

                <div class="media-body">

                  <!--<h4 class="media-heading heading">Care Advice</h4>-->

                  <p>User friendly – find any pet related service which just one click</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-right flip ml-5 pl-0">

                <i class="flaticon-pet-pets-hotel-house-sign-with-a-paw font-18"></i></a>

                <div class="media-body">

                  <p>No hidden charges – flat membership fee structure</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-right flip ml-5 pl-0">

                <i class="flaticon-pet-people-2 font-18"></i></a>

                <div class="media-body">

                  <p>Receive automated alerts for all the latest pet deals and promotion</p>

                </div>

              </div>

            </div>

            <div class="col-md-4">

              <img src="<?php echo base_url('assets/user_assets/images/about/9.png');?>" alt="">

            </div>

            <div class="col-md-4 mt-10 row">

            <h4>Business Reasons</h4>

              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-left flip">

                <i class="flaticon-pet-transport font-18"></i></a>

                <div class="media-body">

                  <p>Reach a massive audience of pet owners and enthusiasts</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-left flip">

                <i class="flaticon-pet-play font-18"></i></a>

                <div class="media-body">

                  <p>Dramatically increase the volume of sales without vastly additional advertising costs</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-left flip">

                <i class="flaticon-pet-feeding-the-dog font-18"></i></a>

                <div class="media-body">

                  <p>No hidden charges – flat membership fee structure</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-left flip">

                <i class="flaticon-pet-transport font-18"></i></a>

                <div class="media-body">

                  <p>Create your own adverts on the site to attract even more customers</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-left flip">

                <i class="flaticon-pet-play font-18"></i></a>

                <div class="media-body">

                  <p>Online inventory system which can be actively updated from store end</p>

                </div>

              </div>

              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-10">

                <a href="#" class="icon icon-circled icon-sm border-1px border-theme-colored pull-left flip">

                <i class="flaticon-pet-feeding-the-dog font-18"></i></a>

                <div class="media-body">

                  <p>Scheduling system & appointment system for services</p>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>

        <!--start gallary Section-->

    <section class="">

      <div class="container">

        <div class="section-title text-center mt-0">

          <div class="row">

            <div class="col-md-8 col-md-offset-2">

              <h2 class="mt-0 line-height-1">Our <span class="text-theme-colored2">Gallery</span></h2>

            <div class="title-icon">

              <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">

            </div>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>

            </div>

          </div>

        </div>

        <div class="section-content">

          <div class="row">

            <div class="col-md-12">

              <!-- Portfolio Filter -->

              <div class="portfolio-filter text-center">

                <a href="#" class="active" data-filter="*">All</a>

                <a href="#branding" class="" data-filter=".branding">Catagory 1</a>

                <a href="#design" class="" data-filter=".design">Catagory 2</a>

                <a href="#photography" class="" data-filter=".photography">Catagory 3</a>

              </div>

              <!-- End Portfolio Filter -->

              

              <!-- Portfolio Gallery Grid -->

              <div class="gallery-isotope default-animation-effect grid-3 gutter-small clearfix" data-lightbox="gallery">

                <!-- Portfolio Item Start -->

                <div class="gallery-item design">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/1.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/1.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item branding photography">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/2.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/2.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item design">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/3.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/3.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item branding">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/4.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/4.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item design photography">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/5.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/5.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item photography">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/6.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/6.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item branding">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/7.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/7.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item photography">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/8.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/8.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->

                <div class="gallery-item branding">

                  <div class="thumb">

                    <img class="img-fullwidth" src="<?php echo base_url('assets/user_assets/images/gallery/9.jpg');?>" alt="project">

                    <div class="overlay-shade bg-theme-colored2"></div>

                    <div class="icons-holder">

                      <div class="icons-holder-inner">

                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">

                          <a href="images/gallery/full/9.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- Portfolio Item End -->

              </div>

              <!-- End Portfolio Gallery Grid -->

            </div>

          </div>

        </div>

      </div>

    </section>



    <!--start testimonial Section-->

    <!--<section class="divider" data-bg-img="<?php echo base_url("assets/user_assets/images/bg/p2.jpg");?>">

      <div class="container">

        <div class="section-title text-center">

          <div class="row">

            <div class="col-md-8 col-md-offset-2">

              <h2 class="text-white text-uppercase mt-0 line-height-1">Testimonial</h2>

              <div class="title-icon">

                <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">

              </div>

              <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>

            </div>

          </div>

        </div>

        <div class="section-content">

          <div class="row">

            <div class="col-md-12">

              <div class="owl-carousel-3col" data-dots="true">

                <div class="item">

                  <div class="testimonial testimonial-dog">

                    <div class="comment p-30">

                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>

                    </div>

                    <div class="content mt-20">

                      <div class="thumb pull-left flip mr-20">

                        <img class="img-circle img-thumbnail" alt="" src="<?php echo base_url('assets/user_assets/images/testimonials/1.png');?>">

                      </div>

                      <div class="pull-left flip mt-10">

                        <div class="author">Tegan Bolton</div>

                        <div class="author-title">Happy Client</div>

                      </div>

                    </div>

                  </div>

                </div>

                <div class="item">

                  <div class="testimonial testimonial-dog">

                    <div class="comment p-30">

                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>

                    </div>

                    <div class="content mt-20">

                      <div class="thumb pull-left flip mr-20">

                        <img class="img-circle img-thumbnail" alt="" src="<?php echo base_url('assets/user_assets/images/testimonials/2.png');?>">

                      </div>

                      <div class="pull-left flip mt-10">

                        <div class="author">Linda Hamilton</div>

                        <div class="author-title">Happy Client</div>

                      </div>

                    </div>

                  </div>

                </div>

                <div class="item">

                  <div class="testimonial testimonial-dog">

                    <div class="comment p-30">

                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>

                    </div>

                    <div class="content mt-20">

                      <div class="thumb pull-left flip mr-20">

                        <img class="img-circle img-thumbnail" alt="" src="<?php echo base_url('assets/user_assets/images/testimonials/3.png');?>">

                      </div>

                      <div class="pull-left flip mt-10">

                        <div class="author">Jennifer Smith</div>

                        <div class="author-title">Happy Client</div>

                      </div>

                    </div>

                  </div>

                </div>

                <div class="item">

                  <div class="testimonial testimonial-dog">

                    <div class="comment p-30">

                      <p>Lorem ipsum dolor sit amet, consectetur adipis icing elit tatem error sit qui volupt atem obcae cati amet conse ctetur adip ctetur isicing elitvolup.</p>

                    </div>

                    <div class="content mt-20">

                      <div class="thumb pull-left flip mr-20">

                        <img class="img-circle img-thumbnail" alt="" src="<?php echo base_url('assets/user_assets/images/testimonials/1.jpg');?>">

                      </div>

                      <div class="pull-left flip mt-10">

                        <div class="author">Gracy Smith</div>

                        <div class="author-title">Happy Client</div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div> 

      </div>

    </section>-->



    <!-- Section: Blog -->

   <section id="blog" class="">

      <div class="container pb-50">

        <div class="section-title text-center">

          <div class="row">

            <div class="col-md-8 col-md-offset-2">

              <h2 class="mt-0 line-height-1">Pet <span class="text-theme-colored2"> News</span></h2>

              <div class="title-icon">

                <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">

              </div>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-md-12">

              <div class="owl-carousel-3col owl-nav-top mb-sm-0" data-nav="true">

              <div class="item">

                <article class="post clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">

                  <div class="entry-header">

                    <div class="post-thumb thumb"> 

                      <img src="<?php echo base_url('assets/user_assets/images/blog/3.jpg');?>" alt="" class="img-responsive img-fullwidth"> 

                    </div>

                  </div>

                  <div class="entry-content">

                    

                    <div class="event-content">

                      <h3 class="entry-title text-white text-capitalize mt-5"><a href="#">Things Need To Know Before You Move With Pets</a></h3>

                      <p class="entry-paragraph mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit eius illum libero dolor nobis deleniti, sint iste veritatis ipsa optio nobis</p>

                      <div class="mt-20"><a href="#" class="btn btn-theme-colored2">Read More</a> </div>

                    </div>

                    <div class="clearfix"></div>

                  </div>

                </article>

              </div>

              <div class="item">

                <article class="post clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">

                  <div class="entry-header">

                    <div class="post-thumb thumb"> 

                      <img src="<?php echo base_url('assets/user_assets/images/blog/4.jpg');?>" alt="" class="img-responsive img-fullwidth"> 

                    </div>

                  </div>

                  <div class="entry-content">

                   

                    <div class="event-content">

                      <h3 class="entry-title text-white text-capitalize mt-5"><a href="#">Things Need To Know Before You Move With Pets</a></h3>

                      <p class="entry-paragraph mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit eius illum libero dolor nobis deleniti, sint iste veritatis ipsa optio nobis</p>

                      <div class="mt-20"><a href="#" class="btn btn-theme-colored2">Read More</a> </div>

                    </div>

                    <div class="clearfix"></div>

                  </div>

                </article>

              </div>

              <div class="item">

                <article class="post clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">

                  <div class="entry-header">

                    <div class="post-thumb thumb"> 

                      <img src="<?php echo base_url('assets/user_assets/images/blog/5.jpg');?>" alt="" class="img-responsive img-fullwidth"> 

                    </div>

                  </div>

                  <div class="entry-content">

                    <div class="event-content">

                      <h3 class="entry-title text-white text-capitalize mt-5"><a href="#">Things Need To Know Before You Move With Pets</a></h3>

                      <p class="entry-paragraph mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit eius illum libero dolor nobis deleniti, sint iste veritatis ipsa optio nobis</p>

                      <div class="mt-20"><a href="#" class="btn btn-theme-colored2">Read More</a> </div>

                    </div>

                    <div class="clearfix"></div>

                  </div>

                </article>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>

     

    

  </div>

  <!-- end main-content -->



  <!-- Footer -->

  <?php include("footer.php"); ?>

</body>

</html>