  <?php include_once ('header.php');?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Login/Registration For Business</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Login/Registration For Business</a></li>
              <?php if($success = $this->session->flashdata('success')):?>
				 <div class="alert alert-success"><?php echo $success;?> </div>
				 <?php endif;?>
				 <?php if($error = $this->session->flashdata('error')):?>
				 <div class="alert alert-danger"><?php echo $error;?> </div>
				 <?php endif;?>
           </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About -->
    <section class="divider">
      <div class="container">
        <div class="row">
        <div class="col-md-6">
            <div class="border-1px p-25">
              <h4 class="text-theme-colored text-uppercase m-0">Login</h4>
              <div class="line-bottom mb-10"></div>
              <p>Already Register Please Login</p>
              <?php echo form_open('businesslogin/business_log');?> 
                <div class="row">
                  
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                     <?php echo form_input(['name'=>'username','class'=>'form-control required email','placeholder'=>'Enter Email']);?> 
                     <?php echo form_error('username');?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                    <?php echo form_input(['type'=>'password','name'=>'password','class'=>'form-control required email','placeholder'=>'Enter Email Password']);?> 
                    <?php echo form_error('password');?>
                    </div>
                  </div>
                </div>
                <div class="form-group mb-10">
                 <input name="stay" id="stay" type="checkbox" value=""> <label for="stay">Stay Connected</label>
                 <a href="<?php echo base_url('businesslogin/password');?>" class="pull-right"> Forgot Password ? </a>
                </div>
                <div class="form-group mb-0 mt-20">
                  <input name="form_botcheck" class="form-control" type="hidden" value="">
                 <?php echo form_submit(['name'=>'submit','value'=>'Login','class'=>'btn btn-dark btn-theme-colored']);?> 
                </div>
            <?php echo form_close();?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="border-1px p-30 mb-0">
              <h3 class="text-theme-colored mt-0 pt-5">Register For Business</h3>
              <hr>
              <p>Register For Business</p>
             <?php echo form_open('businesslogin/business_mem',['enctype'=>'multipart/form-data']);?>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Name <small style="color: red">*</small></label>
                      <?php echo form_input(['name'=>'name','class'=>'form-control','placeholder'=>'Enter Full Name','value'=>set_value('name')]);?>
                      <?php echo form_error('name');?>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Email <small style="color: red">*</small></label>
                       <?php echo form_input(['name'=>'email','class'=>'form-control','placeholder'=>'Enter Email','value'=>set_value('email')]);?>
                       <?php echo form_error('email');?>
                    </div>
                  </div>
                </div>
                <div class="row">               
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Phone <small style="color: red">*</small></label>
                      <?php echo form_input(['name'=>'mobile','class'=>'form-control','placeholder'=>'Enter Mobile','value'=>set_value('mobile')]);?>
                      <?php echo form_error('mobile');?>
                       
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Category <small style="color: red">*</small></label>
                      <select name="category" class="form-control required">
                        <option value="">Select Category</option>
                        <?php foreach($results as $value):?>
                        <option value="<?php echo $value->ser_code;?>"><?php echo $value->name;?></option>
                        <?php endforeach;?>
                      </select>
                      <?php echo form_error('category');?>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Address <small style="color: red">*</small></label>
                  <?php echo form_textarea(['name'=>'address','class'=>'form-control','placeholder'=>'Enter Full Address','value'=>set_value('address')]);?>
                  <?php echo form_error('address');?>
                </div>
                <div class="form-group">
                  <label>Upload Photo</label>
                   <?php echo form_input(['type'=>'file','name'=>'memimg','class'=>'form-control','placeholder'=>'Enter Full Name','value'=>'']);?>
                  <?php echo form_error('memimg');?>
                </div>
                <div class="form-group">
                  <input name="form_botcheck" class="form-control" type="hidden" value="" />
                  <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Register Now</button>
                </div>
              <?php echo form_close();?>
              <!-- Job Form Validation-->
              <script>
                $("#job_apply_form").validate({
                  submitHandler: function(form) {
                    var form_btn = $(form).find('button[type="submit"]');
                    var form_result_div = '#form-result';
                    $(form_result_div).remove();
                    form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                    var form_btn_old_msg = form_btn.html();
                    form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                    $(form).ajaxSubmit({
                      dataType:  'json',
                      success: function(data) {
                        if( data.status === 'true' ) {
                          $(form).find('.form-control').val('');
                        }
                        form_btn.prop('disabled', false).html(form_btn_old_msg);
                        $(form_result_div).html(data.message).fadeIn('slow');
                        setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                      }
                    });
                  }
                });
              </script>
            </div>
          </div>
          
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <?php include_once ('footer.php');?>

</body>
</html>