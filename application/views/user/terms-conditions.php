  <?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">Terms and Conditions</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <!--<li><a href="#">Pages</a></li>-->
                <li class="active text-theme-colored">Terms and Conditions</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

<section class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class=" mt-0 line-height-1">Terms and    <span class="text-theme-colored2">Conditions</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
          <p align="justify"> Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.</p>
        </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <?php include("footer.php"); ?>

</body>
</html>