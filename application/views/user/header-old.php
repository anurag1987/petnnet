<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>

	<!-- Stylesheet -->
<link rel="icon" href="<?php echo base_url('assets/user_assets/images/logo-wide.png');?>" type="image/x-icon">
<?php echo link_tag('assets/user_assets/css/bootstrap.min.css');?>
<?php echo link_tag('assets/user_assets/css/jquery-ui.min.css');?>
<?php echo link_tag('assets/user_assets/css/animate.css');?>
<?php echo link_tag('assets/user_assets/css/css-plugin-collections.css');?>


<!-- CSS | menuzord megamenu skins -->
<?php echo link_tag('assets/user_assets/css/menuzord-megamenu.css');?>
<link href="<?php echo base_url('assets/user_assets/css/menuzord-skins/menuzord-boxed.css');?>" id="menuzord-menu-skins" rel="stylesheet">
<!-- CSS | Main style file -->
<?php echo link_tag('assets/user_assets/css/style-main.css');?>

<!-- CSS | Custom Margin Padding Collection -->
<?php echo link_tag('assets/user_assets/css/custom-bootstrap-margin-padding.css');?>
<!-- CSS | Responsive media queries -->
<?php echo link_tag('assets/user_assets/css/responsive.css');?>
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<?php echo link_tag('assets/user_assets/js/revolution-slider/css/settings.css');?>
<?php echo link_tag('assets/user_assets/js/revolution-slider/css/layers.css');?>
<?php echo link_tag('assets/user_assets/js/revolution-slider/css/navigation.css');?>

<!-- CSS | Theme Color -->
<?php echo link_tag('assets/user_assets/css/colors/theme-skin-color-set1.css');?>
<?php echo link_tag('assets/user_assets/css/mystyle.css');?>
<?php echo link_tag('assets/user_assets/css/tab1.css');?>

<!-- external javascripts -->
	<script src="<?php echo base_url("assets/user_assets/js/jquery-2.2.4.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/jquery-ui.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/bootstrap.min.js");?>"></script>

<!-- JS | jquery plugin collection for this theme -->
	<script src="<?php echo base_url("assets/user_assets/js/jquery-plugin-collection.js");?>"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/jquery.themepunch.tools.min.js");?>"></script>
<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js");?>"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="has-side-panel side-panel-right fullwidth-page">
<!-- Mobile Search and register -->
<?php include("mobile-top.php"); ?>
<!--End Mobile Search and register-->
<div id="wrapper">
   
   <header id="header" class="header">
    
    <div class="header-middle p-0 bg-lighter xs-text-center hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3">
              <a class="menuzord-brand pull-left flip sm-pull-center" href="<?php echo base_url('Welcome');?>"><img src="<?php echo base_url('assets/user_assets/images/logo-wide.png');?>" alt=""></a>
            </div>
          <div class="col-xs-12 col-sm-4 col-md-5">
              <div class="widget no-border sm-text-center mt-15 mb-10 m-0">
              <?php if($memid = $this->session->userdata('memid')){ 
				}elseif(!$busid = $this->session->userdata('busid')){?>
               <a class="btn btn-colored btn-theme-colored" href="<?php echo base_url('Member_login');?>"><i class="fa fa-user-plus"></i> Sign up Member</a>
                <a class="btn btn-colored btn-theme-colored" href="<?php echo base_url('Business_Logn');?>"><i class="fa fa-sign-in"></i> Register for Business</a>
               <?php }else{ 
							}	?>
                

              </div>
            </div>
          
          <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="widget no-border sm-text-center mt-15 mb-10 m-0">
                <form class="">
                <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-colored btn-theme-colored dropdown-toggle" data-toggle="dropdown">
                    	<span id="search_concept">Category</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <?php foreach($results as $value){?>
              			<li> <?php echo $value->name;?></li>
              			<?php }?>
                     
                    </ul>
                </div>
                <input name="search_param" value="all" id="search_param" type="hidden">         
                <input class="form-control" name="x" placeholder="Search term..." type="text" style="height:40px;">
                <span class="input-group-btn">
                    <button class="btn btn-colored btn-theme-colored" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
                </form>
              </div>
          </div>
        </div>
    </div>
    </div>
    <div class="header-nav">
        <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
          <div class="container">
            <nav id="menuzord" class="menuzord no-bg">
             <a class="menuzord-brand pull-left flip sm-pull-none hidden-lg hidden-md" href="index.php"><img src="images/logo-wide.png" alt=""></a>
             <div id="side-panel-trigger" class="side-panel-trigger mr-15 ml-15 mt-10 pull-right sm-pull-none hidden-lg hidden-md hidden-sm"><a href="#"><i class="fa fa-user"></i></a></div>
             <div class="mr-15 ml-15 mt-10 pull-right sm-pull-none hidden-lg hidden-md hidden-sm display-inline">
             <a href="#fullscreen-search-form" id="fullscreen-search-btn"><i class="search-icon fa fa-search"></i></a>
                <div id="fullscreen-search-form">
                  <button type="button" class="close">x</button>
                  <form>
                    <input type="search" value="" placeholder="Search keywords(s)" />
                    <button type="submit"><i class="search-icon fa fa-search"></i></button>
                  </form>
                </div>
                </div>
              <ul class="menuzord-menu">
              <li class="active"><a href="<?php echo base_url('Welcome');?>">Home</a></li>
              <li><a href="javascript:void(0)">Pet Services</a>
                <div class="megamenu">
                  <div class="megamenu-row">
                    <div class="col4">
                      <ul class="list-unstyled list-dashed">
              			<?php foreach($results as $value){?>
              			<li><a href="javascript:void(0)"><i class="fa fa-paw" aria-hidden="true"></i> <?php echo $value->name;?></a></li>
              			<?php }?>
                      </ul>
                    </div>
                    </div>
                </div>
              </li>
             <li><?php echo anchor('Welcome/dailytips','Daily Tips');?></li>
              <li><?php echo anchor('Welcome/petcharity','Pet charity / shelters / help');?></li>
              <li><?php echo anchor('Welcome/lostfound','Pet Lost/Found');?></li>
              <li><?php echo anchor('Welcome/contact','Contact');?></li>
              </ul>
              
            </nav>
          </div>
        </div>
    </div>
  </header>