<?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">About Us</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <!--<li><a href="#">Pages</a></li>-->
                <li class="active text-silver-gray">About</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="content-one">
              <h2 class="mb-10">About Us</h2>
              <p class="mb-20 lead">Lorem ipsum dolor sit amet arcu rhoncus dictum imperdiet id aliquet nonummy, mauris curae in purus in Egestas etiam wisi vulputate eu elit</p>
              <p>Lorem ipsum dolor sit amet arcu rhoncus dictum imperdiet id aliquet nonummy, mauris curae in purus in Egestas etiam</p>
              <a href="#" class="btn btn-circled btn-lg btn-theme-colored2 mb-sm-20">Read More</a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content-tow">
              <img src="<?php echo base_url('assets/user_assets/images/about/4.jpg');?>">
              <h3>Special Care</h3>
              <p>Lorem ipsum dolor sit amet arcu rhoncus dictum imperdiet id aliquet nonummy, mauris curae in purus in Egestas etiam wisi</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content-three">
              <img src="<?php echo base_url('assets/user_assets/images/about/5.jpg');?>" alt="">
              <h3>Veterinary Help</h3>
              <p>Lorem ipsum dolor sit amet arcu rhoncus dictum imperdiet id aliquet nonummy, mauris curae in purus in Egestas etiam wisi</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!--start funfact Section-->
     

    <!-- Section: Choose Us -->
    <section>
      <div class="container pb-0">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase line-bottom-double-line-centered mt-0">Why <span class="text-theme-colored2">Choose </span> Us</h2>
              <div class="title-icon">
                <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-4 mt-20">
              <div class="icon-box icon-theme-colored benefit-icon tmedia text-right p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-right flip ml-30 pl-0">
                <i class="flaticon-pet-animals font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">Care Advice</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-right flip ml-30 pl-0">
                <i class="flaticon-pet-pets-hotel-house-sign-with-a-paw font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">pet washing</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon media text-right p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-right flip ml-30 pl-0">
                <i class="flaticon-pet-people-2 font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">pet treatment</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <img src="<?php echo base_url('assets/user_assets/images/about/9.png');?>" alt="">
            </div>
            <div class="col-md-4 mt-10">
              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-left flip">
                <i class="flaticon-pet-transport font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">Emergency Service</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-left flip">
                <i class="flaticon-pet-play font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">walking & training</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
              <div class="icon-box icon-theme-colored benefit-icon left media p-0 mb-sm-10 mt-30">
                <a href="#" class="icon icon-circled icon-md border-1px border-theme-colored pull-left flip">
                <i class="flaticon-pet-feeding-the-dog font-36"></i></a>
                <div class="media-body">
                  <h4 class="media-heading heading">pet accessories</h4>
                  <p>Ecoforestry is a creative skill and a joy beyond anything found</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <?php include("footer.php"); ?>
</body>
</html>