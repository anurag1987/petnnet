<footer id="footer" class="footer bg-black-111">
    <div class="container pt-70 pb-40">
     
      <div class="row border-bottom-black">
        <div class="col-sm-6 col-md-4">
            <div class="widget dark">
                <img class="mt-10 mb-20" alt="" src="<?php echo base_url("assets/user_assets/images/logo-wide-white.png");?>">
                <p>Lorem ipsum dolor adipisicing amet, consectetur sit elit. Aspernatur incidihil quo officia.</p>
                <p>Lorem ipsum dolor adipisicing amet, consectetur sit elit. Aspernatur incidihil quo officia.</p>
                <div class="mt-20"><a href="#" class="btn btn-colored btn-theme-colored text-white">Read More...</a> </div>
                
            </div>
            
        </div>
        
        <div class="col-sm-6 col-md-4">
            
            <div class="widget dark">
                <h5 class="widget-title line-bottom">Other Links</h5>
                <ul class="list-border">
                    <li><?php echo anchor('welcome/businessus','Business With Us');?></li>
                    <li><?php echo anchor('welcome/payment','Payment Options');?></li>
                    <li><?php echo anchor('welcome/privacy','Privacy Policy');?></li>
                    <li><?php echo anchor('welcome/terms','Terms and Conditions');?></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
          
            <div class="widget dark">
                <h5 class="widget-title line-bottom">Useful Links</h5>
                <ul class="list-border">
                    <li><?php echo anchor('welcome','Home');?></li>
                    <li><?php echo anchor('welcome/about','About us');?></a></li>
                    <li><?php echo  anchor('welcome/contact','Contact us');?></li>
                </ul>
            </div>
            <div class="widget dark">
                <h5 class="widget-title line-bottom">Subscribe Us</h5>
                <!-- Mailchimp Subscription Form Starts Here -->
                <form method="post" class="form-horizontal" id="myform" action="">
                    <div class="input-group">
                       <input type="text" required name="email" id="emailid" class="form-control input-lg font-16" >                        
                        <span class="input-group-btn">
                            <input type="button" id="formsubmit" class="btn btn-colored btn-theme-colored btn-xs m-0 font-14" value="Subscribe" style="height: 45px;">
                        </span>
                    </div>
                     <?php echo form_error('email');?>
                     <span id="output"></span>
                </form>
            </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom bg-black-222">
        <div class="container pt-10 pb-0">
            <div class="row">
                <div class="col-md-6 sm-text-center">
                    <p class="font-13 text text-gray-ccc m-0">Copyright &copy;2018 petnnet.com. All Rights Reserved</p>
                </div>
                <div class="col-md-6 text-right flip sm-text-center">
                    <div class="widget no-border m-0">
                        <ul class="styled-icons icon-dark icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </footer>
    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url("assets/user_assets/js/custom.js");?>"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
(Load Extensions only on Local File Systems ! 
 The following part can be removed on Server for On Demand Loading) -->
 	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.actions.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.migration.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js");?>"></script>
	<script src="<?php echo base_url("assets/user_assets/js/revolution-slider/js/extensions/revolution.extension.video.min.js");?>"></script>