<div class="body-overlay"></div>
<div id="side-panel" class="dark layer-overlay overlay-white-8" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/bg1.jpg');?>">
  <div class="side-panel-wrap">
    <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon_close font-30"></i></a></div>
    <div class="side-panel-widget">
      <div class="widget">
        <a href="javascript:void(0)"><img alt="logo" src="<?php echo base_url('assets/user_assets/images/logo-wide.png');?>"></a>
      </div>
      <div class="widget">
        <nav>
          <ul class="nav nav-list">
            <li><a href="<?php echo base_url('member_login');?>" class="btn btn-theme-colored">Become a Member</a></li>
            <li><a href="<?php echo base_url('business_Logn');?>" class="btn btn-theme-colored">Registration for Business</a></li>
            
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>