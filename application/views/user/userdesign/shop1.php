<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>
<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="css/menuzord-megamenu.css" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">
<link href="css/mystyle.css" rel="stylesheet" type="text/css">
<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- Header -->
  <?php include("header.php"); ?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Pet Shop</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Pet Services</a></li>
                <li class="active text-silver-gray">Pet Shop</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-md-9 blog-pull-right">
          <div class="row">
           <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-2">
            <div class="upcoming-events bg-white-f3 mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="https://placehold.it/220x160" alt="...">
                  </div>
                </div>
                
                <div class="col-sm-5 col-xs-8 p-0 pl-xs-30">
                  <div class="event-count pb-15">
                    <h4 class="media-heading mt-15">Shop Name</h4>
                    <ul>
                      <li class="text-theme-colored font-12"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored font-12"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-4 p-0">
                  <div class="event-count pt-15">
                   <a class="btn btn-dark btn-theme-colored btn-sm mt-10" href="products.php">Products</a>
                   <a href="shop-detail.php" class="btn btn-dark btn-sm mt-10">Details</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
            
            
            
            <div class="">
              <div class="col-sm-12">
                <nav>
                  <ul class="pagination theme-colored pull-right xs-pull-center mb-xs-40">
                    <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li>
                  </ul>
                </nav>
              </div>
            </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="sidebar sidebar-left mt-sm-30">
              <div class="widget">
                <h5 class="widget-title">Search By Category</h5>
                <form method="post" action="#">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <input class="form-control" type="text" required="" placeholder="Contry Name" aria-required="true">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <input class="form-control" type="text" required="" placeholder="City Name" aria-required="true">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <input class="form-control" type="text" required="" placeholder="Shop Type" aria-required="true">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <input name="form_appontment_date" class="form-control required date-picker" type="text" placeholder="Appoinment Date" aria-required="true">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <input name="form_appontment_time" class="form-control required time-picker" type="text" placeholder="Oppening Time" aria-required="true">
                    </div>
                  </div>
                </div>
                
                <div class="form-group mb-0 mt-20">
                  <button type="submit" class="btn btn-dark btn-theme-colored">Search</button>
                </div>
              </form>
              </div>
              <div class="widget">
                <h5 class="widget-title">Search By Products</h5>
                <ul class="list list-divider list-border">
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                </ul>
              </div>
              
              
              
            </div>
          </div> 
         </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <!-- Footer -->
  <?php include("footer.php"); ?>
</div>
<!-- end wrapper -->
<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>
</body>
</html>
