<header id="header" class="header">
    <!--<div class="header-top bg-theme-colored2 sm-text-center hidden-xs">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="widget no-border m-5 text-white">
              <i class="fa fa-clock-o text-white"></i> 24 x 7 Hours support
            </div>
          </div>
          <div class="col-md-4">
              <div class="widget no-border m-0">
                <ul class="list-inline text-right flip sm-text-center mt-5">
                  <li>
                    <a class="text-white" href="#">FAQ</a>
                </li>
                  <li class="text-white">|</li>
                  <li>
                    <a class="text-white" href="#">Help Desk</a>
                  </li>
                  <li class="text-white">|</li>
                  <li>
                    <a class="text-white" href="contact.php">Contact</a>
                  </li>
                </ul>
              </div>
          </div>
        </div>
      </div>
    </div>-->
    <div class="header-middle p-0 bg-lighter xs-text-center hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3">
              <a class="menuzord-brand pull-left flip sm-pull-center" href="index.php"><img src="images/logo-wide.png" alt=""></a>
            </div>
          <div class="col-xs-12 col-sm-4 col-md-5">
              <div class="widget no-border sm-text-center mt-15 mb-10 m-0">
                <a class="btn btn-colored btn-theme-colored" href="#"><i class="fa fa-user-plus"></i> Sign up Member</a>
                                <a class="btn btn-colored btn-theme-colored" href="#"><i class="fa fa-sign-in"></i> Register for Business</a>

              </div>
            </div>
          
          <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="widget no-border sm-text-center mt-15 mb-10 m-0">
                <form class="">
                <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-colored btn-theme-colored dropdown-toggle" data-toggle="dropdown">
                    	<span id="search_concept">Category</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">All Category</a></li>
                      <li><a href="#">Category</a></li>
                      <li><a href="#">Category</a></li>
                      <li><a href="#">Category</a></li>
                      <li><a href="#">Special Category </a></li>
                      <li class="divider"></li>
                      <li><a href="#">Other Categories</a></li>
                    </ul>
                </div>
                <input name="search_param" value="all" id="search_param" type="hidden">         
                <input class="form-control" name="x" placeholder="Search term..." type="text" style="height:40px;">
                <span class="input-group-btn">
                    <button class="btn btn-colored btn-theme-colored" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
                </form>
              </div>
          </div>
        </div>
    </div>
    </div>
    <div class="header-nav">
        <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
          <div class="container">
            <nav id="menuzord" class="menuzord no-bg">
             <a class="menuzord-brand pull-left flip sm-pull-none hidden-lg hidden-md" href="index.php"><img src="images/logo-wide.png" alt=""></a>
              <ul class="menuzord-menu">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="#">Pet Services</a>
              	<ul class="dropdown">
              		<li><a href="shop.php">Pet Shops</a></li>
              		<li><a href="doctor.php">Pet Pet doctors / veterinarians</a></li>
              		<li><a href="#">Pet trainers</a></li>
              		<li><a href="#">Pet Hostels</a></li>
              		<li><a href="#">Pet Sitters</a></li>
              		<li><a href="#">Pet Travel Companies / Handlers</a></li>
              		<li><a href="#">Pet activity diary / schedule platform</a></li>
              		<li><a href="#">Pet Breeding</a></li>
              		<li><a href="#">Pet Grooming Centers & Spas</a></li>
              		<li><a href="#">Pet Event Organizers</a></li>
              		<li><a href="#">Pet Education / Courses / Course Providers </a></li>
              		<li><a href="#">Pet food / accessories</a></li>
                 </ul>
              </li>
              <li><a href="#">Daily Tips</a></li>
              <li><a href="#">Daily Care</a></li>
              <li><a href="#">Expert Advice</a></li>
              <li><a href="#">Pet charity / shelters / help</a></li>
              <li><a href="#">Lost/Found</a></li>
              <li><a href="#">Contact</a></li>
              </ul>
              
            </nav>
          </div>
        </div>
    </div>
  </header>