<header id="header" class="header">
    <div class="header-top bg-theme-colored2 sm-text-center hidden-xs">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="widget no-border m-5 text-white">
              <i class="fa fa-clock-o text-white"></i> 24 x 7 Hours support
            </div>
          </div>
          <div class="col-md-4">
              <div class="widget no-border m-0">
                <ul class="list-inline text-right flip sm-text-center mt-5">
                  <li>
                    <a class="text-white" href="#">FAQ</a>
                </li>
                  <li class="text-white">|</li>
                  <li>
                    <a class="text-white" href="#">Help Desk</a>
                  </li>
                  <li class="text-white">|</li>
                  <li>
                    <a class="text-white" href="contact.php">Contact</a>
                  </li>
                </ul>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle p-0 bg-lighter xs-text-center hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3">
              <a class="menuzord-brand pull-left flip sm-pull-center" href="index.php"><img src="images/logo-wide1.png" alt=""></a>
            </div>
          <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="widget no-border sm-text-center mt-10 mb-10 m-0">
                <i class="fa fa-envelope text-theme-colored font-32 mt-5 mr-sm-0 sm-display-block pull-left flip sm-pull-none"></i>
                <a href="#" class="font-12 text-gray text-uppercase">Mail Us Today</a>
                <h5 class="font-13 text-black m-0"> info@yourdomain.com</h5>
              </div>
            </div>
          <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="widget no-border sm-text-center mt-10 mb-10 m-0">
                <i class="fa fa-phone-square text-theme-colored font-32 mt-5 mr-sm-0 sm-display-block pull-left flip sm-pull-none"></i>
                <a href="#" class="font-12 text-gray text-uppercase">Call us for more details</a>
                <h5 class="font-13 text-black m-0"> +(012) 345 6789</h5>
              </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="widget no-border sm-text-center mt-10 mb-10 m-0">
                <i class="fa fa-building-o text-theme-colored font-32 mt-5 mr-sm-0 sm-display-block pull-left flip sm-pull-none"></i>
                <a href="#" class="font-12 text-gray text-uppercase">Company Location</a>
                <h5 class="font-13 text-black m-0"> 121 King Street, Melbourne</h5>
              </div>
          </div>
        </div>
    </div>
    </div>
    <div class="header-nav">
        <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
          <div class="container">
            <nav id="menuzord" class="menuzord no-bg">
             <a class="menuzord-brand pull-left flip sm-pull-none hidden-lg hidden-md" href="index.php"><img src="images/logo-wide.png" alt=""></a>
              <ul class="menuzord-menu">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="#">Pet Services</a>
              	<ul class="dropdown">
              		<li><a href="#">Pet Hostels</a></li>
              		<li><a href="#">Pet Travel Companies / Handlers</a></li>
              		<li><a href="#">Pet Grooming Centers & Spas</a></li>
              		<li><a href="#">Pet Event Organizers</a></li>
              		<li><a href="#">Pet Education / Courses / Course Providers </a></li>
              		<li><a href="#">Pet Products</a></li>
                 </ul>
              </li>
              <li><a href="shop.php">Pet Shops</a></li>
              <li><a href="doctor.php">Pet Doctors</a></li>
              <li><a href="#">Pet Sitters</a></li>
              <li><a href="#">Pet Trainers</a></li>
              <li><a href="#">Pet Breeding</a></li>
              <li><a href="#">Lost/Found</a></li>
              <li class="pull-right"><a class="btn btn-colored btn-flat btn-theme-colored" href="#" style="color:#fff;">Register for Business</a></li>
              </ul>
              
            </nav>
          </div>
        </div>
    </div>
  </header>