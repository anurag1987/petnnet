<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>
<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="css/menuzord-megamenu.css" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">
<link href="css/mystyle.css" rel="stylesheet" type="text/css">
<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- Header -->
  <?php include("header.php"); ?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Pet Sitter</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Pet Services</a></li>
                <li class="active text-silver-gray">Pet Sitter</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-md-9 blog-pull-right">
          <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="images/per1.jpg" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppersittercase font-weight-500">Pet Sitter Name</h4>
                    <p class="team-subtitle">Speciality</p>
                    <p class="team-subtitle">Qualification</p>
                    <p class="team-subtitle">Exp: 3 Years</p>
                    <!--<div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>-->
                    <a href="-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <a href="appointment.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Appointment <i class="fa fa-angle-double-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="images/per1.jpg" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppersittercase font-weight-500">Pet Sitter Name</h4>
                    <p class="team-subtitle">Speciality</p>
                    <p class="team-subtitle">Qualification</p>
                    <p class="team-subtitle">Exp: 3 Years</p>
                    <!--<div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>-->
                    <a href="-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <a href="appointment.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Appointment <i class="fa fa-angle-double-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="images/per1.jpg" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppersittercase font-weight-500">Pet Sitter Name</h4>
                    <p class="team-subtitle">Speciality</p>
                    <p class="team-subtitle">Qualification</p>
                    <p class="team-subtitle">Exp: 3 Years</p>
                    <!--<div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>-->
                    <a href="-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <a href="appointment.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Appointment <i class="fa fa-angle-double-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="images/per1.jpg" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppersittercase font-weight-500">Pet Sitter Name</h4>
                    <p class="team-subtitle">Speciality</p>
                    <p class="team-subtitle">Qualification</p>
                    <p class="team-subtitle">Exp: 3 Years</p>
                    <!--<div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>-->
                    <a href="-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <a href="appointment.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Appointment <i class="fa fa-angle-double-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="images/per1.jpg" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppersittercase font-weight-500">Pet Sitter Name</h4>
                    <p class="team-subtitle">Speciality</p>
                    <p class="team-subtitle">Qualification</p>
                    <p class="team-subtitle">Exp: 3 Years</p>
                    <!--<div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>-->
                    <a href="-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <a href="appointment.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Appointment <i class="fa fa-angle-double-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="upcoming-events bg-white-f9 box-shadow mb-20">
              <div class="row">
                <div class="col-sm-4 pr-0 pr-sm-15">
                  <div class="thumb p-15">
                    <img class="img-fullwidth" src="images/per1.jpg" alt="...">                </div>
                </div>
                <div class="col-sm-5 pl-0 pl-sm-15">
                  <div class="event-details p-15 mt-0">
                    <h4 class="media-heading text-uppersittercase font-weight-500">Pet Sitter Name</h4>
                    <p class="team-subtitle">Speciality</p>
                    <p class="team-subtitle">Qualification</p>
                    <p class="team-subtitle">Exp: 3 Years</p>
                    <!--<div class="star-rating ml-0" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>-->
                    <a href="-detail.php" class="btn btn-flat btn-dark btn-theme-colored btn-sm">Details <i class="fa fa-angle-double-right"></i></a>
                    <a href="appointment.php" class="btn btn-dark btn btn-theme-colored2 btn-sm"> Appointment <i class="fa fa-angle-double-right"></i></a>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="event-count pt-30">
                    <ul>
                      <li class="mb-10 text-black-666"><i class="fa fa-envelope mr-5"></i> info@domain.com</li>
                      <li class="mb-10 text-black-666"><i class="fa fa-phone mr-5"></i> +91 9999999999</li>
                    </ul>
                    <ul>
                      <li class="mb-10 text-theme-colored"><i class="fa fa-clock-o mr-5"></i> at 5.00 pm - 7.30 pm</li>
                      <li class="text-theme-colored"><i class="fa fa-map-marker mr-5"></i> 25 Newyork City.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            
            
            
            
            
            
            <div class="row">
              <div class="col-sm-12">
                <nav>
                  <ul class="pagination theme-colored pull-right xs-pull-center mb-xs-40">
                    <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="sidebar sidebar-left mt-sm-30 box-shadow p-15">
              <div class="widget">
                <h5 class="widget-title">Search By Category</h5>
                <form method="post" action="#">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="input-group mb-10">
                        <input placeholder="Click to Search" class="form-control search-input" type="text">
                        <span class="input-group-btn">
                        <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                        </span>
                      </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
  <select class="form-control" id="sel1" placeholder="Select City">
    <option>Select City</option>
    <option>Delhi</option>
    <option>Mumgai</option>
    <option>Chenai</option>
    <option>Kolkata</option>
  </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                     <select class="form-control" id="sel1" placeholder="Select Doctor Type">
    <option>Doctor Type</option>
    <option>Cunsultant</option>
    <option>Surgon</option>
  </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                     <select class="form-control" id="sel1" placeholder="Select Time">
    <option>7:00 AM</option>
    <option>7:30 AM</option>
    <option>8:00 AM</option>
    <option>8:30 AM</option>
    <option>9:00 AM</option>
    <option>9:30 AM</option>
    <option>10:00 AM</option>
    <option>10:30 AM</option>
    <option>11:00 AM</option>
  </select>
                    </div>
                  </div>
                  
                </div>
                
                <div class="form-group mb-0 mt-20">
                  <button type="submit" class="btn btn-dark btn-theme-colored">Search</button>
                </div>
              </form>
              </div>
              <div class="widget">
                <h5 class="widget-title">Search By Products</h5>
                <ul class="list list-divider list-border">
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                </ul>
              </div>
              
              
              
            </div>
          </div> 
         </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <!-- Footer -->
  <?php include("footer.php"); ?>
</div>
<!-- end wrapper -->
<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>
</body>
</html>
