<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>
<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="css/menuzord-megamenu.css" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">
<link href="css/mystyle.css" rel="stylesheet" type="text/css">
<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- Header -->
  <?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">Pet Activity Diary / Schedule platform</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Services</a></li>
                <li class="active text-theme-colored">Pet Activity Diary / Schedule platform</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

<section class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class=" mt-0 line-height-1">Pet Activity  <span class="text-theme-colored2">Diary / Schedule</span> platform</h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
          <p align="justify"> Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati.</p>
        </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <?php include("footer.php"); ?>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

</body>
</html>