<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>PetNnet</title>
<meta name="author" content="">
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7CMontserrat:400,500">
<?php echo link_tag('login_assets/css/bootstrap.min.css');?>
<?php echo link_tag('login_assets/css/fontawesome-all.min.css');?>
<?php echo link_tag('login_assets/css/jquery-ui.min.css');?>
<?php echo link_tag('login_assets/css/perfect-scrollbar.min.css');?>
<?php echo link_tag('login_assets/css/morris.min.css');?>
<?php echo link_tag('login_assets/css/select2.min.css');?>
<?php echo link_tag('login_assets/css/jquery-jvectormap.min.css');?>
<?php echo link_tag('login_assets/css/horizontal-timeline.min.css');?>
<?php echo link_tag('login_assets/css/weather-icons.min.css');?>
<?php echo link_tag('login_assets/css/dropzone.min.css');?>
<?php echo link_tag('login_assets/css/ion.rangeSlider.min.css');?>
<?php echo link_tag('login_assets/css/ion.rangeSlider.skinFlat.min.css');?>
<?php echo link_tag('login_assets/css/datatables.min.css');?>
<?php echo link_tag('login_assets/css/fullcalendar.min.css');?>
<?php echo link_tag('login_assets/css/style.css');?>
</head>
<body>
<div class="wrapper">
  <div class="m-account-w" data-bg-img="<?php echo base_url('login_assets/img/account/wrapper-bg.jpg');?>">
    <div class="m-account">
      <div class="row no-gutters">
        <div class="container">
        <div class="col-md-6 col-md-offset-3" style="margin:0 auto;">
          <div class="m-account--form-w">
            <div class="m-account--form">
              <div class="logo"> <img src="login_assets/img/logo.png" alt=""> </div>
              <?php echo form_open('Admin_login/updatepass');?>
               <label class="m-account--title">Change Your Admin Password</label><br>
				<?php if($error = $this->session->flashdata('error')){?>
               <div class="text-danger"><?php echo $error;?></div>
               <?php }?>
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend"> <i class="fas fa-user"></i> </div>
                    <?php echo form_input(['name'=>'email','class'=>'form-control','placeholder'=>'Registered Email ID']);?><br>
					<?php echo form_error('username');?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend"> <i class="fas fa-key"></i> </div>
                    <?php echo form_password(['name'=>'password','class'=>'form-control','placeholder'=>'New Password']);?> 
                    <?php echo form_error('password');?>
                  </div>
                </div>
                <div class="m-account--actions"> <a href="#" class="btn-link"></a>
                 <?php echo form_submit(['name'=>'submit','value'=>'Update','class'=>'btn btn-rounded btn-info']);?> 
                </div>
                
                <div class="m-account--footer">
                  <p>&copy; 2018 PetNnet</p>
                </div>
              <?php echo form_close();?>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url('login_assets/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/jquery-ui.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/bootstrap.bundle.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/perfect-scrollbar.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/jquery.sparkline.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/raphael.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/morris.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/select2.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/jquery-jvectormap.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/jquery-jvectormap-world-mill.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/horizontal-timeline.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/jquery.validate.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/jquery.steps.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/dropzone.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/ion.rangeSlider.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/datatables.min.js');?>"></script>
<script src="<?php echo base_url('login_assets/js/main.js');?>"></script>
</body>
</html>
