  <?php include_once dirname(__FILE__).('/../header.php');?>
  <?php $arr = [];	foreach($result as $value):?>
  <?php $arr = $value;?>
    <?php endforeach;?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
  <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Manage Profile</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="<?php echo base_url('business');?>">Home</a></li>
                <li class="active text-silver-gray"> Profile</li>
               
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <div align="center"><img src="<?php echo base_url('assets/image/business/'.$arr->mem_image);?> " alt=""></div>
              </div>
              <h3 class="name font-24 mt-20 mb-0 text-center"><?php echo $arr->names;?></h3>
              <h4 class="mt-5 text-theme-colored text-center">Member Id : #<?php echo $arr->memid;?></h4>
              <?php include('menu.php');?>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-12">
                   <?php if($success = $this->session->flashdata('success')):?>
                    <div class="alert alert-success"><?php echo $success;?></div>
                    <?php endif;?>
                     <?php if($error = $this->session->flashdata('error')):?>
                    <div class="alert alert-success"><?php echo $error;?></div>
                    <?php endif;?>

                  <h4 class="line-bottom mt-0">Update Profile</h4>
                  <form id="mreg_form" name="mreg_form" action="<?php echo base_url('member/updateprofile/'.$arr->memid);?>" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Name  </label>
                      <input name="names" type="text" placeholder="Enter Name" class="form-control" value="<?php echo $arr->names;?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Email  </label>
                      <input name="email" class="form-control " type="email" placeholder="Enter Email"  value="<?php echo $arr->email;?>">
                    </div>
                  </div>
                </div>
                <div class="row">               
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Mobile  </label>
                      <input name="mobile" class="form-control" type="text" placeholder="Enter phone No." value="<?php echo $arr->mobile;?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Member Type  </label>
                      <select class="form-control required email" name="mem_type">
		<option>Select Services</option>
		<option value="Buyer" <?php if($arr->mem_type=='Buyer'){?>selected <?php }?> >Buyer</option>
		<option value="Owner" <?php if($arr->mem_type=='Owner'){?>selected <?php }?> >Owner</option>
		<option value="Both" <?php if($arr->mem_type=='Both'){ ?> selected <?php }?> >Both</option>
			  </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Country  </label>
                      <input name="country" type="text" placeholder="Enter country"  class="form-control" value="<?php echo $arr->country;?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>State  </label>
                      <input name="state" class="form-control " type="text" placeholder="Enter state"  value="<?php echo $arr->state;?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>City  </label>
                      <input name="city" type="text" placeholder="Enter city" class="form-control" value="<?php echo $arr->city;?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>  </label>
                     
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Address  </label>
                  <textarea name="address" class="form-control" rows="5" placeholder="Adress"><?php echo $arr->city;?></textarea>
                </div>
                <!--<div class="form-group">
                  <label>Image  </label>
                  <input type="file" name="profileimg" value="<?php echo $arr->city;?>" ><br>
                  <img src="<?php echo base_url('assets/image/business/'.$arr->mem_image);?>" width="150" >
                </div>-->
                
                <div class="form-group">
                  <input name="form_botcheck" class="form-control" type="hidden" value="" />
                  <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Update Now</button>
                </div>
              </form>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <?php include_once dirname(__FILE__).('/../footer.php');?>

</body>
</html>
