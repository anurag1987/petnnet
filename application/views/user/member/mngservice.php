  <?php include_once dirname(__FILE__).('/../header.php');?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Manage Services</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="<?php echo base_url('member');?>">Home</a></li>
                <li class="active text-silver-gray">Services</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <div align="center"><img src="" alt=""></div>
              </div>
              <h3 class="name font-24 mt-20 mb-0 text-center">Aria Martin</h3>
              <h4 class="mt-5 text-theme-colored text-center">Member Id : #2564325GH</h4>
               <?php include_once("menu.php");?>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="line-bottom mt-0">Manage Services</h4>
                  <form method="post" enctype="multipart/form-data" action="<?php echo base_url('member/addservice');?>">
                <div class="form-group">
                    <label>Service Title <small>*</small></label>
                 <input type="text" name="title" class="form-control"> 
                 <?php echo form_error('title');?>
                </div>
                <div class="form-group">
                  <label>Description <small>*</small></label>
                  
                  <textarea name="description" class="form-control required" rows="5" placeholder="Adress">
                  	
                  </textarea>
                  <?php echo form_error('description');?>
                </div>
                <div class="form-group">
                  <label>Image <small>*</small></label>
                   <input type="file" name="serimg" class="form-control"> 
                </div>
                
                <div class="form-group">
                  <input name="form_botcheck" class="form-control" type="hidden" value="" />
                  <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Add Now</button>
                </div>
             </form>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <?php include_once dirname(__FILE__).('/../footer.php');?>

</body>
</html>
