  <?php include_once dirname(__FILE__).('/../header.php');?>
  <?php $arr = [];	foreach($result as $value):?>
  <?php $arr = $value;?>
    <?php endforeach;?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
  <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Manage Product</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="<?php echo base_url('business');?>">Home</a></li>
                <li class="active text-silver-gray">Add Product</li>
               
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <div align="center"><img src="<?php echo base_url('assets/image/business/'.$arr->mem_image);?> " alt=""></div>
              </div>
              <h3 class="name font-24 mt-20 mb-0 text-center"><?php echo $arr->names;?></h3>
              <h4 class="mt-5 text-theme-colored text-center">Member Id : #<?php echo $arr->memid;?></h4>
              <?php include('menu.php');?>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-12">
                  <?php if($success = $this->session->flashdata('success')):?>
                    <div class="alert alert-success"><?php echo $success;?></div>
                    <?php endif;?>
                     <?php if($error = $this->session->flashdata('error')):?>
                    <div class="alert alert-danger"><?php echo $error;?></div>
                    <?php endif;?>
                  <h4 class="line-bottom mt-0">Change Password</h4>
                  <form id="mreg_form" name="mreg_form" action="<?php echo base_url('member/changerpassword');?>" method="post" >
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Current Password<small style="color: red">*</small></label>
                      <input name="oldpass" type="password" placeholder="Old Password"  class="form-control">
                      <?php echo form_error('oldpass');?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>New Password <small style="color: red">*</small></label>
                      <input name="newpass" type="password" placeholder="New Password"  class="form-control">
                    <?php echo form_error('newpass');?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Confirm New Password <small style="color: red">*</small></label>
                      <input name="confirmpass" type="password" placeholder="Confirm New Password" class="form-control">
                      <?php echo form_error('confirmpass');?>
                    </div>
                  </div>
                </div>
                
                
                <div class="col-md-2">
                <div class="form-group">
                  <input name="form_botcheck" class="form-control" type="hidden" value="" />
                  <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Update Now</button>
                </div>
                </div>
              </form>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <?php include_once dirname(__FILE__).('/../footer.php');?>
</body>
</html>
