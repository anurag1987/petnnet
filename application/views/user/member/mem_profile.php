
  <?php include_once dirname(__FILE__).('/../header.php');?>
  <?php $arr = [];	foreach($result as $value):?>
  <?php $arr = $value;?>
  <?php endforeach;?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Manage Services</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="<?php echo base_url('member');?>">Home</a></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <div align="center"><img src="<?php echo base_url('assets/image/memimg/'.$arr->mem_image);?>" alt=""></div>
              </div>
              <h3 class="name font-24 mt-20 mb-0 text-center"><?php echo $arr->names;?></h3>
              <h4 class="mt-5 text-theme-colored text-center">Member Id : #<?php echo $arr->memid;?></h4>
              <?php include_once("menu.php");?>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="line-bottom mt-0">Profile</h4>
                  <div class="volunteer-address">
                    <div class="row">
                      <ul>
                        <li class="pull-left col-md-6">
                          <div class="bg-light media border-bottom p-15">
                            <div class="media-left"> <i class="fa fa-phone text-theme-colored font-24 mt-5"></i> </div>
                            <div class="media-body">
                              <h5 class="mt-0 mb-0">Contact:</h5>
                              <p><span>Phone:</span> +91<?php echo $arr->mobile;?><br>
                                <span>Email:</span> <?php echo $arr->email;?></p>
                            </div>
                          </div>
                        </li>
                        <li class="pull-left col-md-6">
                          <div class="bg-light media border-bottom p-15 mb-20">
                            <div class="media-left"> <i class="fa fa-map-marker text-theme-colored2 font-24 mt-5"></i> </div>
                            <div class="media-body">
                              <h5 class="mt-0 mb-0">Address:</h5>
                              <p><?php echo $arr->address;?></p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 mt-0">
                  <div class="border-bottom mb-20">
                    <h4 class="theme-colored pull-left mt-0"> Manage Pets</h4>
                    <a href="#" class="btn-theme-colored2 btn-sm pull-right">Veiw All</a> <a href="#" class="btn-theme-colored2 btn-sm pull-right mr-20">Manag</a>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <table class="table table-bordered tbl-pad-2">
                        <tr>
                          <td><strong>#</strong></td>
                          <td><strong>Photo</strong></td>
                          <td><strong>Pet Name</strong></td>
                          <td><strong>Remark</strong></td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><img src="images/gs.jpg" class="img-circle img-thumbnail"></td>
                          <td>Dog</td>
                          <td>german shepherd dog</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td><img src="images/gs.jpg" class="img-circle img-thumbnail"></td>
                          <td>Dog</td>
                          <td>german shepherd dog</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td><img src="images/gs.jpg" class="img-circle img-thumbnail"></td>
                          <td>Dog</td>
                          <td>german shepherd dog</td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="border-bottom clearfix">
                    <h4 class="theme-colored pull-left mt-0"> Manage Order</h4>
                    <a href="#" class="btn-theme-colored2 btn-sm pull-right">Veiw All</a> <a href="#" class="btn-theme-colored2 btn-sm pull-right mr-20">Manag</a>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <table class="table table-bordered tbl-pad-2">
                        <tr>
                          <td><strong>#</strong></td>
                          <td><strong>Photo</strong></td>
                          <td><strong>Qnt.</strong></td>
                          <td><strong>Product Name</strong></td>
                          <td><strong>Price</strong></td>
                          <td><strong>Order Date</strong></td>
                          <td><strong>Status</strong></td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><img src="images/pf1.jpg" class="img-circle img-thumbnail"></td>
                          <td>2</td>
                          <td>Dog</td>
                          <td>$200.00</td>
                          <td>12/08/2018</td>
                          <td><a href="#" class="text-success">Deleverd</a></td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><img src="images/pf1.jpg" class="img-circle img-thumbnail"></td>
                          <td>2</td>
                          <td>Dog</td>
                          <td>$200.00</td>
                          <td>12/08/2018</td>
                          <td><a href="#" class="text-danger">Pending</a></td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><img src="images/pf1.jpg" class="img-circle img-thumbnail"></td>
                          <td>2</td>
                          <td>Dog</td>
                          <td>$200.00</td>
                          <td>12/08/2018</td>
                          <td><a href="#" class="text-success">Deleverd</a></td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td><img src="images/pf1.jpg" class="img-circle img-thumbnail"></td>
                          <td>2</td>
                          <td>Dog</td>
                          <td>$200.00</td>
                          <td>12/08/2018</td>
                          <td><a href="#" class="text-success">Deleverd</a></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <?php include_once dirname(__FILE__).('/../footer.php');?>
  
	</body>
</html>
