
  <?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Update Password</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="#">Home</a></li>
                <li><a href="#">Login</a></li>
                <li class="active text-silver-gray">New Password</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-push-3">
            <div class="border-1px p-25">
              <h4 class="text-theme-colored text-uppercase m-0">Forgot Password</h4>
              <div class="line-bottom mb-10"></div>
              <p>Please Email For Get Password</p>
              <?php $reset_key = $this->uri->segment(2);
				echo form_open('Welcome/reset_validation');?>
               <?php form_hidden(['name'=>'reset_key','value'=>set_value('reset_key',$reset_key)]);?>
                <div class="row">
                  
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <?php echo form_password(['name'=>'password','class'=>'form-control required email','placeholder'=>'Enter New Password ']);?>
                    <?php echo form_error('email');?>
                     </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                      <?php echo form_password(['name'=>'confirmpass','class'=>'form-control required email','placeholder'=>'Enter Confirm Password ']);?>
                    <?php echo form_error('confirmpass');?>
                     </div>
                  </div>
                  
                </div>
                
                <div class="form-group mb-0 mt-20">
                  <button type="submit" class="btn btn-dark btn-theme-colored" data-loading-text="Please wait...">Get Password</button>
                </div>
              <?php echo form_close();?>
              <!-- Appointment Form Validation-->
              <script>
                $("#appointment_form").validate({
                  submitHandler: function(form) {
                    var form_btn = $(form).find('button[type="submit"]');
                    var form_result_div = '#form-result';
                    $(form_result_div).remove();
                    form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                    var form_btn_old_msg = form_btn.html();
                    form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                    $(form).ajaxSubmit({
                      dataType:  'json',
                      success: function(data) {
                        if( data.status === 'true' ) {
                          $(form).find('.form-control').val('');
                        }
                        form_btn.prop('disabled', false).html(form_btn_old_msg);
                        $(form_result_div).html(data.message).fadeIn('slow');
                        setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                      }
                    });
                  }
                });
              </script>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <?php include("footer.php"); ?>

</body>
</html>