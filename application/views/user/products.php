  <?php include("header.php"); ?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
   <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Products</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="<?php echo base_url('welcome');?>">Home</a></li>
                <li><a href="#">Shop Name</a></li>
                <li class="active text-silver-gray"Products</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
          <div class="col-md-3">
            <div class="sidebar sidebar-left mt-sm-30 box-shadow p-15">
              <div class="widget">
                <h5 class="widget-title">Filter By Category</h5>
                <form method="post" action="#">
                <div class="row">
                  
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
  <select class="form-control" id="sel1" placeholder="Products Type">
    <option>Products Type</option>
    <option>Food</option>
    <option>Cloth</option>
    <option>Medicines</option>
    <option>Grooming Products</option>
  </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                     <select class="form-control" id="sel1" placeholder="Price Range">
    <option>Price Range</option>
    <option>$100-$150</option>
    <option>$100-$150</option>
    <option>$100-$150</option>
    <option>$100-$150</option>
    <option>$100-$150</option>
    <option>$100-$150</option>
    <option>$100-$150</option>
  </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-10">
                     <select class="form-control" id="sel1" placeholder="Brand Name">
    <option>Brand Name</option>
    <option>Brand Name</option>
    <option>Brand Name</option>
    <option>Brand Name</option>
    <option>Brand Name</option>
    <option>Brand Name</option>
    <option>Brand Name</option>
    <option>Brand Name</option>
  </select>
                    </div>
                  </div>
                  
                </div>
                
                
              </form>
              </div>
              <div class="widget">
                <h5 class="widget-title">Filter By Products</h5>
                <ul class="list list-divider list-border">
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Grooming Prooducts</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Pets Medical Stores</a></li>
                  <li><a href="#"><i class="fa fa-check-square-o mr-10 text-black-light"></i> Perts Accesoseries</a></li>
                </ul>
              </div>
              
              
              
            </div>
          </div>
            <div class="col-md-9">
              <div class="products">
                <div class="row multi-row-clearfix">
                  <?php foreach($result as $value):?>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <span class="tag-sale">Sale!</span>
                      <div class="product-thumb"> <img alt="" src="<?php echo base_url('assets/image/product/'.$value->proimg);?>" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                       <h5 class="product-title"><a href="<?php echo base_url('welcome/productDetalis/'.$value->product_id);?>"><?php echo $value->title;?></a></h5>
                        <div class="star-rating" title="Rated 4.50 out of 5"><span data-width="90%">3.50</span></div>
                        <div class="price"><del><span class="amount"><?php echo $value->actual_price;?></span></del><ins><span class="amount"><?php echo $value->sale_price;?></span></ins></div>
                        <div class="btn-add-to-cart-wrapper">
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#">Add To Cart</a>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach;?>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <nav>
                      <ul class="pagination theme-colored">
                        <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">...</a></li>
                        <li> <a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <!-- Footer -->
  <?php include("footer.php"); ?>

</body>
</html>
