  <?php include("header.php"); ?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="<?php echo base_url('assets/user_assets/images/bg/b1.jpg');?>">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">Daiy Tips</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <!--<li><a href="#">Pages</a></li>-->
                <li class="active text-theme-colored">Daiy Tips</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

<section class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class=" mt-0 line-height-1">Daily <span class="text-theme-colored2">Tips</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="<?php echo base_url('assets/user_assets/images/title-icon.png');?>" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-4">
              <div class="item text-center p-20">
                <img src="<?php echo base_url('assets/user_assets/images/flaticon-pet/1.png');?>" alt="">
                <h3>Pet Health</h3>
                <p>Lorem ipsum dolor sit amet rutrum nibh est eu sodales diam eu aenean mauris maecenas lacus vel Lorem iaculis mus.</p>
              </div>
              <div class="item text-center p-20">
                <img src="<?php echo base_url('assets/user_assets/images/flaticon-pet/2.png');?>" alt="">
                <h3>Pet Accessories</h3>
                <p>Lorem ipsum dolor sit amet rutrum nibh est eu sodales diam eu aenean mauris maecenas lacus vel Lorem iaculis mus.</p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="item text-center p-20">
                <img src="<?php echo base_url('assets/user_assets/images/flaticon-pet/3.png');?>" alt="">
                <h3>Pet Wellness</h3>
                <p>Lorem ipsum dolor sit amet rutrum nibh est eu sodales diam eu aenean mauris maecenas lacus vel Lorem iaculis mus.</p>
              </div>
              <div class="item text-center p-20">
                <img src="<?php echo base_url('assets/user_assets/images/flaticon-pet/4.png');?>" alt="">
                <h3>Natural & Raw Food</h3>
                <p>Lorem ipsum dolor sit amet rutrum nibh est eu sodales diam eu aenean mauris maecenas lacus vel Lorem iaculis mus.</p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="item text-center p-20">
                <img src="<?php echo base_url('assets/user_assets/images/flaticon-pet/5.png');?>" alt="">
                <h3>Pet Grooming</h3>
                <p>Lorem ipsum dolor sit amet rutrum nibh est eu sodales diam eu aenean mauris maecenas lacus vel Lorem iaculis mus.</p>
              </div>
              <div class="item text-center p-20">
                <img src="<?php echo base_url('assets/user_assets/images/flaticon-pet/6.png');?>" alt="">
                <h3>Pet Adoption</h3>
                <p>Lorem ipsum dolor sit amet rutrum nibh est eu sodales diam eu aenean mauris maecenas lacus vel Lorem iaculis mus.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <?php include("footer.php"); ?>
</body>
</html>