<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="" />
<!-- Page Title -->
<title>PetNnet</title>
<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="css/menuzord-megamenu.css" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">
<link href="css/mystyle.css" rel="stylesheet" type="text/css">
<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- Header -->
  <?php include("header.php"); ?>
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="images/bg/b1.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-center">Pet Doctor</h2>
              <ol class="breadcrumb text-center text-white mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Pet Services</a></li>
                <li class="active text-silver-gray">Pet Doctor</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="bg-silver-light">
      <div class="container pt-30 pb-30">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
                <form class="">
                <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button"  data-height="45px" class="btn btn-colored btn-theme-colored dropdown-toggle" style="height: 45px;" data-toggle="dropdown">
                    	<span id="search_concept">Category</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">All Category</a></li>
                      <li><a href="#">Category</a></li>
                      <li><a href="#">Category</a></li>
                      <li><a href="#">Category</a></li>
                      <li><a href="#">Special Category </a></li>
                      <li class="divider"></li>
                      <li><a href="#">Other Categories</a></li>
                    </ul>
                </div>
                <input name="search_param" value="all" id="search_param" type="hidden">         
                <input class="form-control" name="x" placeholder="Search term..." type="text">
                <span class="input-group-btn">
                    <button data-height="45px" class="btn btn-colored btn-theme-colored" style="height: 45px;" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
                </form>
                </div>
        </div>
      </div>
    </section>
    <!-- start Team Section:-->
    <section class="bg-silver-light">
      <div class="container pt-0">
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel-4col" data-dots="true" data-nav="true">
              <div class="item">
                <div class="team-members maxwidth400">
                  <div class="team-thumb"> <img class="img-fullwidth" alt="" src="images/team/1.jpg">
                    <div class="team-overlay"></div>
                  </div>
                  <div class="team-details text-center p-20">
                    <h3 class="team-title mb-0"><a href="doctor-detail.php">Dr. Smriti Agrawal</a></h3>
                    <p class="team-subtitle">Cunsultant and Surgen</p>
                    <ul class="team-info-list styled-icons">
                      <li><i class="fa fa-map-marker"></i> Singapore</li>
                      <li>Exp: 3 Years</li>
                    </ul>
                    <ul class="team-info-list">
                      <li><i class="fa fa-phone"></i> +91 00000 00000</li>
                      <li><a href="#"><i class="fa fa-envelope"></i> contact@gmail.com</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
  <!-- Footer -->
  <?php include("footer.php"); ?>
</div>
<!-- end wrapper -->
<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>
</body>
</html>
