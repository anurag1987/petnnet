-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2018 at 07:16 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petnnet_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE `admin_tbl` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `loginid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`admin_id`, `name`, `mobile`, `email`, `loginid`, `password`, `website`) VALUES
(1, 'admin', '9999999999', 'admin@gmail.com', '', '1234567', 'petnnet.com');

-- --------------------------------------------------------

--
-- Table structure for table `pernnet_buss_services`
--

CREATE TABLE `pernnet_buss_services` (
  `serviceid` bigint(20) NOT NULL,
  `memid` varchar(255) NOT NULL,
  `serv_title` varchar(255) NOT NULL,
  `serv_desc` text NOT NULL,
  `serv_img` varchar(255) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pernnet_buss_services`
--

INSERT INTO `pernnet_buss_services` (`serviceid`, `memid`, `serv_title`, `serv_desc`, `serv_img`, `created_at`, `status`) VALUES
(1, '1', 'nothing', 'whatevber                  	\r\n                  ', '', 1533806260, '0'),
(2, '3', 'nothing', '                  	\r\nqqq                  ', '', 1533807338, '0'),
(3, '3', 'nothing', '                  	\r\nqqq                  ', '', 1533807459, '0'),
(4, '24', 'nothing', '                  	\r\nqqq                  ', '', 1533807481, '0'),
(5, '24', 'nothing', '                  	\r\nqqq                  ', '', 1533807702, '0'),
(6, '24', 'nothing', '                  	\r\nqqq                  ', '', 1533807781, '0'),
(7, '24', 'nothing', '                  	\r\nqqq                  ', '', 1533807789, '0'),
(8, '24', 'nothing', '                  	\r\nqqq                  ', '', 1533807827, '0'),
(9, '24', 'nothing', 'qqq                  	\r\n                  ', '', 1533807877, '0'),
(10, '24', 'nothing', 'qqq                  	\r\n                  ', '', 1533807915, '0'),
(12, '33', 'this is new post', '                  	\r\nqqqq', '', 1533763791, '0'),
(13, '33', 'programming language', ' qqqq', '', 1533767545, '0'),
(14, '33', 'programming language', ' ss', '', 1533786747, '0'),
(15, '33', 'programming language', ' aaaa', 'best-mobiles-phone-2015.jpg', 1533787314, '0'),
(16, '33', 'programming language', ' sss', 'download.png', 1533787452, '0'),
(17, '33', 'programming language', ' sss', 'best-mobiles-phone-20151.jpg', 1533787542, '0');

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_business`
--

CREATE TABLE `petnnet_business` (
  `memid` bigint(20) NOT NULL,
  `names` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `reset_pass` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `add_date` bigint(20) NOT NULL,
  `mem_type` varchar(50) NOT NULL,
  `mem_image` varchar(255) NOT NULL,
  `otppass` varchar(20) NOT NULL,
  `member_from` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petnnet_business`
--

INSERT INTO `petnnet_business` (`memid`, `names`, `email`, `password`, `reset_pass`, `mobile`, `status`, `country`, `state`, `city`, `address`, `add_date`, `mem_type`, `mem_image`, `otppass`, `member_from`) VALUES
(24, 'karan sahani', 'karansahani98@gmail.com', '1234567', '', '9580671499', 1, '', '', '', '', 1533290754, 'Doctor', '1.PNG', '', ''),
(25, 'ARVIND SAHANI', 'asd@lkj.ckd', 'sD2LUoiq', '', '9580671499', 0, '', '', '', '', 1533290775, 'Doctor', '2.jpeg', '', ''),
(26, 'anurag verma', 'asd@lkj.ckd', 'ZpNMWcqs', '', '9580671499', 0, '', '', '', '', 1533290847, 'Doctor', '3.jpg', '', ''),
(27, 'ARVIND SAHANI', 'tesst@gmail.com', 'n5TD30ti', '', '9580671499', 0, '', '', '', '', 1533250542, 'selected  ', 'Capture.PNG', '', ''),
(28, 'business test', 'business@gamil.com', 'MF8fZY0l', '', '000000000', 0, '', '', '', '', 1533252270, 'Hostels', 'Capture1.PNG', '', ''),
(29, 'ARVIND SAHANI', 'rvrsssvsahani@gmail.com', 'Qdp5AlTi', '', '7668236432', 0, '', '', '', '', 1533512190, 'Hostels', '6c35d2d3-f413-4c0b-af9f-66f0c6fd2588_zps1b436d471.png', '', ''),
(30, 'ARVIND SAHANI', 'tessst@gmail.comss', 'je8M6Gem', '', '7668236432', 0, '', '', '', 'aa', 1533753740, 'Trainer', '5528-tangled3.jpg', '', ''),
(31, 'ARVIND SAHANI', 'qqqtest@gmail.com', 'wRyssjUO', '', '9580671499', 0, '', '', '', 'sss', 1533754610, 'Doctor', '', '', ''),
(32, 'ARVIND SAHANI', 'ssssssss@gmail.com', '4ypkijUJ', '', '7668236432', 0, '', '', '', '7668236432', 1533754716, 'Organizers', '', '', ''),
(33, 'ARVIND SAHANI', 'arni@gmail.com', 'EafQdR8O', '', '9580671499', 0, '', '', '', 'qqq', 1533757579, 'Shop', '1.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_buss_appoinment`
--

CREATE TABLE `petnnet_buss_appoinment` (
  `apointid` bigint(20) NOT NULL,
  `memid` bigint(20) NOT NULL,
  `appoinment_date` bigint(20) NOT NULL,
  `appoinment_time` varchar(50) NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `client_phone` varchar(20) NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `client_address` varchar(255) NOT NULL,
  `client_city` varchar(50) NOT NULL,
  `client_state` varchar(50) NOT NULL,
  `client_pincode` varchar(20) NOT NULL,
  `client_need_service` text NOT NULL,
  `client_for_animal` varchar(255) NOT NULL,
  `client_tot_animal` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_buss_contact`
--

CREATE TABLE `petnnet_buss_contact` (
  `mconid` bigint(20) NOT NULL,
  `mem_id` bigint(20) NOT NULL,
  `pet_business_name` varchar(255) NOT NULL,
  `mem_country` varchar(100) NOT NULL,
  `mem_state` varchar(100) NOT NULL,
  `mem_city` varchar(100) NOT NULL,
  `mem_address` varchar(255) NOT NULL,
  `mem_pincode` varchar(20) NOT NULL,
  `mem_google_map` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_buss_opeing_hour`
--

CREATE TABLE `petnnet_buss_opeing_hour` (
  `memopenid` bigint(20) NOT NULL,
  `mem_id` bigint(20) NOT NULL,
  `mem_mon_from` varchar(50) NOT NULL,
  `mem_tues_from` varchar(50) NOT NULL,
  `mem_wed_from` varchar(50) NOT NULL,
  `mem_thus_from` varchar(50) NOT NULL,
  `mem_fri_from` varchar(50) NOT NULL,
  `mem_sat_from` varchar(50) NOT NULL,
  `mem_sun_from` varchar(50) NOT NULL,
  `mem_mon_to` varchar(50) NOT NULL,
  `mem_tues_to` varchar(50) NOT NULL,
  `mem_wed_to` varchar(50) NOT NULL,
  `mem_thus_to` varchar(50) NOT NULL,
  `mem_fri_to` varchar(50) NOT NULL,
  `mem_sat_to` varchar(50) NOT NULL,
  `mem_sun_to` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_buss_services`
--

CREATE TABLE `petnnet_buss_services` (
  `servid` bigint(20) NOT NULL,
  `memid` bigint(20) NOT NULL,
  `serv_title` varchar(255) NOT NULL,
  `serv_image` varchar(255) NOT NULL,
  `serv_details` text NOT NULL,
  `add_date` bigint(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_member`
--

CREATE TABLE `petnnet_member` (
  `memid` bigint(20) NOT NULL,
  `names` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `reset_pass` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `add_date` bigint(20) NOT NULL,
  `mem_type` varchar(50) NOT NULL,
  `mem_image` varchar(255) NOT NULL,
  `otppass` varchar(20) NOT NULL,
  `member_from` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petnnet_member`
--

INSERT INTO `petnnet_member` (`memid`, `names`, `email`, `password`, `reset_pass`, `mobile`, `status`, `country`, `state`, `city`, `address`, `add_date`, `mem_type`, `mem_image`, `otppass`, `member_from`) VALUES
(54, 'ARVIND SAHANI', 'test@gmail.com', 'gsiZlVYf', '', '9580671499', 0, '', '', '', 'karan', 1533797116, 'Both', '6c35d2d3-f413-4c0b-af9f-66f0c6fd2588_zps1b436d472.png', '', ''),
(55, 'ARVIND SAHANI', 'tesqt@gmail.com', 'oBChys0n', '', '9580671499', 0, '', '', '', 'aaa', 1533753556, 'Both', '', '', ''),
(56, 'ARVIND SAHANI', 'tesssssssst@gmail.com', 'LZJavf7s', '', '9580671499', 0, '', '', '', 'sss', 1533754159, 'Pet Buyer', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_product`
--

CREATE TABLE `petnnet_product` (
  `product_id` bigint(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `memid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `proimg` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sale_price` float NOT NULL,
  `actual_price` float NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petnnet_product`
--

INSERT INTO `petnnet_product` (`product_id`, `category_id`, `memid`, `title`, `proimg`, `description`, `sale_price`, `actual_price`, `created_at`, `status`) VALUES
(9, 10, 33, 'programming language', '', 'sss', 0, 0, 1533785540, 0),
(10, 4, 33, 'programming language', '', 'sss', 0, 0, 1533785566, 0),
(11, 8, 33, 'ssss', '', 'sss', 0, 0, 1533785584, 0),
(12, 3, 33, 'programming language', 'download1.png', 'sss', 0, 0, 1533785895, 0),
(13, 8, 33, 'programming language', '', 'sss', 0, 0, 1533785929, 0),
(14, 1, 33, 'ssss', 'download.png', 'sss', 0, 0, 1533785950, 0);

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_product_category`
--

CREATE TABLE `petnnet_product_category` (
  `prod_catid` int(11) NOT NULL,
  `prod_cat_title` varchar(255) NOT NULL,
  `prod_cat_description` text NOT NULL,
  `prod_cat_image` varchar(255) NOT NULL,
  `prod_cat_status` int(5) NOT NULL,
  `prod_cat_add_date` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petnnet_product_category`
--

INSERT INTO `petnnet_product_category` (`prod_catid`, `prod_cat_title`, `prod_cat_description`, `prod_cat_image`, `prod_cat_status`, `prod_cat_add_date`) VALUES
(1, 'update category', 'update category', '71jVdEEI6nL__SL1024_.jpg', 1, 28),
(4, 'programming language', 'qqqqqqqqqqqqq', 'IMG-20180623-WA00041.jpg', 0, 1267386851),
(5, 'programming language', 'sss', 'best-mobiles-phone-2015.jpg', 0, 1533292698),
(6, 'pet food', 'this is awesome food', '81zw5mrrCwL__UL1500_.jpg', 0, 1533242038),
(7, 'text category', 'nothing', 'best-mobiles-phone-20151.jpg', 0, 1533242132),
(8, 'text category', 'nothing', 'best-mobiles-phone-20152.jpg', 0, 1533242153),
(9, 'this is new post', '', '6c35d2d3-f413-4c0b-af9f-66f0c6fd2588_zps1b436d47.png', 0, 1533242598);

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_services`
--

CREATE TABLE `petnnet_services` (
  `serviceid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ser_code` varchar(255) NOT NULL,
  `add_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petnnet_services`
--

INSERT INTO `petnnet_services` (`serviceid`, `name`, `ser_code`, `add_date`) VALUES
(1, 'Pet Shops', 'Shop', ''),
(2, 'Pet Doctors', 'Doctor', ''),
(3, 'Pet Trainers', 'Trainer', ''),
(4, 'Pet Hostels', 'Hostels', ''),
(5, 'Pet Sitters', 'Sitter', ''),
(6, 'Pet Travel Companies / Handlers', 'Handler', ''),
(7, 'Pet Breeding', 'Breeding', ''),
(8, 'Pet Grooming Centers & Spas', 'Spas', ''),
(9, 'Pet Event Organizers', 'Organizers', ''),
(10, 'Pet Education / Courses / Course Providers ', 'Course Providers ', '');

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_shop_product`
--

CREATE TABLE `petnnet_shop_product` (
  `prod_id` bigint(20) NOT NULL,
  `shopid` bigint(20) NOT NULL,
  `prod_cat_id` int(11) NOT NULL,
  `prod_title` varchar(255) NOT NULL,
  `prod_description` text NOT NULL,
  `prod_price` float NOT NULL,
  `prod_unit` varchar(50) NOT NULL,
  `prod_pic` varchar(255) NOT NULL,
  `prod_for` varchar(255) NOT NULL,
  `prod_status` int(5) NOT NULL,
  `prod_add_date` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petnnet_subscribe`
--

CREATE TABLE `petnnet_subscribe` (
  `subid` int(11) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petnnet_subscribe`
--

INSERT INTO `petnnet_subscribe` (`subid`, `emailid`, `created_at`, `status`) VALUES
(131, 'test@gmail.com', '1533468201', '0'),
(132, 'test@gmail.com', '1533468201', '0'),
(133, 'test@gmail.com', '1533468202', '0'),
(134, 'test@gmail.com', '1533468202', '0'),
(135, 'test@gmail.com', '1533468202', '0'),
(136, 'test@gmail.com', '1533468202', '0'),
(137, 'test@gmail.com', '1533468202', '0'),
(138, 'test@gmail.com', '1533468203', '0'),
(139, 'test@gmail.com', '1533468203', '0'),
(140, 'test@gmail.com', '1533468203', '0'),
(141, 'test@gmail.com', '1533468203', '0'),
(142, 'test@gmail.com', '1533468203', '0'),
(143, 'test@gmail.com', '1533468203', '0'),
(144, 'test@gmail.com', '1533468203', '0'),
(145, '', '1533532731', '0'),
(146, '', '1533532732', '0'),
(147, '', '1533532732', '0'),
(148, '', '1533533573', '0'),
(149, '', '1533533573', '0'),
(150, '', '1533533573', '0'),
(151, '', '1267382686', '0'),
(152, '', '1267382687', '0'),
(153, '', '1267382687', '0'),
(154, '', '1267382687', '0'),
(155, '', '1267382687', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `pernnet_buss_services`
--
ALTER TABLE `pernnet_buss_services`
  ADD PRIMARY KEY (`serviceid`);

--
-- Indexes for table `petnnet_business`
--
ALTER TABLE `petnnet_business`
  ADD PRIMARY KEY (`memid`);

--
-- Indexes for table `petnnet_member`
--
ALTER TABLE `petnnet_member`
  ADD PRIMARY KEY (`memid`);

--
-- Indexes for table `petnnet_product`
--
ALTER TABLE `petnnet_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `petnnet_product_category`
--
ALTER TABLE `petnnet_product_category`
  ADD PRIMARY KEY (`prod_catid`);

--
-- Indexes for table `petnnet_services`
--
ALTER TABLE `petnnet_services`
  ADD PRIMARY KEY (`serviceid`);

--
-- Indexes for table `petnnet_subscribe`
--
ALTER TABLE `petnnet_subscribe`
  ADD PRIMARY KEY (`subid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pernnet_buss_services`
--
ALTER TABLE `pernnet_buss_services`
  MODIFY `serviceid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `petnnet_business`
--
ALTER TABLE `petnnet_business`
  MODIFY `memid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `petnnet_member`
--
ALTER TABLE `petnnet_member`
  MODIFY `memid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `petnnet_product`
--
ALTER TABLE `petnnet_product`
  MODIFY `product_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `petnnet_product_category`
--
ALTER TABLE `petnnet_product_category`
  MODIFY `prod_catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `petnnet_services`
--
ALTER TABLE `petnnet_services`
  MODIFY `serviceid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `petnnet_subscribe`
--
ALTER TABLE `petnnet_subscribe`
  MODIFY `subid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
